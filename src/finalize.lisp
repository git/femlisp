;;; -*- mode: lisp; fill-column: 75; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; finalize.lisp - Femlisp setup file
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2010 Nicolas Neuss, Karlsruhe Institute of Technology
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE KARLSRUHE INSTITUTE OF TECHNOLOGY, OR
;;; OTHER CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.start)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Register banner
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun femlisp-version () *femlisp-version*)
(defun femlisp-herald () (format nil "    Femlisp/~a" (femlisp-version)))

(defun femlisp-banner ()
  (format
   t "~&~%*** Femlisp-~A ***

Copyright (C) 2003-2018
Nicolas Neuss
University Heidelberg, KIT Karlsruhe, University Erlangen-Nuernberg.

Femlisp comes with ABSOLUTELY NO WARRANTY, for details see the
file LICENSE in the Femlisp main directory.  This is free
software, and you are welcome to redistribute it under certain
conditions.

You can enter \"(fl.demo:femlisp-demo)\" to get a guided tour
through Femlisp, and enter \"(quit)\" to leave the program.~%~%"
   *femlisp-version*))

(defun message (string &rest args)
  (format t "~&~%* ~?~%" string args))

(defun femlisp-check-features ()
  (let ((memory-size (fl.port:dynamic-space-size)))
    (when (and memory-size (< memory-size 1e9))
      (message "Your Lisp image has only ~D MB available memory.  If you are
working with decent hardware, you can probably increase this in
several ways:
1. By starting your Lisp image with appropriate command-line options,
   e.g.
      sbcl --dynamic-space-size 4000
   which gives sbcl 4GB memory at startup.
2. If you use Emacs/SLIME, by putting something like
      (setq inferior-lisp-program-args '(\"--dynamic-space-size\" \"4000\"))
   in your Emacs initialization file (.emacs or .emacs.d/init.el).
 " memory-size)))

  (let ((library-message
          "Femlisp could not find a ~A library.  Femlisp does not require
these urgently, but it may improve performance in some situations.  You may
install it by following the instructions in the Femlisp user manual at
<http://www.femlisp.org/femlisp-html>.  On Debian/Ubuntu, this can be as
simple as entering
    sudo apt install ~A
in a terminal.  After this operation you should recompile Femlisp
either completely or at least its Matlisp part as follows:
    (asdf:make :femlisp-matlisp :force t)"))
    
    (unless (fl.alien::load-blas-library)
      (message library-message "BLAS" "libblas-dev"))
    
    (unless (fl.alien::load-lapack-library)
      (message library-message "LAPACK" "liblapack-dev")))

    (unless (or (fl.alien::load-superlu-library) (fl.alien::load-umfpack-library))
      (message "Femlisp could not find libraries for sparse solvers.
Femlisp does not require these urgently, but it may improve performance in
many situations.  At the moment, Femlisp can use either SuperLU or UMFPACK.
You may install those by following the instructions in the Femlisp user manual
at <http://www.femlisp.org/femlisp-html>.
After installing these libraries on your system, you must compile a small
interface part by issuing the command
    make superlu
and/or
    make umfpack
in a terminal window from within the Femlisp main directory.  Following this step,
Femlisp must be recompiled completely as follows
    (asdf:make :femlisp-matlisp :force t)
and restarted afterwards."))
  )

(defun femlisp-restart ()
  "Should be called when restarting a saved Femlisp core."
  #+(or cmu scl) (progn (ext::print-herald))
  (femlisp-banner)
  (setq *read-default-float-format* 'double-float)
  (setq *package* (find-package :fl.application))
  #+linux
  (unless (fl.port:getenv "DISPLAY")
    (message "No DISPLAY variable set which is necessary for running dx.~%~
Therefore, interactive graphics is switched off.~%~
You may switch it on again using: (setq fl.port::*plot* T)")
    (setq fl.plot::*plot* nil))
  )

(pushnew :femlisp *features*)

(femlisp-check-features)

(femlisp-banner)

