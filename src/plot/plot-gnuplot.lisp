;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; plot-gnuplot.lisp - Plotting with Gnuplot
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.plot)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Default graphic commands
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod graphic-commands (object (program (eql :gnuplot)) &rest paras)
  "Default gnuplot plotting command."
  (list (format nil "plot ~S title \"data\" with linespoints"
                (apply #'graphic-file-name object program
                       :system-p t paras))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Plotting of ordinary graphs (list of 2D-points)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun ensure-polygons (polygons)
  (if (listp (first polygons))
      polygons
      (list polygons)))

(defun destructure-polygon (polygon)
  "A polygon is a list of the form
@lisp
  ([title-string] {key value}* vec1 ...)
@end lisp
This function ensures the form of a property list
  (:title title-string :type type :key1 :value1 ... :data (vec1 ...))"
  (let ((title (when (stringp (first polygon))
                 (pop polygon)))
        (keys (loop while (keywordp (first polygon))
                    collect (pop polygon)
                    collect (pop polygon))))
    (ensure (getf keys :type) :lines)
    (list* :title title :data polygon keys)))

(defmethod graphic-write-data (stream (polygons list) (program (eql :gnuplot))
			       &key &allow-other-keys)
  "Can handle either a single list of points, or a single list of the form
\(string . points) or a list of such lists which is interpreted as multiple
graphs."
  (dovec (polygon (ensure-polygons polygons))
    (destructuring-bind (&key type data color &allow-other-keys)
	(destructure-polygon polygon)
      (print color)
      (dovec (point data)
	(case type
	  ((:vectorfield :vectors)
           (format stream "~@{~,,,,,,'EG ~}~%"
                   (elt (car point) 0) (elt (car point) 1)
                   (elt (cdr point) 0) (elt (cdr point) 1)))
          (t
           (format stream "~@{~,,,,,,'EG ~}"
                   (elt point 0) (elt point 1))
           (when (eql color :rgb-variable)
             (format stream "~D" (elt point 2)))
           (terpri stream)))))
    (format stream "~%~%")))

;;; plot sin(x) with filledcurves lc "red" fs solid 0.1 noborder
(defmethod graphic-commands ((polygons list) (program (eql :gnuplot))
			     &key (linewidth 1) &allow-other-keys)
  (let ((gnuplot-file (graphic-file-name t :gnuplot :system-p t)))
    (list
     (apply #'concatenate 'string
	    "plot "
	    (loop for polygon in (ensure-polygons polygons)
               and i from 0 unless (zerop i)
               collect ", " collect
               (destructuring-bind (&key title type color fillstyle (lw linewidth) &allow-other-keys)
                   (destructure-polygon polygon)
                 (format nil "~S index ~D title ~S ~A lw ~D ~@[lc ~A ~] ~@[fs ~{ ~A~} ~]"
                         gnuplot-file i (or title (format nil "data-~D" i))
                         (case type
                           (:dashes "with lines dt 2")
                           (:vectorfield "with vectors")
                           (t (format nil "with ~A" (string-downcase (symbol-name type)))))
                         lw
                         (when color
                           (case color
                             (:rgb-variable "rgb variable")
                             (t (format nil "~S" color))))
                         (mapcar (_ (if (symbolp _)
                                        (string-downcase (symbol-name _))
                                        _))
                                 fillstyle))))))))

(defmethod plot ((polygons list) &rest rest &key &allow-other-keys)
  (apply #'graphic-output polygons :gnuplot rest))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Plotting of finite element functions (1d)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod graphic-write-data (stream object (program (eql :gnuplot))
			       &key cells cell->values (depth 0))
  "Writes data in gnuplot format to a stream."
  (declare (ignore object))
  (assert (= 1 (dimension (car cells))))
  (let* ((position-indices (compute-position-indices cells depth))
	 (position-array (position-array cells position-indices depth))
	 (values (and cell->values
		      (compute-all-position-values
		       cells position-indices depth cell->values))))
    (flet ((index->xpos (index) (aref (aref position-array index) 0)))
      (dolist (connection (sort (connections cells position-indices depth)
				#'<= :key (compose #'index->xpos #'second)))
	(dolist (index connection)
	  (format stream "~,,,,,,'EG ~,,,,,,'EG~%" (index->xpos index)
		  (if values (aref values index) 0.0)))))))

(defmethod plot ((mat standard-matrix) &key &allow-other-keys)
  "Plot a 2xn matrix similar to Gnuplot/Octave."
  (plot (loop for i below (ncols mat)
              collect (vector (mref mat i 0) (mref mat i 1)))))

(defmethod plot ((f function) &key range &allow-other-keys)
  "Plot a real function similar to Gnuplot/Octave."
  (destructuring-bind (a b step) range
    (plot (loop for x from a to b by step
                collect (vector x (funcall f x))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Plotting of finite element functions (1d)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod plot ((array array) &key &allow-other-keys)
  "Plot a 2xn matrix similar to Gnuplot/Octave."
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Testing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun test-plot-gnuplot ()
  
  (plot (lambda (x) (* (sin x) (cos (* 17.7 x)))) :range '(-5.0 5.0 .01))
  (plot (numerical-complex-derivative #'langevinx) :range '(-10.0 10.0 .01))
  (plot #'langevin :range '(-5.0 5.0 .01))
  (plot #'langevinx :range '(-100.0 100.0 .01))

  (let ((graph '(("graph-1" :type :dashes :color "blue" #(1.0 2.0) #(0.5 0.5) #(3.0 4.0)))))
    (plot graph :debug t))
  (let ((graph '(("x" :type :filledcurves :color "red" :fillstyle (:transparent :solid 0.5 :noborder) #(0.0 0.5) #(1.0 0.5) #(1.0 0.6) #(0.0 0.6) #(0.0 0.5)))))
    (plot graph :debug t :bottom 0.0 :top 1.0))
  (let ((graph '(("graph-2" :type :vectorfield
		  (#(1.0 2.0) . #(0.5 -0.5)) (#(3.0 4.0) . #(-0.5 0.5))))))
    (plot graph :debug t))
  (let ((graph '("graph-2" :type :histeps
                 (loop for i below 10 collect (vector (coerce i 'double-float) (sin i))))))
    (plot graph :debug t))
  (plot
   (list
    (list*
     "A"
     (loop for phi from 0 upto (* 2 pi) by (* 0.005 pi)
	   collect (vector (cos phi) (sin phi))))
    (list*
     "B"
     (loop for phi from 0 upto (* 2 pi) by (* 0.005 pi)
	   for r = (+ 1.0 (* 0.1
                             (expt (sin phi) 2)
                             (sin (* 40 phi))))
	   collect (vector (* r (cos phi)) (* r (sin phi))))))
   :border nil :tics nil
   :terminal "epslatex color" :output "example.tex"
   ;;:terminal #-(or)"postscript eps enhanced color" #+(or) "epslatex color"
   :left -1.7 :right 1.7 :top 1.15 :bottom -1.15 :linewidth 3)
  )

;;; (test-plot-gnuplot)
(fl.tests:adjoin-test 'test-plot-gnuplot)

