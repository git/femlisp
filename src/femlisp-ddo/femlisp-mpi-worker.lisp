(in-package :fl.application)

(file-documentation
 "This file initializes some memoized data at compile time and then saves
 an mpi-worker.")

(elahom-performance-initialize)

(sb-ext:save-lisp-and-die
 "mpi-worker"
 :executable t
 :toplevel #'mpi-worker::main)
