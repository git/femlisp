;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; krylow.lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.iteration)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Gradient method
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <gradient-method> (<linear-iteration>)
  ()
  (:documentation "Gradient-method.  Better use CG."))

(defmethod make-iterator ((linit <gradient-method>) mat)
  "Iterator for the gradient method."
  (let ((p (make-domain-vector-for mat))
	(a (make-image-vector-for mat)))
    (make-instance
     '<iterator>
     :matrix mat
     :initialize nil
     :residual-before t
     :iterate
     #'(lambda (x b r)
	 (declare (ignore b))
	 (copy! r p)
	 (gemm! 1.0 mat p 0.0 a)
	 (let ((lam (/ (dot r p) (dot a p))))
	   (axpy! lam p x)))
     :residual-after nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Preconditioned conjugate gradient iteration
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <preconditioned-krylow>  (<linear-iteration>)
  ((preconditioner :initform nil :initarg :preconditioner
                   :documentation "A linear iteration used as a preconditioner")
   (restart-cycle :reader restart-cycle :initform nil :initarg :restart-cycle
                  :documentation "For many Krylow methods a restart is
                  necessary after some steps.")))

(defmethod print-object :after ((iter <preconditioned-krylow>) stream)
  (with-slots (preconditioner) iter
    (when preconditioner
      (format stream "[pc:~A]" (class-name (class-of preconditioner))))))

(defclass <cg> (<preconditioned-krylow>)
  ()
  (:documentation "Preconditioned conjugate gradient iteration"))

(defmethod make-iterator ((cg <cg>) mat)
  "Standard method for the preconditioned conjugate-gradient iteration."
  (declare (optimize debug))
  (let ((precond (aand (slot-value cg 'preconditioner)
                       (make-iterator it mat))))
    (when (and precond (slot-value precond 'residual-after))
      (error "This preconditioner does not work, because the application
here wants to keep residual and rhs intact."))
    (let ((p (make-domain-vector-for mat))
	  (a (make-image-vector-for mat))
	  (w (make-image-vector-for mat))
	  (q (and precond (make-domain-vector-for mat)))
	  (alpha 0.0)
          (count 0))
      (assert (and p a w))
      (with-slots (initialize iterate) precond
        (flet ((restart (r)
                 (cond
                   (precond
                    (copy! r w)
                    (awhen initialize (funcall it p w w))
                    (funcall iterate p w w))
                   (t (copy! r p)))
                 (setq alpha (dot p r))))
          (make-instance
           '<iterator>
           :matrix mat
           :residual-before t
           :initialize
           (lambda (x b r)
             (declare (ignore x b))
             (restart r))
           :iterate
           (lambda (x b r)
             (declare (ignore b))
             (when (aand (plusp count)
                         (restart-cycle cg)
                         (zerop (mod count it)))
               (restart r))
             (unless (zerop alpha)
               (gemm! 1.0 mat p 0.0 a)
               (let* ((beta (dot a p))
                      (lam (/ alpha beta)))
                 (axpy! lam p x)
                 (axpy! (- lam) a r)
                 (let ((q (cond (precond
                                 (copy! r w) (x<-0 q)
                                 (funcall iterate q w w)
                                 q)
                                (t (copy r)))))
                   (let ((new-alpha (dot q r)))
                     (scal! (/ new-alpha alpha) p)
                     (m+! q p)
                     (setq alpha new-alpha))
                   )))
             ;; restart procedure
             (unless (aand (restart-cycle cg)
                           (zerop (mod (incf count) it)))
               (list :residual-after t)))
           :residual-after t))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; BiCGStab
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <bi-cgstab> (<preconditioned-krylow>)
  ()
  (:documentation "Preconditioned Bi-CGStab iteration"))

(defmethod make-iterator ((bcgs <bi-cgstab>) mat)
  "Standard method for the preconditioned BiCGstab iteration.  The
algorithm follows the original article of H. A. van der Vorst in SIAM
J. Sci. and Stat. Comput., 13(2), 631–644
@url{https://doi.org/10.1137/0913035}."
  (let* ((preconditioner (slot-value bcgs 'preconditioner))
         (precond (and preconditioner (make-iterator preconditioner mat))))
    (let ((p (make-domain-vector-for mat))
          (p^ (make-domain-vector-for mat))
          (r~ (make-image-vector-for mat))
          (v (make-image-vector-for mat))
          (s (make-image-vector-for mat))
          (s^ (make-domain-vector-for mat))
          (tee (make-image-vector-for mat))
          (res (and precond (make-image-vector-for mat)))
          (rho 1.0)
          (alpha 1.0)
          (omega 1.0)
          (count 0))
      (flet ((restart (x b r)
               (declare (ignore x b))
               (copy! r r~)
               (copy! r p)
               (setq rho 1.0
                     alpha 1.0
                     omega 1.0)
               (setq alpha (dot p r))))
        (make-instance
         '<iterator>
         :matrix mat
         :residual-before t
         :initialize
         (lambda (x b r)
           (declare (ignore x b))
           (copy! r r~)
           (copy! r p))
         :iterate
         (lambda (x b r)
           (when (aand (plusp count)
                       (restart-cycle bcgs)
                       (zerop (mod count it)))
             (restart x b r))
           (let ((rho~ (dot r~ r)))
             (when (zerop rho~)
               (error "Failure"))
             (axpy! (- omega) v p)
             (scal! (* (/ rho~ rho) (/ alpha omega)) p)
             (m+! r p)
             (setf rho rho~))
           (cond (precond
                  (with-slots (initialize iterate) precond
                    (x<-0 p^)
                    (copy! p res) ; the preconditioner might destroy res
                    (awhen initialize (funcall it p^ res res))
                    (funcall iterate p^ res res)))
                 (t (copy! p p^)))
           (gemm! 1.0 mat p^ 0.0 v)
           (setf alpha (/ rho (dot r~ v)))
           (copy! r s)
           (axpy! (- alpha) v s)
           (cond (precond
                  (with-slots (initialize iterate) precond
                    (x<-0 s^)
                    (copy! s res) ; the preconditioner might destroy res
                    (awhen initialize (funcall it s^ res res))
                    (funcall iterate s^ res res)))
                 (t (copy! s s^)))
           (gemm! 1.0 mat s^ 0.0 tee)
           ;; check of (norm s^) dropped
           (setf omega (/ (dot tee s)
                          (max (dot tee tee) 1.0e-30)))
           ;; update of x
           (axpy! alpha p^ x)
           (axpy! omega s^ x)
           (copy! s r)
           (axpy! (- omega) tee r)
           (unless (aand (restart-cycle bcgs)
                         (zerop (mod (incf count) it)))
             (list :residual-after t)))
         :residual-after t
         )))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; GMRES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <gmres> (<preconditioned-krylow>)
  ()
  (:documentation "Preconditioned GMRES iteration"))

(defmethod make-iterator ((it <gmres>) mat)
  "The algorithm follows ."
  (let ((precond (whereas ((preconditioner (slot-value it 'preconditioner)))
		   (make-iterator preconditioner mat))))
    (when (and precond (slot-value precond 'residual-after))
      (error "This preconditioner does not work, because the application
here wants to keep residual and rhs intact."))
    #+(or)
    (let ((p (make-domain-vector-for mat))
          (p^ (make-domain-vector-for mat))
          (r~ (make-image-vector-for mat))
          (v (make-image-vector-for mat))
          (s (make-image-vector-for mat))
          (s^ (make-domain-vector-for mat))
          (tee (make-image-vector-for mat))
          (rho 1.0)
          (alpha 1.0)
          (omega 1.0))
      (with-slots (initialize iterate) precond
        (make-instance
         '<iterator>
         :matrix mat
         :residual-before t
         :initialize
         (lambda (x b r)
           (declare (ignore x b))
           (copy! r r~)
           (copy! r p))
         :iterate
	 (lambda (x b r)
           (declare (ignore b))
           (let ((rho~ (dot r~ r)))
             (when (zerop rho~)
               (error "Failure"))
             (axpy! (- omega) v p)
             (scal! (* (/ rho~ rho) (/ alpha omega)) p)
             (m+! r p)
             (setf rho rho~))
           (cond (precond (x<-0 p^)
                          (funcall iterate p^ p p))
                 (t (copy! p p^)))
           (gemm! 1.0 mat p^ 0.0 v)
           (setf alpha (/ rho (dot r~ v)))
           (copy! r s)
           (axpy! (- alpha) v s)
           (cond
             (precond (x<-0 s^)
                      (funcall iterate s^ s s))
             (t (copy! s s^)))
           (gemm! 1.0 mat s^ 0.0 tee)
           ;; check of (norm s^) dropped
           (setf omega (/ (dot tee s) (dot tee tee)))
           ;; update of x
           (axpy! alpha p^ x)
           (axpy! omega s^ x)
           (copy! s r)
           (axpy! (- omega) tee r)
           ;; convergence check dropped (is done outside)
           (list :residual-after t))
       :residual-after t
       )))))

;;;; Testing

(defun test-krylow ()
  (let* ((dim 3)
         (n 20)
         (A (fd-laplace-matrix dim n))
         (b (ones (nrows A) 1)))
    ;; scale matrix
    (dorows (i A)
      (for-each-key-and-entry-in-row
       (lambda (j x)
         (setf (mref A i j)
               (* x (sqrt (* (1+ i) (1+ j))))))
       A i))
    (let ((sol1
            (linsolve A b :iteration (make-instance '<cg> :preconditioner (make-instance '<jacobi>))
                          :output t :success-if '(> :step 100)))
          (sol2 (linsolve A b :iteration (make-instance '<bi-cgstab> :preconditioner (make-instance '<jacobi>))
                              :output t :success-if '(> :step 100))))
      (assert (< (norm (m- sol1 sol2)) 1e-8))))

  (let* ((A fl.matlisp::*crs-test-matrix*)
         (b (ones (nrows A) 1)))
    (loop for precond in (list nil (make-instance '<jacobi>)) do
      (linsolve A b :iteration (make-instance '<bi-cgstab> :preconditioner precond)
                  :output t :success-if '(> :step 10))))

  (let* ((A fl.matlisp::*sparse-test-matrix*)
         (b (make-image-vector-for A)))
    (dorows (k A)
      (setf (vref b k) (eye 1)))
    (loop for precond in (list nil (make-instance '<jacobi>)) do
      (linsolve A b :iteration (make-instance '<bi-cgstab> :preconditioner precond)
                    :output t :success-if '(> :step 10))))

  (let ((A #m((2.0 -1.0  0.0)
	      (-1.0  2.0 -1.0)
	      ( 0.0 -1.0  2.0)))
	(b #m((1.0) (2.0) (3.0))))
    (linsolve A b :output t :iteration (make-instance '<cg> :preconditioner (make-instance '<jacobi>)))
    (linsolve A b :output t :iteration (make-instance '<cg>))
    (setf (mref A 2 1) 7.0)
    (ignore-errors
     (linsolve A b :output t :iteration (make-instance '<cg>)))  ; diverges
    (linsolve A b :output t :iteration (make-instance '<bi-cgstab>)) ; converges
    (linsolve A b :output t :iteration
              (make-instance '<bi-cgstab>
                             :preconditioner (make-instance '<jacobi>))))
  )

;;; (test-krylow)
(fl.tests:adjoin-test 'test-krylow)
