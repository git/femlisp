(in-package :fl.dealii)

(file-documentation
 "Implements Step-3 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_3.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-3}.  This file is
 written such that it can be compiled and loaded, but also processed by
 PROCESS-TUTORIAL-FILE.")

"The model problem of this tutorial step is a Poisson problem with
Dirichlet zero boundary conditions posed on the quadrangle [-1,1]^2 and
with a constant source of size 1.  It can be defined very easily, because
these settings of diffusion-constant and right-hand-side are defaults for
the function CDR-MODEL-PROBLEM."

(defun step-3-problem ()
  (cdr-model-problem
   (box-domain '((-1.0 1.0) (-1.0 1.0)))))

;;;

"We solve this problem by writing it on a blackboard and calling solve on
the blackboard.  A `solver' or better a procedure for approximation is then
determined automatically.  The termination criterion is that the number of
refinement levels is larger than 3."

#+when-processing
(storing
  (solve (blackboard :problem (step-3-problem)
                     :success-if '(> :nr-levels 3))))

;;;

"Because the call to solve was wrapped inside the macro @macro{storing},
much data from the calculation is still available in a special variable
@var{*result*} for interactive postprocessing.  For example, the solution
could be plotted:"

#+when-processing (plot (getbb *result* :solution))

;;;

"Because looking at something from *result* occurs often in interactive use,
there is an alternative for getting an item from the *result* blackboard
which works in the same way but is shorter."

#+when-processing (plot (^ :solution))

;;;

"Now we want to look at the convergence history, and for brevity and
flexibility later on, we wrap a suitable function definition around the
call for solving the problem."

(defun step-3/calculate (levels observer)
  (solve (blackboard
          :problem (step-3-problem)
          :observe (append *fe-approximation-observe* (list observer))
          :output 1
          :success-if `(>= :nr-levels ,levels))))

;;;

"Using this function we then examine the convergence of point values at the
point #(1/3 1/3)"

#+when-processing (step-3/calculate 6 (point-observe #(1/3 1/3)))

;;;

"And we can also study the convergence in mean value as the deal.II
tutorial does."

#+when-processing
(step-3/calculate 6
                  (list "        Mean" "~12,10F"
                        (lambda (blackboard)
                          (let ((solution (^ :solution blackboard)))
                            (* .25      ; 1/volume(domain)
                               (vref (aref (fe-integrate solution) 0) 0))))))

"Obviously, the convergence we observe here is faster than the rate
reported in deal.II/Step-3, which is because Femlisp uses higher order
finite elements by default."
