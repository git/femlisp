(in-package :fl.dealii)

(file-documentation
 "Implements Step-1 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_1.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-1}.  This file is
 written such that it can be compiled and loaded, but also processed by
 PROCESS-TUTORIAL-FILE.")

"This tutorial step shows the refinement of meshes."

;;;

"First, we write a function which generates a quadrilateral mesh for a unit
quadrilateral by refining an initial mesh four times uniformly."

(defun step-1/first-grid (&optional (dim 2))
  (let ((domain (n-cube-domain dim)))
    (make-mesh-from domain :initial-mesh-refinements 4)))

;;;

"Then we call this function and plot its result."

#+when-processing (plot (step-1/first-grid))

"This command should have popped up a graphics window showing the mesh.
Place it on your desktop such that it is visible and not covered by your
Lisp console - which is hopefully Emacs.  If you do not see this graphics
window, it might either be covered by other windows, or the interactive
Femlisp graphics does not work for whatever reason (maybe you are on
Windows and/or DX is not installed)."

;;;

"Note that this simple function is written in a dimension-independent way.
We can easily use it for obtaining a cube mesh instead:"

#+when-processing (plot (step-1/first-grid 3))

;;;

"Or a mesh for an interval:"

#+when-processing (plot (step-1/first-grid 1))

;;;

"Next, a quadrilateral mesh of a circle ring is refined locally towards the
inner boundary in the following function:"

(defun step-1/second-grid ()
  (lret* ((domain (circle-ring-domain .5 1.0 :interior-p nil))
          (mesh (make-hierarchical-mesh-from domain :parametric :from-domain)))
    (flet ((at-inner-circle-p (cell)
             (some (lambda (corner)
                     (< (norm corner) .50001))
                   (corners cell))))
      (loop repeat 2 do (refine mesh))
      (loop repeat 2 do
        (refine mesh :indicator #'at-inner-circle-p)))))

;;;

"And we can again plot the result of calling this function."

#+when-processing (plot (step-1/second-grid))

;;;

"We note that this locally refined mesh is not completely the same mesh as
in deal.II/step-1, because our initial mesh -copied again from the domain
definition- is coarser and is therefore refined two times uniformly at the
beginning.  Note also that (although the graphics shows only linear cell
edges) Femlisp works with nonlinear cell maps and calculations on this
domain would work quite well even without that uniform refinement."

;;;

"Finally, if you want to modify the program along the deal.II suggestions,
you might try to modify the above function so that the indicator can be
parametrized with a keyword parameter.  Then you might try one which
refines all cells in the upper halfplane, where the test function is
something like
@lisp
 (lambda (cell)
   (plusp (aref (midpoint cell) 0)))
@end lisp"
