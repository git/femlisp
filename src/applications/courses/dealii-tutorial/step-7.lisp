(in-package :fl.dealii)

(file-documentation
 "Implements Step-7 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_7.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-7}.  This file is
 written such that it can be compiled and loaded, but also processed by
 PROCESS-TUTORIAL-FILE.")

"This tutorial step solves a Helmholtz problem.  More precisely, we solve
the problem @math{- \Delta u + u = f} on a cube @math{\Omega=[-1,1]^d} with
Dirichlet boundary conditions @math{u=g_1} on one part @math{\Gamma_1} of
the boundary, and Neumann boundary conditions @math{n\cdot \nabla u=g_2} on
another part @math{\Gamma_2}.

f, g_1, g_2 should be chosen in such a way such that the solution is of the
form @math{u(x)=\sum_{i=1}^N exp(-|x-p_i|^2/\sigma^2)}, where p_1,...,p_N
are special points in @math{\Omega} and @math{\sigma} is a positive real
number.

Note that the plus sign in front of u in the equation ensures that the
equation is well-posed, even if there is no Dirichlet boundary part."

;;;

"For defining this problem, we first define a helper function which
gives us the peak centers for all dimensions in such a way that it agrees
with the deal.ii/Step-7 in dimension 2."

(defun peak-centers (dim)
  (remove (make-array dim :initial-element 0.5)
          (apply #'map-product #'vector
                 (make-list dim :initial-element '(-0.5 0.5)))
          :test 'equalp))

;;;

"Then we memoize this function (i.e. make it remember its results),
because it will be called a lot on very few parameters."

#+when-processing
(memoize-symbol 'peak-centers)

;;;

"With this helper function, we can then define our desired exact solution
as follows.  Note that we make the halfwidth of the peaks parametrizable."

(defvar *peak-halfwidth* 1/8)

(defun step-7-solution (x)
  (let ((dim (length x)))
    (loop for point in (peak-centers dim)
          for d = (m- x point)
          sum (exp (- (expt (/ (norm d)
                               *peak-halfwidth*)
                            2))))))

;;;

"We are lazy, so we define derivatives by numerical differentiation.
Internally, this uses central differences so we have to accept cancellation
errors in turn.  A more careful examination shows that the result after
two-fold numerical differentiation with double-floats is only about 5
digits."

(defun step-7-gradient (x)
  (funcall (numerical-gradient #'step-7-solution) x))

(defun step-7-hesse (x)
  (funcall (numerical-gradient #'step-7-gradient) x))

(defun step-7-laplace (x)
  (reduce #'+ (diagonal (step-7-hesse x))))

;;;

"With all these helper functions we can then start defining our coefficient
functions.  Because they are a little longer, we define them separately
instead of as anonymous functions in the problem definition."

;;;

"The source is given by the operator applied to the exact solution."

(defun step-7-source (x)
  (- (step-7-solution x)
     (step-7-laplace x)))

;;;

"And for the definition of the Dirichlet and Neumann boundary values we
introduce the following helper functions.  Note that we use that for the
case of the cube [-1,1]^d, the coordinates of x at the boundary can be used
to determine the normal direction.  The Dirichlet part is then
characterised by some coordinate having the value 1.0, and the Neumann part
by some coordinate having the value -1.0."

(defun step-7-dirichlet-p (x)
  (find 1.0 x))

(defun step-7-neumann-p (x)
  (find -1.0 x))

(defun step-7-neumann-flux (x)
  (loop for xc across x
        and k from 0
              thereis
              (and (or (= xc 1.0) (= xc -1.0))
                   (* xc
                      (vref (step-7-gradient x) k)))))

;;;

"Because of the special boundary conditions, this problem cannot be defined
using CDR-MODEL-PROBLEM anymore and we have to use a more general form of
problem definition."

(defun step-7-problem (dim)
  (create-problem
      (:type '<cdr-problem>
       :domain (dealii-cube-domain dim)
       :components '(u))
    (select-on-patch ()
      (:d-dimensional
       (diffusion () 1.0)
       (reaction () 1.0)
       (source (x)
         (step-7-source x)))
      ((and :external-boundary :d-1-dimensional)
       (constraint (x)
         (and (step-7-dirichlet-p x)
              (step-7-solution x)))
       (source (x)
         (if (step-7-neumann-p x)
             (step-7-neumann-flux x)
             0.0))))))

;;;

"Now we can solve the problem in the usual way calling solve."

#+when-processing
(storing
  (solve (blackboard
          :problem (step-7-problem 2)
          :output 1
          :success-if `(>= :time 10))))

;;;

"And plotting the solution shows three distinct peaks showing the response
to the three localized sources."

#+when-processing
(plot (^ :solution))

;;;

"For examining the order of convergence, we generate a coefficient which
takes the arguments x (position) and u (fe approximation) and calculates
the square of the difference between u and the actual solution evaluated at
x.  This coefficient is then integrated over all surface cells by the
function fe-integrate."

(let ((coeff
        (fxu->coefficient
         'STEP-7-L2-ERROR
         (lambda (x u)
           (let ((u (vref (aref u 0) 0)))
             (expt (- u (step-7-solution x)) 2))))))
  (defun step-7-l2-error (asv)
    (sqrt (fe-integrate asv :coeff-func coeff))))

;;;

"We can call this function directly to measure the L2-error of our
previously computed approximation."

#+when-processing
(step-7-l2-error (^ :solution))

;;;

"But we can also define an L2-norm observer with which we can enhance the
output of solve."

(defparameter *step-7-L2-error-observer*
  (list " L2-error" "~9,2,2E"
	(lambda (blackboard)
          (step-7-l2-error (^ :solution blackboard)))))

#+when-processing
(solve (blackboard
        :problem (step-7-problem 2)
        :output 1
        :fe-class (lagrange-fe 1)
        :observe (append *fe-approximation-observe*
                         (list *step-7-L2-error-observer*))
        :success-if `(>= :time 10)))

;;;

"Because we will call solve with different observers and problem parameters
in the following we define an abbreviation for a slightly generalized
construct."

(defun step-7-calculate
    (&key (dim 2) (levels 8) (order 1) (observers nil) adaptive-p)
  (solve (blackboard
          :problem (step-7-problem dim)
          :estimator (and adaptive-p (make-instance '<projection-error-estimator>))
          :indicator (if adaptive-p
                         (make-instance '<largest-eta-indicator> :from-level 3)
                         (make-instance '<uniform-refinement-indicator>))
          :output 1
          :fe-class (lagrange-fe order)
          :observe (append *fe-approximation-observe* observers)
          :success-if `(>= :nr-levels ,levels))))

;;;

"The deal.ii/step-7 tutorial suspects that a higher quadrature rule is
needed for avoiding superconvergence effects.  We want to check this by
binding the special variable *quadrature-order* to different values around
the above calculation and solving for different orders."

;;;

#+when-processing
(loop for order from 2 upto 8 by 2 do
  (let ((*quadrature-order* order))
    (format t "~&~%Quadrature order= ~D~%" order)
    (step-7-calculate :observers (list *step-7-L2-error-observer*))))

"Indeed it looks as if the quadrature order should be at least 4 in this
case for obtaining precise results asymptotically."

;;;

"We can generalize this to by defining a function which gives us observers
for Lp-norms and arbitrary exact solutions."

(defun lp-error (asv exact-solution &key (p 2))
  (let ((*quadrature-order* 4)
        (coeff (fxu->coefficient
                'STEP-7-LP-ERROR
                (lambda (x u)
                  (let* ((u (vref (aref u 0) 0))
                         (absdiff (abs (- u (funcall exact-solution x)))))
                    (if (eql p :inf)
                        absdiff
                        (expt absdiff p)))))))
    (if (eql p :inf)
        (fe-integrate asv :coeff-func coeff :combiner #'max :no-weights-p t)
        (expt (fe-integrate asv :coeff-func coeff) (/ p)))))

(defun lp-error-observer (exact-solution p)
  (list (format nil "~10<L~A-error~>" p)
        "~10,2,2E"
	(lambda (blackboard)
          (lp-error (^ :solution blackboard) exact-solution :p p))))

;;;

"We now perform the convergence tests for two different orders and several
error norms with global refinement for obtaining tables similar to the
deal.ii-tutorial gives us."

#+when-processing
(loop for order in '(1 2) do
  (format t "~&~%Discretization Q~D~%" order)
  (step-7-calculate
   :levels 8 :order order
   :observers
   (loop for p in '(2 :inf)
         collect
         (lp-error-observer #'step-7-solution p))))

;;;

"And a similar table with adaptive refinement.  We see that the mesh is
refined where the solution humps are situated.  But we see also that the
adaptivity gains are not enormous in this example."

#+when-processing
(loop for order in '(1 2) do
  (format t "~&~%Discretization Q~D~%" order)
  (step-7-calculate
   :levels 8 :order order :adaptive-p t
   :observers
   (loop for p in '(2 :inf)
         collect
         (lp-error-observer #'step-7-solution p))))

