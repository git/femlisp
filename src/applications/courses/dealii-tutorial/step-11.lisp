(in-package :fl.dealii)

(file-documentation
 "Implements Step-11 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_11.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-11}.  This file is
 written such that it can be compiled and loaded, but also processed by
 PROCESS-TUTORIAL-FILE.")

"This tutorial solves a Poisson problem on a circle with a constant source
f=-2 and pure Neumann conditions $\partial_n u=1$.  Because boundary
condition and source are compatible, this problem has a solution which is
determined up to a constant."

;;;

(defun step-11-problem ()
  "Special Poisson problem on circle with constant source and compatible
Neumann bc."
  (create-problem
      (:type '<cdr-problem> :domain (dealii-n-ball-domain 2) :components '(u))
    (select-on-patch ()
      (:d-dimensional
       (diffusion () 1.0)
       (source () -2.0))
      ((and :external-boundary :d-1-dimensional)
       (source () 1.0)))))

;;;

"The quantity of interest is the energy of the solution which does not care
about the constant.  We generate an observer which we can use when solving.
It simply uses the system matrix $A$ for calculating the quantity
$dot(u,Au)$."

(defparameter *energy-observer*
  (list "      Energy" "~16,14F"
	(lambda (blackboard)
          (sqrt (dot (^ :solution blackboard)
                     (m* (^ :matrix blackboard)
                         (^ :solution blackboard)))))))

"We now call solve on this problem.  Internally, it uses an iterative
solver (multigrid) which does not care about the non-uniqueness of the
solution and converges to some limit which is determined up to a constant."

#+when-processing
(storing
  (solve (blackboard :problem (step-11-problem)
                     :output 1
                     :success-if '(> :nr-levels 4)
                     :observe (append *fe-approximation-observe*
                                      (list *energy-observer*)))))

"We observe an extremely fast convergence of order h^{2p} to the precise
value $\sqrt{pi/2}=1.2533141373155...$ where p is the degree of
approximating polynomials.  The degree of the polynomials which was used in
this calculation could be determined as follows by extracting the
information from the hidden blackboard in the *result* variable."

#+when-processing
(discretization-order (^ :fe-class))

"And the solution can be plotted.  One can calculate easily that it has the
exact form $u(x,y)=1/2*(x^2+y^2)$."
