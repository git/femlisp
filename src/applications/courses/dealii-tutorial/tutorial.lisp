(in-package :fl.dealii)

(file-documentation
 "Provides routines for processing a tutorial file.  This enters a session
where you can step through a given tutorial file.  A tutorial file consists
of strings and code which is sometimes decorated by the feature conditional
#+when-processing.  Thus loading the tutorial without this feature active
ensures a certain consistency, but the tutorial is actually run only if
:when-processing is in *features* which is ensured by
@function{process-tutorial-file}.")

(defparameter *automatic-input* nil
  "When T, wait does not wait for input, but returns an empty string.")

(defun wait (&optional (string "<RETURN>"))
  (format t "~&~A~%" string)
  (if *automatic-input*
      ""
      (read-line *standard-input*)))

(defmacro when-processing (&body body)
  "Do nothing during load-compile of the file."
  (declare (ignore body))
  nil)

(defmacro combined (&body body)
  "Do nothing during load-compile of the file."
  `(progn ,@body))

(defun start-tutorial ()
  "This serves only as an indicator for the start of a tutorial session.")
(defun end-tutorial ()
  "This serves only as an indicator for the end of a tutorial session.")

#+(or)
(defun function-source (name)
  "Incomplete - find the source of a function which is better formatted
than the pretty printer stuff"
  (swank::find-definitions-for-emacs name))

(defun generate-diy-copy (file)
  "Incomplete - generate a copy of the file for playing around"
  (probe-file file)
  )
  
(defun process-tutorial-file (file)
  "Process a tutorial file.  It reads the files, prints all strings,
executes all code, and waits when a comment line is read.  While reading
the file, the feature :when-processing is set, which ensures that
everything is read and executed."
  ;; allow a shortcut for tutorial files of the standard deal.ii tutorial
  (when (symbolp file)
    (setq file
          (merge-pathnames (make-pathname :name (string-downcase (symbol-name file))
                                          :type "lisp")
                           (probe-file #p"femlisp:src;applications;courses;dealii-tutorial;")))
    (assert (probe-file file) ()
            "Deal tutorial ~A not found - probably that tutorial has not yet been ported to Femlisp"
            file))
  (with-open-file (stream file)
    (let ((*features* (cons :when-processing *features*)))
      (flet ((print-and-eval (expr)
               (let ((*print-case* :downcase)
                     (*print-pretty* t))
                 (format t "~&~%~S~%" expr))
               (locally
                   (declare #+sbcl(sb-ext:muffle-conditions sb-kernel:redefinition-warning))
                   (handler-bind
                       (#+sbcl(sb-kernel:redefinition-warning #'muffle-warning))
                     (eval expr))))
             (print-paragraph (string)
               (format t "~&~%~A~%" string)))
        (loop
          for expr = (read stream nil nil) while expr
          do
             (typecase expr
               (string (print-paragraph expr))
               (list
                (case (car expr)
                  ((file-documentation eval-when))
                  (in-package (eval expr))
                  (t (print-and-eval expr)))))
             (when (eql (peek-char t stream nil) #\;)
               (wait))
          finally
             (format t "~&~%Finished with processing the tutorial file ~S."
                     file)
          )))))

(defun test-dealii-tutorials ()
  (let ((*features* (cons :when-processing *features*))
        (*automatic-input* t))
    (loop for step from 1 to 12
          for path = (merge-pathnames #p"femlisp:src;applications;courses;dealii-tutorial;"
                                      (format nil "step-~D.lisp" step))
          for file = (probe-file path)
          do
             (format t "~&~%~%******* Step ~D *******~%" step)
             (load file))))

(fl.tests:adjoin-test 'test-dealii-tutorials)
;;;; (test-dealii-tutorials)
