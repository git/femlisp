(in-package :fl.dealii)

(file-documentation
 "Implements Step-10 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_10.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-10}.  This file is
 written such that it can be compiled and loaded, but also processed by
 PROCESS-TUTORIAL-FILE.")

"The idea is to approximate pi by calculating the area of refined
boundary-adapted meshes of the unit disk.  Alternatively, the length of the
mesh boundary is used to get an approximation to 2*pi.  Tables which
measure the order of convergence are printed."

;;;

"The dual logarithm is useful later on.  Note that the peculiar notation
#.(log 2.0) calculates that number immediately while reading the
expression which avoids computing a logarithm at runtime."

(defun log2 (x)
  (/ (log x) #.(log 2.0)))

"And this is a reasonable flexible function doing optionally the
approximation of pi by the mesh area or of 2*pi by using the boundary
length."

(defun step-10-approximate-pi (levels &key (type :area))
  "Approximate pi by generating a domain for a disk, meshing it with a
boundary-adapted mesh and calculating area and perimeter."
  (let ((domain (dealii-n-ball-domain 2)))
    (format t "~&Approximation of pi by ~A~%" type)
    (loop for degree from 1 upto 4 do
      (format t "Degree = ~D~%" degree)
      (let ((mesh (make-mesh-from domain :parametric (lagrange-mapping degree))))
        (multiple-value-bind (submesh factor)
            (ecase type
              (:area (values mesh 1.0))
              (:perimeter (values (skeleton-boundary mesh) 2.0)))
          (loop repeat levels
                for volume = (fl.discretization::domain-volume submesh degree)
                for error = (abs (- (/ volume factor) pi))
                and old-error = nil then error
                do
                   (plot submesh)
                   (format t "~5D  ~16,12F  ~5,2G  ~A~%"
                           (nr-of-cells submesh :highest)
                           volume error
                           (if old-error
                               (format nil "~3,2F"
                                       (log2 (/ old-error error)))
                               "-"))
                   (setf submesh (refine submesh))))))))

;;;

"Now here is a table using the area:"

#+when-processing (step-10-approximate-pi 6 :type :area)

"We can see that the order is p+1 for odd polynomial degree p+2 for even
polynomial degree which is in complete agreement with what one would
expect.  The deal.II which show a convergence order of 2p are nice on the
one hand, because they show that the deal.II quadrature rules and higher
order elements are carefully implemented with Gauss-Lobatto degrees of
freedom.  On the other hand, they are problematic, because they do not show
the area of the computational mesh accurately which should be completely
the same for polynomial degrees 2 and 3."

;;;

"And here is one using the perimeter or boundary length."

#+when-processing (step-10-approximate-pi 6 :type :perimeter)

;;;

"You can now try to modify the above functions into one which uses
triangles instead of quadrangles by simply calling N-BALL-DOMAIN instead of
DEALII-N-BALL-DOMAIN.  For a bonus, you can try to generalize the function
by including a keyword parameter :patch-type which switches between both."
