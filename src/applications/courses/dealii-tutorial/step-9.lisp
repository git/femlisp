(in-package :fl.dealii)

(file-documentation
 "Implements Step-9 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_9.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-9}.  This file is
 written such that it can be compiled and loaded, but also processed by
 PROCESS-TUTORIAL-FILE.")

"This tutorial step handles a convection problem on the usual domain
$[-1,1]^d$.  The velocity field is wiggling around the direction (2,1)^T.
A concentrated source is added in a ball around position $x_0=(-3/4,...)^T$
with diameter $0.1$.  On the inflow boundary, oscillating Dirichlet
boundary are prescribed as well."

;;;

"We first define some coefficient functions in a dimension independent way.
In principle, we could define them directly in the problem definition.
However, defining them as separate functions allows for separate testing
which can be helpful.

Note that all the following definitions are dimension-independent."

;;;

"The convection field wiggles around direction $(2,1)^T$."

(defun step-9-convection (x)
  (let ((dim (length x)))
    (apply #'double-vec
           2.0
           (make-list (1- dim)
                      :initial-element #I"1.0+0.8*sin(8*pi*x[0])"))))

;;;

"The localized source is scaled in such a way that its integral over the
domain is 1."

(defparameter *step-9-source-diameter* .1)

(defun step-9-source (x)
  (let* ((dim (length x))
         (x0 (make-double-vec dim -.75))
         (s *step-9-source-diameter*))
    (if (< (norm (m- x x0)) s)
        (expt (/ s) dim)
        0.0)))

;;;

"The boundary condition is defined with the help of two functions: The
first gives the value of the boundary condition, and the second is an
indicator if x is located on an inflow boundary (which is determined by
checking if some coordinate is -1.0."

(defun step-9-dirichlet (x)
  (let ((nx2 (dot x x)))
    #I"exp(5*(1-nx2))*sin(16*pi*nx2)"))

(defun step-9-inflow-p (x)
  (find -1.0 x))

;;;

"Because of these separate definition, we can easily generate a 1D plot for
its values along the left boundary."

#+when-processing
(plot (loop for y from -1.0 upto 1.0 by .01
            collect (vector y (step-9-dirichlet (vector -1.0 y)))))

"Note that (if Gnuplot graphics are working) a Gnuplot graphics window
should have popped up now.  However, it might be hidden under some other
windows."

;;;

"Using these helper functions we can then write our problem definition."

(defun step-9-problem (dim)
  (create-problem
      (:type 'fl.cdr::<cdr-problem>
       :domain (dealii-cube-domain dim)
       :components '(u)
       :multiplicity 1)
    (select-on-patch ()
      (:d-dimensional
       (fl.cdr::convection (x)
         (step-9-convection x))
       (source (x)
         (step-9-source x)))
      (:external-boundary
       (constraint (x)
         (and (step-9-inflow-p x)
              (step-9-dirichlet x)))))))

"Solving works as always, but we switch on some output, because we want to
check what is going on."

#+when-processing
(storing
  (solve (blackboard :problem (step-9-problem 2)
                     :success-if '(> :nr-levels 6)
                     :output 2)))

"We see that the solver tries multigrid at first which is not working
because of the dominant convection.  It then switches to direct solving."

;;;

"We can remove this first trial of a non-working scheme by setting the
solver to do a direct decomposition immediately."

#+when-processing
(storing
  (solve (blackboard :problem (step-9-problem 2)
                     :success-if '(> :nr-levels 6)
                     :solver (lu-solver)
                     :output 2)))

"Now the solution process should be a bit faster..."

;;;

"Of course, plotting the solution works too."

#+when-processing (plot (^ :solution))
