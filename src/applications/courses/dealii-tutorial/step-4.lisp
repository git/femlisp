(in-package :fl.dealii)

(file-documentation
 "Implements Step-4 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_4.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-4}.  This file is
 written such that it can be both loaded and processed by
 PROCESS-TUTORIAL-FILE.")

"This tutorial step solves the Poisson problem on the domain [-1,1]^2 with
a more interesting source function f(x)=||x||_4^4 and non-zero Dirichlet
values u_D(x)=||x||_2^2."

;;;

"Because the steps in the deal.II-tutorial use cubes of the form [-1,1]^d
very often, it is convenient to define a function for getting such cubes."

(defun dealii-cube-domain (dim)
  (box-domain (make-list dim :initial-element '(-1.0 1.0))))

;;;

"Note that this function `dealii-cube-domain' is written in a
dimension-independent way.  The same is true for the following problem
definiton which we write again using the function CDR-MODEL-PROBLEM:"

(defun step-4-problem (dim)
  (cdr-model-problem
   (dealii-cube-domain dim)
   :source (lambda (x) #I"4.0*norm(x,4)^^4")
   :dirichlet (lambda (x) #I"dot(x,x)")))

;;;

"Now we solve the problem in 3D and plot the solution.  This may take a
bit of time."

#+when-processing
(storing
  (solve (blackboard :problem (step-4-problem 3)
                     :success-if '(> :nr-levels 3)
                     :output 2)))

#+when-processing (plot (^ :solution))

;;;

"Note that if you would like to have nicer pictures like those in the Deal.II tutorial,
you could play around with paraview on the VTK file
`femlisp/images/step-4.vtk' obtained by the following command:"

#+when-processing
(plot (^ :solution) :program :vtk :filename "step-4.vtk")

;;;

"Now we turn to look at the convergence history and write again a flexible
function which can accomodate several test situations."

(defun step-4/calculate (dim levels observer)
  (solve (blackboard
          :problem (step-4-problem dim)
          :observe (append *fe-approximation-observe* (list observer))
          :output 1
          :success-if `(>= :nr-levels ,levels))))

;;;

"Using this function we can study the convergence of values at the
point (1/3 ...) in several dimensions."

#+when-processing
(loop for dim from 1 upto 3 do
  (format t "Solving in ~DD~%" dim)
  (step-4/calculate dim 4 (point-observe 1/3)))

;;;

"And the same is possible with mean value convergence as well.  Because we
need a `mean-value observer' already for the second time (the first time
was in step-3), it is reasonable to write a function which gives us such
`mean-value observers' for this and all later uses."

(defun mean-observer (domain-volume)
  (list "        Mean" "~12,10F"
        (lambda (blackboard)
          (let ((solution (^ :solution blackboard)))
            (/ (vref (aref (fe-integrate solution) 0) 0)
               domain-volume)))))

;;;

"This observer-generating function then can be used in the following study
of convergence in mean value."

#+when-processing
(loop for dim from 1 upto 3 do
  (format t "Solving in ~DD~%" dim)
  (step-4/calculate
   dim 4 (mean-observer 4.0)))

;;;

"Note that ideally the mean-value observer should not need the volume of
the domain as parameter but could calculate it itself by integrating the
constant 1 over the domain.  However, for the moment the above function is
a sufficiently good solution to the problem at hand."
