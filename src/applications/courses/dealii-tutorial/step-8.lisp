(in-package :fl.dealii)

(file-documentation
 "Implements Step-8 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_8.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-8}.  This file is
 written such that it can be compiled and loaded, but also processed by
 PROCESS-TUTORIAL-FILE.")

"We solve a 2D elasticity problem again on the same domain as before.  The
elasticity coefficient is isotropic with Lame parameters lambda=1 and mu=1.
Volume forces of size 1 pull in x-direction in two disks of radius 0.2 with
centers at (-0.5,0) and (0.5,0), and in y-direction in a disk of the same
size at the origin (0,0)."

;;;

"The problem can be defined dimension-independent and with the disk radius
as a possibly interesting parameter as follows."

(defun step-8-problem (&key (dim 2) (radius .2))
  (let* ((e_x (unit-vector dim 0))
         (e_y (unit-vector dim 1))
         (origin (make-double-vec dim))
         (center1 (scal -0.5 e_x))
         (center2 (scal .5 e_x)))
    (flet ((nearby(x y)
             (< (norm (m- x y)) radius)))
      (elasticity-model-problem
       (dealii-cube-domain dim)
       :force (lambda (x)
                (cond ((or (nearby x center1)
                           (nearby x center2))
                       e_x)
                      ((nearby x origin) e_y)
                      (t origin)))))))

;;;

"Solving works as before."

#+when-processing
(storing
  (solve (blackboard
          :problem (step-8-problem)
          :output 1
          :success-if `(>= :time 10))))

;;;

"By default, the x-component of the solution is plotted."

#+when-processing
(plot (^ :solution))

;;;

"But you can also plot the y-component."

#+when-processing
(plot (^ :solution) :component 1)

;;;

"And you can plot arrows in the direction of the distortion as well."

#+when-processing
(plot (^ :solution) :component 'fl.elasticity::u :rank 1 :shape 2)
                      
;;;

"This was quite easy, but what was going on behind the curtains when
solving?  Here are some exercises you can do to find out:

1. You are hopefully using Emacs?  Then you can inspect the solution by
   pressing the key combination `C-c I` on (^ :solution).  Navigating
   through the data you can find out which Lagrange ansatz space was used
   for approximation.

2. Increasing the output level in the call to solve you get more
   information about which solver was used.  Note that -if you are using
   Emacs- you can try pressing 'M-.' if your cursor is positioned on a
   symbol.  In many cases, Lisp will find its definition.  (This will not
   be successful, if the symbol is not visible in the current package.  In
   this case, you can try to find the correct package using the command
   'apropos' which is also available via the menu and keyboard shortcut."

