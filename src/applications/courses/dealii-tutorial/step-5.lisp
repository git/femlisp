(in-package :fl.dealii)

(file-documentation
 "Implements Step-5 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_5.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-5}.  This file is
 written such that it can be compiled and loaded, but also processed by
 PROCESS-TUTORIAL-FILE.")

"Step 5 of the tutorial solves the Poisson problem on an n-dimensional ball
with constant source term, but varying diffusion coefficient."

;;;

"Again, this problem can be defined by calling CDR-MODEL-PROBLEM with
suitable parameters:"

(defun step-5-problem (dim)
  (cdr-model-problem
   (n-ball-domain dim)
   :diffusion (lambda (x)
                (scal (if (< (norm x) .5)
                          20.0
                          1.0)
                      (eye 2)))))

;;;

"We solve the 2D version of this problem as follows:"

#+when-processing
(storing
  (solve (blackboard
          :problem (step-5-problem 2)
          :output 1
          :success-if `(>= :nr-levels 4))))

;;;

"And we plot the resulting approximation to the solution."

#+when-processing (plot (^ :solution))

;;;

"Note that the meshes originating from the dimension-independent domain
definition in the function N-BALL-DOMAIN are simplicial and use nonlinear
cell mappings which can represent the jump in the diffusion coefficient
perfectly which leads to a very good approximation.  We can examine this by
looking at the values of the FE approximations at the origin."

#+when-processing
(storing
  (solve (blackboard
          :problem (step-5-problem 2)
          :output 1
          :observe (append *fe-approximation-observe*
                           (list (fl.strategy::point-observe 0.0)))
          :success-if `(>= :nr-levels 5))))

;;;

"Note that deal.II cannot work with simplices, and therefore could _not_ do
the kind of calculation we did just now.  In contrast, Femlisp can work in
the deal.II way with quadrilateral meshes."

;;;

"Indeed, there exists a function called DEALII-N-BALL-DOMAIN which
generates a domain using cubic patches.  You may look at it by positioning
your cursor on the function name and pressing `M-.` (Alt and . together).
It works by taking an interior small cube Q, projecting its boundary B1 to
the outer sphere resulting in a nonlinear decomposition B2 of the sphere
and connecting B1 and B2.  Together with Q, this results in a decomposition
of the ball by deformed cubes."

;;;

"Now, the above function step-5-problem should be generalized such that the
problem is obtained for arbitrary ball definitions and even other domains.
Note that the new definition keeps the old behaviour when the argument is a
number."

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; we unintern the symbol for avoiding a warning about the duplicate
  ;; definition during usual compiling/loading this file.
  (unintern 'step-5-problem))

(defun step-5-problem (domain)
  (when (numberp domain)
    (setf domain (n-ball-domain domain)))
  (cdr-model-problem
   domain
   :diffusion (lambda (x)
                (scal (if (< (norm x) .5)
                          20.0
                          1.0)
                      (eye 2)))))

;;;

"Then we solve the problem on the ball domain defined by quadrilaterals and
plot the solution."
#+when-processing
(storing
  (solve (blackboard
          :problem (step-5-problem (dealii-n-ball-domain 2))
          :output 1
          :success-if `(>= :nr-levels 4))))

#+when-processing (plot (^ :solution))

;;;

"Note that these quadrilateral meshes do not fit well with the jump in the
diffusion coefficient and therefore quadrature errors together with the
impossibility of representing the jump in the gradient precisely lead to
much worse approximation behaviour than with the deformed triangular mesh
which could resolve the jump after the first refinement step."

#+when-processing
(storing
  (solve (blackboard
          :problem (step-5-problem (dealii-n-ball-domain 2))
          :output 1
          :observe (append *fe-approximation-observe*
                           (list (fl.strategy::point-observe 0.0)))
          :success-if `(>= :nr-levels 5))))

;;;

"In the deal.II version of this tutorial step, the solution is written in
Postscript format to a file.  Now, Femlisp does not implement an output in
Postscript format itself.  However, it can use the coupled program DX for
that purpose as follows which results in a file step-5.eps in the directory
femlisp/images."

#+when-processing
(plot (^ :solution) :filename "step-5" :format "eps")

;;;

"Unfortunately, the resulting EPS file is scaled in an ugly way, probably
due to weaknesses of DX.  Using the TIFF output format instead leads to a
nicer result, which could then be converted to other graphics formats,
including Postscript, by standard tools like, e.g., ImageMagick."

#+when-processing
(plot (^ :solution) :filename "step-5" :format "tiff")
