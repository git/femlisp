(in-package :fl.dealii)

(file-documentation
 "Implements Step-6 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_6.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-6}.  This file is
 written such that it can be compiled and loaded, but also processed by
 PROCESS-TUTORIAL-FILE.")

"In this tutorial step, we work around the problem of the quadrilateral
mesh in Step-5, namely that it cannot resolve the jump in the diffusion
coefficient."

;;;

"The idea is to do this by an adaptive algorithm with local mesh refinement
along the interface.  Mesh adaptivity is an important feature in finite
element methods, and can be used in other situations as well."

;;;

"The following call of solve also shows how one can specify problem solving
in more detail.  More precisely:

1. We specify an error estimator which calculates the L2-difference between the
approximation u_h on the finest mesh, and a projection of u_h to the next
coarser level.

2. We specify an indicator which marks 30% of the cells where the local
error contributions to the error are the largest.

3. We specify that the discretization uses conforming bilinear finite
elements, which is the same as deal.II does, and which is probably also the
best in this situation, because the largest part of the error results from
the insufficient resolution of the jump in the diffusion coefficient.

4. We terminate the approximation process when 9 levels of refinement are
reached."

;;;

#+when-processing
(storing
  (solve
   ;; specify a solver
   (make-instance
    '<stationary-fe-strategy>
    :fe-class (lagrange-fe 1)
    :estimator (make-instance '<projection-error-estimator>)
    :indicator (make-instance '<largest-eta-indicator> :fraction 0.3 :from-level 1)
    :success-if `(>= :nr-levels 9)
    :solver (lu-solver)
    :observe (append *fe-approximation-observe*
                     (list (fl.strategy::point-observe 0.0)))
    :output :all)
   (blackboard
    :problem
    (step-5-problem (dealii-n-ball-domain 2)))))

;;;

"Because the result was stored, we can do postprocessing here like plotting
the solution."

#+when-processing (plot (^ :solution))
;;;

"Or we can can plot the mesh."

#+when-processing (plot (^ :mesh))

;;;

"Note that the same mesh could alternatively be obtained by calling
@function{mesh} on the solution:"

#+when-processing (plot (mesh (^ :solution)))
