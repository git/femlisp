(in-package :fl.dealii)

(file-documentation
 "Implements Step-2 from
 @url{http://www.dealii.org/9.0.0/doxygen/deal.II/step_2.html} resp.
 @file{/usr/share/doc/libdeal.ii-doc/examples/step-2}.  This file is
 written such that it can be compiled and loaded, but also processed by
 PROCESS-TUTORIAL-FILE.")

"In this tutorial step, we want to look at the structure of discretization
matrices."

;;;

"The following function discretizes a simple diffusion problem and returns
the matrix (and as a second value also a right hand side vector which we
ignore in the following):"

(defun step-2/matrix ()
  (let* ((fe-class (lagrange-fe 1))
         (mesh (step-1/second-grid))
         (problem (cdr-model-problem (domain mesh) :dirichlet nil)))
    (discretize-globally problem mesh fe-class)))

;;;

"The following function call will write out the whole matrix on the console.
The matrix is quite large, so don't be appaled.  If you happen to use
Femlisp inside an Emacs console like the SLIME repl, you should enable the
truncation of long lines with `M-x toggle-truncate-lines' before pressing
RETURN now."

;;;

#+when-processing (display (step-2/matrix))

"Note that within Emacs you can manoeuver in the preceding output using
the cursor keys and C-a or C-e."

;;;

"This output can also be collapsed such that it shows only the (block)
sparsity pattern."

;;;

#+when-processing
(display (step-2/matrix) :pattern :block)

"Again -if you use Emacs what I recommend- you can manoeuver in the
preceding output using the cursor keys and C-a or C-e."

;;;

"In contrast to Femlisp, deal.II does not write out this information to the
console, but instead to the disk as an SVG picture.  Femlisp does not have
this functionality built-in, but it can be implemented rather quickly."

;;;

"First we load the library CL-SVG."

#+when-processing
(ql:quickload :cl-svg)

;;;

"Then, with the help of CL-SVG, we write our SVG output function..."

#+when-processing
(defun convert-ascii-to-svg (ascii-pic pathname &optional title)
  (let ((rows (with-input-from-string (stream ascii-pic)
                (loop for line = (read-line stream nil)
                      while line collect line))))
    (let ((scene (cl-svg:make-svg-toplevel
                  'cl-svg:svg-1.1-toplevel
                  :height (length rows)
                  :width (length (first rows)))))
      (when title (cl-svg:title scene title))
      (loop for row in rows and i from 0 do
        (loop for char across row and j from 0 do
          (when (eql char #\*)
            (cl-svg:draw scene (:rect :x i :y j :height .9 :width .9)))))
      ;; write scene to file
      (with-open-file (s pathname :direction :output :if-exists :supersede)
        (cl-svg:stream-out s scene)))))

;;;

"... which we can use as follows:"

#+when-processing
(convert-ascii-to-svg
 (with-output-to-string (stream)
   (display (step-2/matrix) :pattern :block :stream stream))
 (images-pathname "sparsity_pattern1.svg")
 "Sparsity pattern 1")

"You may now look at the generated file `sparsity_pattern1.svg' which
should be located in the images directory."
