(in-package :fl.application)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Testing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun regression-test-femlisp
    (&key (logfile #p"femlisp:fltest.log") (passes '(:serial-1 :serial-2 :parallel-2 :parallel-4)))
  (flet ((test (title)
           (when (member title passes)
             ;; run once in serial mode
             (aif (worker-count)
                  (format t "Testing femlisp with ~D kernels~%" it)
                  (format t "Testing serial femlisp~%"))
             (test-femlisp :title title :logfile logfile)
             (when fl.tests::*failed*
               (format t "Pass ~A of multiple tests failed.~%" title)
               (return-from regression-test-femlisp)))))
    (lparallel::end-kernel)
    (test :serial-1)
    (test :serial-2)
    (fl.parallel:new-kernel 2)
    (test :parallel-2)
    (fl.parallel:new-kernel 4)
    (test :parallel-4)))


