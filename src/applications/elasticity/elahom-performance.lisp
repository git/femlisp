;;; -*- mode: lisp; fill-column: 64; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; elahom-performance.lisp - performance measurements using the elahom problem
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2018-
;;; Nicolas Neuss, Friedrich-Alexander-Universitaet Erlangen-Nuernberg
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
;;; IN NO EVENT SHALL THE AUTHOR, THE FAU ERLANGEN-NUERNBERG, OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.application)

(defun elahom-performance-initialize (&key (order 5))
  "Initializes all tables which the preceding test needs for its default
parameters.  After this function has been executed such performance
measurements can be done in a reasonable way."
  (let ((fedisc (lagrange-fe order)))
    (loop for dim from 0 upto 3 do
      (let* ((cell (n-cube dim))
             (fe (get-fe fedisc cell)))
        (let ((qrule (quadrature-rule fe)))
          (ip-values fe qrule)
          (when (plusp dim)
            (ip-gradients fe qrule)))
        (let ((refrule (get-refinement-rule cell :regular)))
          (local-imatrix refrule fe)
          (local-pmatrix refrule fe))
        ))))

(defun elahom-performance-calculation (&key (levels 2) (output 2) (order 5) (threshold 2e-14))
  (format t "Initializing tables ...")
  (elahom-performance-initialize :order order)
  (format t " done.~%")
  ;; then do the calculation
  (elasticity-interior-effective-coeff-demo
   (elasticity-inlay-cell-problem (n-cell-with-ball-hole 3))
   :order order :levels levels :plot nil :output output)
  (with-items (&key defnorm initial-defnorm)
      (^ :solver-blackboard)
    (format t "Initial defect norm: ~A  --> Defect norm at the end: ~G~%"
            initial-defnorm defnorm)
    (when (member levels '(1 2))
      (let ((serial-defnorm (ecase levels
                              (1 1e-14)
                              (2 7e-15))))
        (assert (< (abs (- defnorm serial-defnorm)) threshold))))))

(defun elahom-performance-test (nrs-of-kernels &rest args &key initialize &allow-other-keys)
  (when initialize
    (format t "We perform one computation on two levels for initializing FE and interpolation:~%")
    (elahom-performance-calculation :levels 2))
  ;; then we test performance for varying number of kernels
  (loop for n in nrs-of-kernels do
    ;; clear previous stuff (otherwise, memory might be a problem)
    (setq *result* nil)
    (gc)
    (new-kernel n)
    (format t "Testing for ~D kernels:~%" n)
    (apply #'elahom-performance-calculation (sans args :initialize))))

;; (elahom-performance-test '(1 2) :levels 3)
(defun elahom-longtime-stability-test (nr-of-trials &rest args
                                       &key (levels 1) (threshold 1e-10)
                                       &allow-other-keys)
  (loop for i below nr-of-trials do
    (format t "~&~%*** Rechnung ~D~%~%" i)
       (apply #'elahom-performance-calculation
              :levels levels :threshold threshold
              (sans args :levels :threshold))))

;;; (elahom-performance-calculation :levels 2)
;;; (elahom-longtime-stability-test 2 :levels 1)

(defun elahom-performance-tests ()
  ;; this takes some time
  (time (elahom-performance-initialize))

  ;; but then the following works good
  (time (elahom-performance-calculation))
  ;; and should take almost the same time when called again
  (time (elahom-performance-calculation))

  #+(or)
  (sb-sprof:with-profiling
      (:max-samples 10000 :reset t :threads :all)
    (time (elahom-performance-calculation :levels 2 :order 5)))
  )
  
