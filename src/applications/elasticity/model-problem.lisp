;;; -*- mode: lisp; fill-column: 64 -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; model-problem.lisp - Model problems for linear elasticity
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.application)

(defun elasticity-model-problem-computation (domain &key (output 1) plot)
  "Performs the model problem demo."
  (storing
    (solve (blackboard
	    :problem (elasticity-model-problem domain)
            :base-level 1 ; necessary for 3D simplex domains and low-order FE
	    :plot-mesh t :output output :success-if `(> :time ,fl.demo:*demo-time*))))
  (when plot
    (plot (^ :solution))))
  
(defun make-elasticity-model-problem-demo (domain domain-name)
  (let ((title domain-name)
	(short (format nil "Solve an elasticity system on a ~A." domain-name))
	(long "Solves a linear elasticity problem with rhs
identical 1 on the given domain.  The solution strategy does
uniform refinement."))
    (let ((demo
	   (make-demo :name title :short short :long long
		      :execute (lambda ()
				 (elasticity-model-problem-computation domain :plot t)))))
      (adjoin-demo demo *elasticity-demo*))))

(make-elasticity-model-problem-demo (n-simplex-domain 2) "unit-triangle")
(make-elasticity-model-problem-demo (n-cube-domain 2) "unit-quadrangle")
(make-elasticity-model-problem-demo (n-simplex-domain 3) "unit-tetrahedron")
(make-elasticity-model-problem-demo (simplex-product-domain '(1 2)) "unit-wedge-1-2")
(make-elasticity-model-problem-demo (simplex-product-domain '(2 1)) "unit-wedge-2-1")
(make-elasticity-model-problem-demo (n-cube-domain 3) "unit-cube")

(defun constant-constraint (vec)
  (let ((n (length vec)))
    (values
     (sparse-tensor
      (loop for k below n collect (list #m(1.0) k)))
     (sparse-tensor
      (loop for k below n collect (list (ensure-matlisp (aref vec k)) k))))))
  
(defun elasticity-dirichlet-neumann-model-problem (dim)
  (create-problem (:type '<elasticity-problem>
                   :components `((u ,dim))
                   :domain (n-cube-domain dim))
    (select-on-patch ()
      ((and :external-boundary :left)
       (FL.ELLSYS::CONSTRAINT ()
         (constant-constraint (coerce (cons -1.0 (make-list (1- dim) :initial-element 0.0))
                                      'vector))))
      ((and :external-boundary :right)
       (FL.ELLSYS::CONSTRAINT ()
         (constant-constraint (coerce (cons 1.0 (make-list (1- dim) :initial-element 0.0))
                                       'vector))))
      (:interior
       (FL.ELLSYS::A ()
         (isotropic-elasticity-tensor :dim dim :lambda 1.0 :mu 1.0))))))

(defun test-elasticity-model-problem ()

  ;; Linear elasticity problem
  (storing
    (time
     (let* ((domain (n-simplex-domain 3))
            (dim (dimension domain)))
       (solve
	(blackboard
	 :problem
	 (elasticity-model-problem
	  domain :lambda 1.0 :mu 1.0
	  :force (ellsys-one-force-coefficient dim 1))
	 :output t :base-level 1 :success-if '(> :nr-levels 2))))))
  
  (storing
    (time
     (let* ((*suggested-discretization-order* 1))
       (solve
	(blackboard
	 :problem (elasticity-dirichlet-neumann-model-problem 2)
         :observe (append *stationary-fe-strategy-observe*
                          (list (point-observe #d(0.75 0.75) :component 0)
                                (point-observe #d(0.75 0.75) :component 1)))
	 :output 1 :base-level 1 :success-if '(> :nr-levels 7))))))

  (plot (^ :solution) :component 'u :rank 1 :shape 2)
  (fe-extreme-values (^ :solution) :component 'fl.elasticity::u)
  (with-dbg (:graphic)
    (plot (^ :solution) :component 1 :rank 0 :shape nil))
)

;;; (fl.application::test-elasticity-model-problem)
(fl.tests:adjoin-test 'test-elasticity-model-problem)
