(in-package :fl.application)

;; do a 2D calculation
(elasticity-interior-effective-coeff-demo
 (elasticity-inlay-cell-problem (n-cell-with-ball-hole 2))
 :order 5 :levels 2 :output :all :plot t)

;; plot the solutions
(loop for index in '(0 1 2 3) do
  (plot (^ :solution)
        :component 'fl.elasticity::u :index index :shape 2 :rank 1
        :background :white :plot :file
        :filename (format nil "els-2017-sol-~D" index)
        :format "tiff"))

;; plot the mesh
(plot (^ :mesh)
      :background :white :plot :file
      :filename "els-2017-mesh" :format "tiff")

;; do a 2D calculation
(elasticity-interior-effective-coeff-demo
 (elasticity-inlay-cell-problem (n-cell-with-ball-hole 2))
 :order 5 :levels 4 :output :all :plot t)

;; plot the mesh
(let ((mesh (^ :mesh)))
  (loop for level in '(1 2 3) do
    (plot (cells-on-level mesh level)
          :background :white :plot :file
          :filename (format nil "els-2017-mesh-~D" level)
          :format "tiff")))
