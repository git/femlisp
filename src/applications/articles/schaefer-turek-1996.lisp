(in-package :fl.flow-application)

(defpackage "FL.SCHAEFER-TUREK"
  (:use "COMMON-LISP"
	"FL.MACROS" "FL.UTILITIES" "FL.MATLISP"
	"FL.DEBUG" "FL.TESTS" "FL.DEMO" "FL.PARALLEL"
        "FL.DICTIONARY"        
	"FL.FUNCTION" "FL.MESH"
	"FL.PROBLEM" "FL.CDR" "FL.ELLSYS" "FL.ELASTICITY" "FL.NAVIER-STOKES"
	"FL.DISCRETIZATION" "FL.ELLSYS-FE" "FL.ELASTICITY-FE" "FL.NAVIER-STOKES-FE"
	"FL.ITERATION" "FL.MULTIGRID" "FL.GEOMG"
	"FL.STRATEGY" "FL.PLOT"
	"FL.DOMAINS" "FL.FLOW-APPLICATION")
  (:documentation "Package for the Schaefer-Turek benchmark."))

(in-package :fl.schaefer-turek)

(file-documentation
 "Implements the calculations of the Schäfer-Turek benchmark as described in

M. Schaefer, S. Turek: 'Benchmark computations of laminar flow around
cylinder'; in 'Flow Simulation with High-Performance Computers II', Notes
on Numerical Fluid Mechanics 52, 547-566, Vieweg 1996")

;;; Very precise values taken from the PhD thesis of Guido Nabh (University
;;; Heidelberg): "On high order methods for the stationary incompressible
;;; Navier-Stokes equation" (1998)

(defparameter *nabh-cd* 5.57953524)
(defparameter *nabh-cl* 0.01061894)

;;;; Benchmark domain definition

(defun st-cylinder
    (&key (dim 2) (radius 0.05) (offset (make-double-vec dim 0.2))
     &allow-other-keys)
  (let* ((skel
           ;; skel ist ein Randgitter für [0, 0.44]x[0, 0.41]
           (skeleton-boundary
            (linearly-transformed-skeleton
             (skeleton (n-cube 2))
             :A (diag #d(0.44 0.41))
             :b #d(0.0 0.0))))
         (projection (project-to-ellipsoid
                      offset
                      (scal (/ (* radius radius))
                            (eye dim))))
         (cylinder-table (nth-value 1 (transformed-skeleton skel :transformation projection))))
    (telescope skel cylinder-table)))

(defun classify-benchmark-domain (&key (bottom 0.0) (top 0.41) (left 0.0) (right 2.2))
  (lambda (cell classifications)
    (let* ((midpoint (midpoint cell))
           (cell-dim (dimension cell)))
      (when (<= cell-dim 1)
	(let ((x (aref midpoint 0))
              (y (aref midpoint 1)))
	  (pushnew (cond
                     ((= 0.0 x y) :origin)
                     ((= y top) :top)
                     ((= y bottom) :bottom)
                     ((= x left) :left)
                     ((= x right) :right)
                     ((and (<= 0.15 x 0.26) (<= 0.15 y 0.26)) :cylinder)
                     (t :internal))
		   classifications))))
    classifications))

(defun st-cylinder-boundary-p (cell mesh)
  "Predicate checking if cell is on the cylinder boundary of the correct dimension."
  (member :cylinder
          (patch-classification (patch-of-cell cell mesh)
                                (domain mesh))))

(defun benchmark-domain (&rest args &key (nr-blocks 0) &allow-other-keys)
  (let* ((cube-skel (linearly-transformed-skeleton
                     (skeleton (n-cube 2))
                     :A (diag #d(0.44 0.41))
                     :b #d(0.0 0.0)))
         (skel (apply #'st-cylinder args)))
    (loop for x from 1.0 upto (float nr-blocks 0.0) by 1.0 do
      (skel-add! skel (shift-skeleton cube-skel (double-vec (* x 0.44) 0.0))))
    (make-domain skel
                 :classifiers
                 (list (classify-benchmark-domain :right (* 0.44 (1+ nr-blocks)))))))

;;;; Benchmark problem definition

(defun inflow-velocity (y &key (H 0.41) (Um 0.3))
  "Parabolic inflow profile for y in [0,H] with maximum at 0.3"
  (make-matrix `(,(/ (* 4 um y (- H y)) (expt H 2))
                 0.0)))

(defun benchmark-problem (&rest args
                          &key (viscosity 1.0d-3) (Um 0.3) time-dependent-p
                            (outflow :do-nothing) start-time end-time
                          &allow-other-keys)
  (let* ((domain (apply #'benchmark-domain args))
         (dim (dimension domain)))
    (create-problem
        (:type '<navier-stokes-problem> :domain domain
         :properties
         `(viscosity ,viscosity
            ,@(and time-dependent-p
                   (list :start-time start-time :end-time end-time))))
      (select-on-patch (patch)
        (:d-dimensional
         (when time-dependent-p
           (list (sigma ()
                   (diagonal-sparse-tensor
                    (make-double-vec dim 1.0)))
                 (initial ()
                   (make-double-vec dim 0.0))))
         (viscosity () viscosity)
         (inertia () 1.0)
         (gradp () 1.0) (continuity () 1.0))
        (:origin
         (ecase outflow
           (:dirichlet (constraint-coefficient (1+ dim) 1))
           (:do-nothing (no-slip ()))))
        ((or :cylinder :top :bottom)
         (no-slip ()))
        (:left (prescribed-velocity (x)
                 (inflow-velocity (aref x 1) :Um Um)))
        (:right (when (eq outflow :dirichlet)
                  (prescribed-velocity (x)
                    (inflow-velocity (aref x 1) :Um Um))))))))

(defun schaefer-turek-observe (dim &optional (drag-lift-p t))
  (append *stationary-fe-strategy-observe*
          (mapcar #'watch-velocity-at-point
                  (list (make-double-vec dim .1)))
          (when drag-lift-p
            (list (list (format nil "~{                 c~1D~}" (range<= 1 dim))
                        "~{~19,10,2E~}"
                        #'(lambda (blackboard)
                            (let* ((solution (^ :solution blackboard))
                                   (mesh (mesh solution))
                                   (force (calculate-drag/lift
                                           solution
                                           (rcurry #'st-cylinder-boundary-p mesh)))
                                   ;; In Schaefer-Turek the result is
                                   ;; de-dimensionalized by multiplying
                                   ;; with 2/(rho*Umean^2*D)=500.
                                   ;; Additionally our force calculation
                                   ;; calculates the force exerted by the
                                   ;; obstacle on the fluid which gives a
                                   ;; negative sign.
                                   (scaling -500.0))
                              (loop for k below 2
                                    collect (* scaling (vref force k))))))))))

(defun benchmark-calculation
    (&rest args &key (dim 2) (levels 2) (output 1) &allow-other-keys)
  (storing
    (solve
     (blackboard
      :problem (apply #'benchmark-problem :dim dim args)
      :initial-mesh-refinements 1
      :success-if `(>= :nr-levels ,levels)
      :plot-mesh t :output output
      :observe (schaefer-turek-observe 2)))))

(defun make-stationary-schaefer-turek-demo (dim levels)
  (let ((title "Schaefer-Turek-1996-stationary")
	(short "Solves the benchmark problem defined in [Schaefer-Turek 1996]")
	(long "We solve the stationary Schäfer-Turek benchmark as
described in

  M. Schaefer, S. Turek: 'Benchmark computations of laminar flow around
  cylinder'; in 'Flow Simulation with High-Performance Computers II', Notes
  on Numerical Fluid Mechanics 52, 547-566, Vieweg 1996.

Very precise reference values were first computed in

  Guido Nabh: 'On high order methods for the stationary incompressible
  Navier-Stokes equation' (PhD thesis, University Heidelberg, 1998)

where he obtained the values C_d=5.57953524 for the drag coefficient and
C_l=0.01061894 for the lift coefficient.  c_d and c_l are called c_1 and
c_2 in the calculation below.  The calculations is very simple and uses the
built-in automatical solving where the discretization order is chosen
automatically and the stopping criterion is the number of levels."))
    (let ((demo
	   (make-demo
	    :name title :short short :long long
	    :execute (_ (benchmark-calculation :dim dim :nr-blocks 4
                                               :levels levels
                                               :output 1 :plot t)
                        (plot (^ :solution) :component 'u)))))
      (adjoin-demo demo *articles-demo*)
      (adjoin-demo demo *navier-stokes-demo*))))

(make-stationary-schaefer-turek-demo 2 4)

;;;; Testing

(defun test-schaefer-turek ()

  ;; (new-kernel 2)
  (benchmark-calculation
   :nr-blocks 4 :outflow :do-nothing :viscosity 1.0d-3 :levels 2)
  
  (plot (^ :solution) :component 'u)
  (plot (^ :mesh))
  (plot (^ :solution) :component 'p)

  )

(adjoin-test 'test-schaefer-turek)
;;; (test-schaefer-turek)
