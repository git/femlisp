;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; bratu.lisp - Solve the Bratu problem
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2004 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.application)

;;;; For more information about the Bratu problem you may take a look at
;;;; the recent references "A.Mohsen: A simple solution of the Bratu
;;;; problem" (CMAA 67, 2014, 26-33) or better Mojtaba Hajipour, Amin
;;;; Jajarmi, Dumitru Baleanu: "On the accurate discretization of a highly
;;;; nonlinear boundary value problem", Numer Algor (2018) 79:679–695,
;;;; https://doi.org/10.1007/s11075-017-0455-1

(defvar *u_1/2-observe*
  (list
   (list (format nil "~19@A" "u(midpoint)") "~19,10,2E"
	 #'(lambda (blackboard)
	     (let* ((sol (getbb blackboard :solution))
		    (dim (dimension (mesh (ansatz-space sol))))
		    (val (fe-value sol (make-double-vec dim 0.5))))
	       (vref (aref val 0) 0))))))

(defun bratu-computation (dim &key (a 0.1) (plot t) (time 5.0) (output 1) (base-level 1))
  "Function performing the Bratu demo."
  (let ((fl.iteration::*suggested-nonlinear-solver*
          (make-instance
           '<newton>
           :linear-solver (lu-solver)
           :success-if '(and (> :step 1) (< :defnorm 1e-8))
           :failure-if '(and (> :step 20) (> :step-reduction 1.0) (> :defnorm 1.0e-9)))))
    (storing
      (solve (blackboard
              :problem (bratu-problem dim :a a) :base-level base-level
              :success-if `(> :time ,time) :output output
              :observe (append *stationary-fe-strategy-observe* *u_1/2-observe*)))))
  (when plot (plot (^ :solution))))

(defun make-bratu-demo (dim)
  (let ((title (format nil "bratu-~Dd" dim))
	(short (format nil "Solve the Bratu problem in ~DD" dim))
	(long "Solves the Bratu problem -Delta u +e^u =0.  During the
iteration the value in the midpoint of the domain is tracked.  There are
two solutions for this nonlinear problem.  First we calculate the 'lower
solution', then we calculate the upper solution."))
    (let ((demo
            (make-demo
             :name title :short short :long long
             :execute (_ (user-input "Press <Enter> for calculating the lower solution.")
                         (bratu-computation dim :a 0.1 :plot t)
                         (user-input "~&~%Press <Enter> again for calculating the upper solution.")
                         (bratu-computation dim :a 5.0 :plot t))
             :test-input (format nil "~%~%"))))
      (adjoin-demo demo *cdr-demo*))))

;;; 1D and 2D problem
(make-bratu-demo 1)
(make-bratu-demo 2)
;; (make-bratu-demo 3)

(defun bratu-tests ()
  ;; the following lines should approximate the upper solutions
  (bratu-computation 1 :a 4.0 :output :all :time 2.0)
  (bratu-computation 2 :a 5.0 :output 2)
  
  ;; Cannot approximate the upper solution in 3D in a satisfactory manner.
  ;; For C=1,a=7, and initial values as a*prod_i sin^2(pi*x_i), an
  ;; approximation on base-level 1 works, but has difficulties with
  ;; refining.  Also an approximation starting from the initial values
  ;; works on base-level 2, but has difficulties refining. Step-control
  ;; does not help, and the Newton iteration converges only lately if
  ;; allowed to perform many steps.  It would be good to examine this
  ;; further, because it hints at a insufficiency of hierarchical solving
  ;; of nonlinear problems which should be understood better.  On the other
  ;; hand, the Bratu problem is difficult to solve for 3D as reported in
  ;; the second reference from above.
  
  ;; (bratu-computation 3 :a 7.0 :time 20 :output :all)
  ;; (plot (^ :solution))
  )

;;; (fl.application::bratu-tests)
(fl.tests::adjoin-test 'bratu-tests)
