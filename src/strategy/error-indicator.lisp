;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; error-indicator.lisp - Refinement indication
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.strategy)

(defclass <refinement-indicator> ()
  ((top-level
    :reader top-level :initform nil :initarg :top-level
    :documentation "If top-level is set, no refinement beyond top-level is indicated.")
   (from-level
    :reader from-level :initform nil :initarg :from-level
    :documentation "Below from-level, global refinement is used.  If
    from-level=NIL, regular global refinement is indicated, if no other
    indication is available.")
   (block-p
    :reader block-p :initform nil :initarg :block-p
    :documentation
    "If block-p is T, all children of a parent cell are indicated for refinement together.")
   (ensure-mesh-quality-p
    :reader ensure-mesh-quality-p
    :initform t :initarg :ensure-mesh-quality-p
    :documentation
    "If ensure-mesh-quality-p is T, the indicator ensures that the difference
of mesh widths of neighboring cells does not become larger than a factor of
4."))
  (:documentation "An error indicator can appear as first argument in the
@function{indicate} which works on a blackboard.  Often, it will use
quantities computed by an error estimator before."))

(defgeneric indicate (indicator blackboard)
  (:documentation "Puts a list of cells to be refined on the blackboard."))

(defmethod indicate :before ((indicator <refinement-indicator>) blackboard)
  "Initialize the refinement hash table on the blackboard."
  (setf (getbb blackboard :refinement-table)
        (make-hash-table)))

(defgeneric cell-rule (indicator cell)
  (:documentation "Determines the refinement rule of the cell.")
  (:method (indicator cell)
    "Default nothing is refined."
    nil))

(defmethod indicate ((indicator <refinement-indicator>) blackboard)
  "The standard way is to indicate cells depending on the result of cell-rule."
  (with-slots (from-level top-level) indicator
    (with-items (&key mesh refinement-table) blackboard
      (declare (type <hierarchical-mesh> mesh))
      (let ((top-level (top-level mesh)))
        (awhen (top-level indicator)
          (setf top-level (min top-level (1- (top-level indicator)))))
        (loop for level from 0 upto top-level do
          (doskel (cell (cells-on-level mesh level))
            (unless (children cell mesh)
              (awhen (acond
                      ((and (numberp from-level) (< level from-level))
                       :regular)
                      ((cell-rule indicator cell) it)
                      ((null from-level) :regular))
                (setf (gethash cell refinement-table) it)))))))))

(defmethod indicate :after ((indicator <refinement-indicator>) blackboard)
  "Postprocess the refinement indication."
  (with-items (&key mesh refinement-table) blackboard
    (when (ensure-mesh-quality-p indicator)
      ;; ensure that there are no enormous jump in refinement between
      ;; neighboring cells
      (doskel (cell mesh :dimension :highest :where :surface)
	(unless (gethash cell refinement-table)
	  (when (some #'(lambda (side)
			  (whereas ((children (children side mesh)))
			    (some (rcurry #'refined-p mesh) children)))
		      (boundary cell))
	    (setf (gethash cell refinement-table) t)))))
    (when (block-p indicator)
      ;; refine blocks of cell
      (let ((reftable refinement-table))
        (maphash (lambda (cell rule)
                   (assert (regular-rule-p rule))
                   (whereas ((parent (parent cell mesh)))
                     (loop for child across (children parent mesh)
                           unless (refined-p child mesh) do
                             (setf (gethash child refinement-table)
                                   :regular))))
                 reftable)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; uniform-refinement-indicator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <uniform-refinement-indicator> (<refinement-indicator>)
  ()
  (:documentation "Marks all cells for refinement."))

(defmethod cell-rule ((indicator <uniform-refinement-indicator>) cell)
  :regular)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; cell-rule-indicator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <cell-rule-indicator> (<refinement-indicator>)
  ((cell-rule :initarg :cell-rule))
  (:documentation "Calls @slot{cell-rule} on a cell for determinining its refinement rule."))

(defmethod cell-rule ((indicator <cell-rule-indicator>) cell)
  (funcall (slot-value indicator 'cell-rule) cell))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; isotropizing-refinement-indicator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <isotropizing-indicator> (<refinement-indicator>)
  ()
  (:documentation "Marks anisotropic product cells in such a way that they
  become isotropic."))

(defmethod cell-rule ((indicator <isotropizing-indicator>) cell)
  (error "NYI"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; largest-eta-indicator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <largest-eta-indicator> (<refinement-indicator>)
  ((fraction :reader fraction :initform nil :initarg :fraction)
   (pivot-factor :reader pivot-factor :initform 0.2 :initarg :pivot-factor))
  (:documentation "Puts the fraction of the cells with the largest error
contributions in the refinement table.  Note that a fraction of 1.0 yields
uniform refinement."))

(defvar *cell->error* nil
  "Internally used: A hash-table mapping cell to the local error.")

(defvar *cell->error-rank* nil
  "Internally used: A hash-table mapping cell to a number indicating its
  rank with respect to the estimated error.")

(defvar *pivot-max-error* nil
  "Internally used: The maximal local error contribution.")

(defmethod cell-rule ((indicator <largest-eta-indicator>) cell)
  (with-slots (fraction pivot-factor) indicator
    (when (and *cell->error-rank* *cell->error*)
      (let ((n (hash-table-count *cell->error-rank*))
            (my-rank (gethash cell *cell->error-rank*))
            (my-error (gethash cell *cell->error*)))
        (when (or (and my-rank fraction
                       (<= (/ my-rank n) fraction))
                  (and my-error *pivot-max-error* pivot-factor
                       (>= my-error (* *pivot-max-error* pivot-factor))))
          :regular)))))

(defmethod indicate ((indicator <largest-eta-indicator>) blackboard)
  "Marks all cells for refinement which have no parent or for which the
error estimator yields a large eta."
  (let ((eta (getbb blackboard :eta)))
    (let ((sorted (and eta
                       (sort (hash-table-keys eta) #'>
                             :key #'(lambda (key) (abs (gethash key eta))))))
          (error-rank (make-hash-table)))
      ;; populate error-rank
      (loop for cell in sorted
            and rank from 0 do
              (setf (gethash cell error-rank) rank))
      ;; then call next method with this information in special variables
      (let ((*cell->error* eta)
            (*cell->error-rank* error-rank)
            (*pivot-max-error* (and sorted (gethash (first sorted) eta))))
        (call-next-method)))))


;;;; Testing

(defun test-error-indicator ()
  (make-instance '<largest-eta-indicator>)
  )

(fl.tests:adjoin-test 'test-error-indicator)
