;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; rothe-ellsys.lisp - Rothe adaption for elliptic systems
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2005 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.strategy)

(defun ellsys-initial-value-interpolation-coefficients (rothe problem patch coeffs)
  "Return a coefficient list for the initial value interpolation problem to
be solved at the beginning."
  (declare (ignore rothe))
  (let ((initials (get-coefficients coeffs 'FL.ELLSYS::INITIAL))
	(constraints (remove-if-not #'constraint-p coeffs)))
    (if initials
        (cons (let ((n (nr-of-components-of-patch patch problem)))
                (constant-coefficient 'FL.ELLSYS::R
                                      (diagonal-sparse-tensor #m(1.0) n)))
              (append (loop for initial in initials
                            collect (copy-coefficient initial :name 'FL.ELLSYS::F))
                      constraints))
	constraints)))

#+(or)
(defmethod initial-value-interpolation-problem ((rothe <rothe>) (problem <ellsys-problem>))
  "Returns a stationary problem for interpolating the initial value."
  (make-instance
   '<ellsys-problem>
   :domain (domain problem)
   :multiplicity (multiplicity problem)
   :components (components problem)
   :coefficients
   (lambda (patch)
     (ellsys-initial-value-interpolation-coefficients
      rothe problem patch (coefficients-of-patch patch problem)))
   :linear-p t))

(defmethod call-with-ivip (func (problem <ellsys-problem>) rothe)
  (let ((coeffs (coefficients problem)))
    (unwind-protect
         (progn (change-class problem (stationary-problem-class problem)
                              :coefficients (lambda (patch)
                                              (ellsys-initial-value-interpolation-coefficients
                                               rothe problem patch (funcall coeffs patch)))
                              :linear-p t)
                (funcall func))
      (change-class problem (time-dependent-problem-class problem)
                    :coefficients coeffs))))

(defun calculate-multistep-weights (rothe)
  (let ((dt1 (time-step rothe))
        (dt2 (old-time-step rothe))
        (scheme (time-stepping-scheme rothe)))
    (if (or (eql scheme :implicit-euler)
            (null dt2)
            ;; check additionally if dt1/dt2 have very different magnitude
            (< (/ (min dt1 dt2) (max dt1 dt2)) 0.25)
            )
        (values (/ dt1) (- (/ dt1)) 0.0)
        (if (eql scheme :bdf2)
            (let ((alpha (/ (+ (* 2 dt1) dt2)
                            (* dt1 (+ dt1 dt2))))
                  (beta (- (/ (+ dt1 dt2) (* dt1 dt2))))
                  (gamma (/ dt1 dt2 (+ dt1 dt2))))
              (values alpha beta gamma))))))

(defun ellsys-time-step-coefficients (rothe coeffs)
  "Return a coefficient list for the stationary problem to be solved at
each time step."
  (let ((sigmas (get-coefficients coeffs 'FL.ELLSYS::SIGMA)))
    (if sigmas
        (list* (make-instance '<coefficient>
                              :name 'FL.ELLSYS::R :demands ()
                              :eval (lambda (&rest args)  ; must not be memoized!
                                      (multiple-value-bind (alpha beta gamma)
                                          (calculate-multistep-weights rothe)
                                        (declare (ignore beta gamma))
                                        (reduce #'m+ sigmas
                                                :key (lambda (sigma)
                                                       (scal alpha
                                                             (evaluate sigma args)))))))
	       (make-instance '<coefficient>
                              :name 'FL.ELLSYS::F
                              :demands '((:fe-parameters :old-solution :very-old-solution))
                              :eval (lambda (&rest args
                                             &key old-solution very-old-solution
                                             &allow-other-keys)
                                      (let ((old-solution (first old-solution))
                                            (very-old-solution (first very-old-solution)))
                                        (multiple-value-bind (alpha beta gamma)
                                            (calculate-multistep-weights rothe)
                                          (declare (ignore alpha))
                                          (let* ((value (scal (- beta) old-solution))
                                                 (value (if gamma
                                                            (axpy (- gamma) very-old-solution value)
                                                            value)))
                                            (reduce #'m+ sigmas
                                                    :key (lambda (sigma)
                                                           (m* (evaluate sigma args) value))))))))
	       (remove 'FL.ELLSYS::SIGMA coeffs :key #'coefficient-name))
	coeffs)))

#+(or)
(defmethod time-step-problem ((rothe <rothe>) (problem <ellsys-problem>))
  "Returns a stationary problem corresponding to a step of the Rothe
method."
  (make-instance
   '<ellsys-problem>
   :domain (domain problem)
   :components (components problem)
   :multiplicity (multiplicity problem)
   :coefficients
   (lambda (patch)
     (ellsys-time-step-coefficients
      rothe (coefficients-of-patch patch problem)))))

(defmethod call-with-time-step-problem (func (problem <ellsys-problem>) rothe)
  (let ((coeffs (coefficients problem)))
    (unwind-protect
         (progn (change-class problem (stationary-problem-class problem)
                              :coefficients (lambda (patch)
                                              (ellsys-time-step-coefficients
                                               rothe (funcall coeffs patch))))
                (funcall func))
      (change-class problem (time-dependent-problem-class problem)
                    :coefficients coeffs))))

(defun test-rothe-ellsys ()
  
  ;; the same test as in rothe-cdr
  (time
   (loop for i below 1 do
        (print i)
        (let* ((dim 1) (levels 1) (order 4) (end-time 0.1) (steps 64)
               (problem (ellsys-model-problem
                         dim '((u 1))
                         :initial (lambda (x)
                                    (vector (ensure-matlisp
                                             (float #I(sin(2*pi*x[0])) 1.0))))
                         :sigma (diagonal-sparse-tensor (vector (eye 1)))
                         :a (isotropic-diffusion 1 1.0)
                         :r (diagonal-sparse-tensor (vector #m(0.0)))
                         :f (vector #m(0.0))
                         :dirichlet (constraint-coefficient 1 1)))
               (rothe (make-instance
                       '<rothe> :model-time 0.0 :time-step (/ end-time steps)
                       :stationary-success-if `(> :nr-levels ,levels)
                       :success-if `(>= :step ,steps)
                       :output t :plot t
                       :observe (append *rothe-observe* (list (point-observe 0.25))))))
          ;;(time-step-problem rothe problem)
          (let ((result
                 (iterate
                  rothe
                  (blackboard
                   :problem problem :fe-class (lagrange-fe order :nr-comps 1)
                   :plot-mesh nil :output :all
                   ))))
            ;; the correct value is exp(-0.1*4*pi*pi)=0.019296302911016777
            ;; but implicit Euler is extremely bad
            (assert (< (abs (- 2.1669732216e-02  ; value computed by CMUCL for end-time=0.1 and 64 steps
         		 (vref (aref (fe-value (getbb result :solution) #d(0.25))
         			     0)
                               0)))
                       1.0e-12))
      ))))
  )

;;; (test-rothe-ellsys)
(fl.tests:adjoin-test 'test-rothe-ellsys)
