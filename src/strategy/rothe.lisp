;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; rothe.lisp - Discretization for the Rothe method
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2004 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.strategy)

(defvar *model-time-observe*
  (list "    Time" "~8,4F"
        (lambda (blackboard)
          (getbb blackboard :model-time))))

(defvar *rothe-observe*
  (list *step-observe*
	*cpu-time-observe*
        *model-time-observe*)
  "Standard observe quantities for Rothe.")

(defclass <rothe> (<iterative-solver>)
  ((model-time :reader model-time :initarg :model-time :documentation
	       "Current time in the time-stepping scheme.")
   (time-step :reader time-step :initarg :time-step)
   (old-time-step :reader old-time-step :initform nil :initarg :old-time-step
                  :documentation "For use in BDF formula")
   (minimal-time-step :reader minimal-time-step :initarg :minimal-time-step)
   (maximal-time-step :reader maximal-time-step :initarg :maximal-time-step)
   (forced-times :reader forced-times :initform () :initarg :forced-times)
   (scheme :reader time-stepping-scheme :initform :implicit-euler :initarg :scheme
	   :documentation "Time-stepping scheme, e.g. @code{:implicit-euler} or @code{:bdf2}.")
   ;; maybe we'll find something more automatic than the following later
   (stationary-success-if :initform nil :initarg :stationary-success-if)
   (stationary-failure-if :initform nil :initarg :stationary-failure-if)
   (plot :initform nil :initarg :plot)
   (fl.iteration::observe :initform *rothe-observe* :initarg :observe
    :documentation "Providing initform for <iteration> slot."))
  (:documentation "Rothe strategy for time-dependent problems.  The idea of
the Rothe method for solving @math{U_t +A U =f} is to do an ODE
time-stepping scheme in an infinite-dimensional function space.  Therefore,
in every time-step, the solution has to be approximated sufficiently well
in the space variable."))

(defmethod initialize-instance :after ((rothe <rothe>) &key &allow-other-keys)
  (unless (slot-boundp rothe 'maximal-time-step)
    (setf (slot-value rothe 'maximal-time-step)
          (slot-value rothe 'time-step)))
  (unless (slot-boundp rothe 'minimal-time-step)
    (setf (slot-value rothe 'minimal-time-step)
          (slot-value rothe 'time-step))))
          
(concept-documentation
 "Projection to the initial value is done by defining a problem performing
an L^2-projection on the finite element ansatz space.  This opens up the
possibility of handling distributional initial values as well.  The
extraction of the interpolation problem as well as the construction of the
time-stepping problem is problem-specific and therefore contained in
separate files.")

(defgeneric call-with-ivip (func problem rothe)
  (:documentation "Calls @arg{func} in an environment where problem is
  changed to a stationary interpolation problem."))

(defmacro with-ivip ((problem rothe) &body body)
  `(call-with-ivip (lambda () ,@body) ,problem ,rothe))

(defmethod initially ((rothe <rothe>) blackboard)
  (with-items (&key iv-ip-blackboard stationary-solver) blackboard
    (ensure iv-ip-blackboard (blackboard))
    (transfer-bb blackboard iv-ip-blackboard
                 '(:problem :fe-class :mesh :plot-mesh :estimator :indicator))
    (with-items (&key problem success-if failure-if solver) iv-ip-blackboard
      (setf (slot-value problem 'time) (model-time rothe))
      (with-ivip (problem rothe)
        (ensure success-if (slot-value rothe 'stationary-success-if))
        (ensure failure-if (slot-value rothe 'stationary-failure-if))
        (ensure solver stationary-solver)
        (solve iv-ip-blackboard))
      (transfer-bb iv-ip-blackboard blackboard '(:mesh :ansatz-space :solution))))
  (call-next-method))

(defmethod intermediate ((rothe <rothe>) blackboard)
  "Plots the solution initially and after each time step."
  (setf (getbb blackboard :model-time) (model-time rothe))
  (awhen (slot-value rothe 'plot)
    (if (eq it t)
        (plot (getbb blackboard :solution))
        (funcall it blackboard)))
  (call-next-method))

(defgeneric call-with-time-step-problem (func problem rothe)
  (:documentation "Calls @arg{func} in an environment where problem is
  changed to calculate the stationary time-step problem."))

(defmacro with-time-step-problem ((problem rothe) &body body)
  `(call-with-time-step-problem (lambda () ,@body) ,problem ,rothe))

(defmethod next-step ((rothe <rothe>) blackboard)
  "Do one time step by solving the stationary problem."
  (with-items (&key time-step-blackboard step) blackboard
    (ensure time-step-blackboard (blackboard))
    (with-items (&key success-if failure-if fe-class problem mesh
		      ansatz-space solution status)
	time-step-blackboard
      (transfer-bb blackboard time-step-blackboard
                   '(:mesh :fe-class :plot-mesh :problem :ansatz-space :estimator :indicator))
      (ensure success-if (slot-value rothe 'stationary-success-if))
      (ensure failure-if (slot-value rothe 'stationary-failure-if))
      (setf (get-property problem :very-old-solution)
            (get-property problem :old-solution))
      (setf (get-property problem :old-solution)
            (copy (getbb blackboard :solution)))
      (assert (not (eq (get-property problem :old-solution)
                       (get-property problem :very-old-solution))))
      ;; time-step update with time-step control
      (with-time-step-problem (problem rothe)
        (with-slots (model-time time-step minimal-time-step maximal-time-step forced-times)
            rothe
          (setf (slot-value problem 'time) model-time)
          ;; handle forced times
          (let* ((tic (find-if (lambda (tic) (< model-time tic (+ model-time time-step)))
                               forced-times))
                 (old-time-step time-step))
            (when tic
              (setf time-step (- tic model-time)))
            (let ((fl.iteration::*iteration-failure-behavior* :continue))
              (loop do
                (setf solution (make-ansatz-space-vector ansatz-space))
                (copy! (getbb blackboard :solution) solution) ; initial value
                (solve time-step-blackboard)
                (case status
                  (:success (loop-finish))
                  (:failure (cond ((>= (/ time-step 2) minimal-time-step)
                                   (_f / time-step 2)
                                   (dbg :rothe "Halving time step"))
                                  (t (loop-finish)))))
                (setf status nil)))
            (copy! solution (getbb blackboard :solution))
            (incf model-time time-step)
            (setf (slot-value rothe 'old-time-step)
                  time-step)
            (when tic
              ;; reset time step if we had met a forced time
              (setf time-step old-time-step)))
          (case status
            (:failure (setf (getbb blackboard :status) :failure))
            (:success (when (and (<= (* 2 time-step) maximal-time-step)
                                 (zerop (mod step 10)))
                        (dbg :rothe "Increasing time step")
                        (_f * time-step 2)))))))))

(defgeneric initial-value-interpolation-problem (rothe problem)
  (:documentation "Extract a stationary PDE problem for interpolating the initial values
out of the given time-dependent PDE problem @arg{problem}."))

(defgeneric time-step-problem (rothe problem)
  (:documentation "Extract a stationary PDE problem for performing one step of the given Rothe method
out of the given time-dependent PDE problem @arg{problem}."))

;;; Integration within GPS problem solving

(defmethod select-solver ((problem <time-dependent-problem>) blackboard)
  (with-items (&key start-time end-time
                    time-step number-of-time-steps
                    number-of-levels
                    plot observe)
      blackboard
    (ensure start-time (or (start-time problem)
                           0.0))
    (ensure end-time (end-time problem))
    (ensure time-step (and start-time end-time number-of-time-steps
                           (/ (- end-time start-time) number-of-time-steps)))
    (ensure number-of-time-steps
            (and start-time end-time time-step
                 (ceiling (/ (- end-time start-time) time-step))))
    (unless (and start-time time-step number-of-time-steps)
      (error "Start-time, time-step and number-of-time-steps could not be found out by available information.  (You could specify start-time and end-time within the problem or all of the above on the blackboard."))
    (unless number-of-levels
      (error "Sorry - at the moment you have to define the number-of-levels for the spatial mesh on the blackboard."))
    (make-instance
     '<rothe> :model-time start-time :time-step time-step :scheme :bdf2
              :stationary-success-if `(> :nr-levels ,number-of-levels)
              :success-if `(>= :step ,number-of-time-steps)
              :observe (or observe *rothe-observe*)
              :plot plot)))

(defun test-rothe ()
  ;; more test in the problem-adapted files
  (make-instance
   '<rothe> :model-time 0.0 :time-step 0.1
   :stationary-success-if '(> :nr-levels 3)
   :success-if '(>= :step 5)
   :output t :plot t)
  )

;;; (test-rothe)
(fl.tests:adjoin-test 'test-rothe)
