(in-package :ddo-test)

(file-documentation
 "This file assumes that you have compiled a binary mpi-worker in the
directory @path{femlisp:bin} and started it with @code{mpirun -np xx
mpi-worker}, where xx denotes the number of processes.  From a separate CL
with DDO loaded you may then interactively evaluate the commands in this
file.")

(connect-to-mpi-workers)

(ddo)
(ddo (/ 2 0))

(ddo (when (zerop (mpi-rank)) (/ 2 0)))

(ddo (assert (= (mpi-rank) 0)))
(ddo (in-package :ddo-test))
(ddo *package*)

;;; number of workers
(defparameter *n* (lfarm:kernel-worker-count))
(ddo (defparameter *n* (mpi-size))
     *n*)

(ddo
  (setq *debug-show-data* t)
  (dbg-on :distribute)
  (dbg-on :communication)
  (dbg-on :local-synchronize))

;; an experiment with data merging

;; for seeing what happens automatic synchronization is switched-off

(ddo (setq ddo::*synchronize-after-ddo-commands* nil))

(ddo
  (defclass data ()
    ((data :initarg :data))))

(ddo (defparameter *x*
       (make-distributed-object
        (make-instance 'data :data (make-double-float-array 1 10.0))
        (range< 0 (mpi-size))
        '(data))))

(ddo
  (fl.parallel::accessing-exclusively ((changed ddo::*changed-distributed-objects*))
    (net.scipolis.relations::tree-leaves changed)))

(ddo (distributed-data))
(ddo (distributed-slots *x*))
(ddo (synchronize))
(ddo (distributed-data))
(ddo (insert-into-changed *x*))
(ddo (slot-value *x* 'data))
(ddo (when (zerop (mpi-rank))
       (setf (aref (slot-value *x* 'data) 0) 3.0d0)
       (insert-into-changed *x*)))
(ddo (slot-value *x* 'data))
(ddo (let ((*synchronization-merger* #'minimum-id-merger))
       (synchronize)))
(ddo (slot-value *x* 'data))

;; delete *x* on one proc
(ddo (when (zerop (mpi-rank)) (setq *x* nil))
     (sb-ext:gc :full t)
     (distributed-data))

(ddo (synchronize))
(ddo (distributed-data))
(ddo *x*)

;; clean up
(ddo (setq *x* nil)
     (sb-ext:gc :full t)
     (synchronize))
(ddo (distributed-data))

;; some work on more  objects
(ddo (defparameter *x*
       (loop for k below 100
             collect
             (make-distributed-object
              (make-instance 'data :data (vector (coerce k 'double-float)))
              (range< 0 (mpi-size)))))
     (synchronize))

(ddo (distributed-data))

;; forget some objects on some workers
(ddo (setq *x* (remove-if (lambda (x)
                            (= (mod (aref (slot-value x 'data) 0)
                                    (mpi-size))
                               (mpi-rank)))
                          *x*)))

(ddo (mapcar #'local-id *x*))
(ddo (distributed-data))
(ddo (sb-ext:gc :full t)
     (synchronize))

;; for two workers, everything in the distributed data should be empty at this point
(ddo (distributed-data))

(ddo (length *x*)

;; and forget all
(ddo (setq *x* nil)
     (sb-ext:gc :full t)
     (synchronize))
(ddo (distributed-data))

;;; finally, switch synchronization on again
(ddo (setq ddo::*synchronize-after-ddo-commands* t))

;;; check sequential ddo

(ddo- (format t "Hallo!~%"))
(ddo- (asdf:req :femlisp-ddo))

;;; Hard test on exchange communication

(ddo (run-all-clmpi-tests))

