;;; -*- mode: lisp; fill-column: 75; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ddo.lisp - dynamic distributed objects
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2015-
;;; Nicolas Neuss, FAU Erlangen-Nuernberg
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
;;; IN NO EVENT SHALL THE AUTHOR, THE KARLSRUHE INSTITUTE OF TECHNOLOGY,
;;; OR OTHER CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :ddo)

;;; DDO synchronization flags

(defparameter *debug-show-data* nil
  "When T show all data communicated between processors.  This is only reasonable for
toy problems.")

(defvar *communicate-with-all-in-first-synchronization* t
  "This is constantly T at the moment, i.e. since 1.9.2017, and has the
effect that the first communication (which is not large) takes place
between all processors.  Maybe we can relax this later on, but this change
and its possible benefit should be examined very carefully, because we use
it for avoiding deadlocks when errors occur on one processor.")

(defvar *in-synchronization-process* nil
  "This variable is T when inside the synchronization process.  It is used
by DDOs error handling DDO-ABORT, because communicating errors is not safe
in that state.")

(defvar *communication-real-time* nil
  "When non-nil, communication time is recorded.")

(defvar *synchronization-real-time* nil
  "When non-nil, communication time is recorded.")

(defvar *communication-size* nil
  "When non-nil, communication size is recorded.")

(defvar *report-ranks* t
  "List of ranks for which reports are shown.  T means all ranks.")

(defvar *check-distribution-p* nil
  "If set, the distribution is checked at the beginning of each synchronization.")

(defvar *fast-communication-p* t
  "If this variable is T, we introduce special encodings which avoids the
conspack library, but only work for special kind of data.")

(defvar *synchronize-after-ddo-commands* t
  "When non-nil, no synchronize is performed after the commands.
Setting this boolean to nil may lead to a MPI deadlock if errors occur on
only one processor.")

;;; data types and flags for the first communication

(defvar +ulong+ '(unsigned-byte 64))
(defvar +ulong-vec+ `(simple-array ,+ulong+ (*)))

;;; Flags coded as integers for use in the first communication

(defconstant +deleted+ 0)
(defconstant +changed+ 1)
(defconstant +new+ 2)

;;; Tags for DDO communication types (must be positive according to MPI spec)

(defconstant +check-distribution+ 0)
(defconstant +first-communication+ 1)
(defconstant +second-communication+ 2)
(defconstant +ddo-error+ 3)
