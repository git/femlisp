(in-package :ddo-test)

(file-documentation
 "Routines for profiling and testing DDO.")

#| This test may be run interactively as follows (assuming that
  mpi-workers have been started and are waiting for a
  connection):

(lfarm:end-kernel)  ; for closing an existing connection
(connect-to-mpi-workers)
(ddo- (load #p"femlisp:src;ddo;ddo-test.lisp"))
(ddo (dbg-on :ddo-test))
(ddo (dbg-off))
(ddo (setf ddo::*debug-show-data* nil))
(ddo (loop for level upto 5 do (test-2 level 1000)))
|#

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  For profiling communication
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Tests communication by sending random messages between many
;;; processors.  These messages are shaped similarly to what
;;; occurs during the solution of the elasticity homogenization
;;; problem with finite elements of order 5.

(defun random-double-vec (n)
  (lret ((result (make-double-float-array n)))
    (loop for k below n do
      (setf (aref result k) (random 1.0d0)))))

(defun random-messages (level nr-procs &optional (order 5) (ncomps 3))
  (let ((nf (expt 4 level))
        (ne (* 4 (expt 4 level)))
        (nv (* 4 (expt 4 level))))
    (let* ((s (- order 1))
           (uf (* s s ncomps))
           (ue (* s ncomps))
           (uv (* ncomps))
           (total (+ (* 3 nf) (* nf uf)
                     (* 3 ne) (* ne ue)
                     (* 3 nv) (* nv uv))))
      (assert (< total 10e6) () "Messages probably too large")
      (let ((k 0))
        (loop repeat nr-procs
              collect
              (append
               (loop repeat nv
                     collect (list (incf k) (random-double-vec uv)))
               (loop repeat ne
                     collect (list (incf k) (random-double-vec ue)))
               (loop repeat nf
                     collect (list (incf k) (random-double-vec uf)))))))))

(defun clmpi-test (level repetitions)
  (do-sequentially
    (room)
    (gc :full t)
    (room)
    (dbg :ddo-test "Proc: ~D, Level: ~D~%" (mpi-rank) level)
    )
  (loop for k below repetitions do
       (let ((messages (random-messages level (mpi-size))))
         (let ((comm-result
                (ddo::exchange
                 (loop for proc below (mpi-size)
                    for message = (elt messages proc)
                    when (not (= proc (mpi-rank)))
                    nconc (list (list :receive proc)
                                (list :send proc message)))
                 :tag 2
                 :encode #'ddo::second-comm-encode
                 :cleanup #'static-vectors:free-static-vector
                 :decode #'ddo::second-comm-decode)))
           (loop for (from nil object) in comm-result do
                (unless (equalp object (elt messages (mpi-rank)))
                  (format t "Proc ~D: Problem at level=~D&iteration=~D: received bad result from proc ~D~%"
                          (mpi-rank) level k from)
                  (error "Stop")))))))

(defun random-messages-2 (level nr-procs)
  (let ((n (* 3 (expt 4 (1+ level)) 5)))
    (loop repeat nr-procs collect
         (random-double-vec n))))

(defun clmpi-test-2 (level repetitions)
  (do-sequentially
    (room)
    (gc :full t)
    (room)
    (dbg :ddo-test "Proc: ~D, Level: ~D~%" (mpi-rank) level)
    )
  (ignore-errors
    ;; simply decoding/encoding
    (loop for k below repetitions do
         (let ((messages (random-messages-2 level (mpi-size))))
           (loop for proc below (mpi-size)
              for message = (elt messages proc)
              for encoded = (ddo::copy-to-static-vector message)
              for decoded = (copy-seq encoded)
              do
                (assert (equalp message decoded))
                (static-vectors:free-static-vector encoded))))
    ;; then with communication
    (loop for k below repetitions do
         (let ((messages (random-messages-2 level (mpi-size))))
           (let ((comm-result
                  (ddo::exchange
                   (loop for proc below (mpi-size)
                      for message = (elt messages proc)
                      when (not (= proc (mpi-rank)))
                      nconc (list (list :receive proc)
                                  (list :send proc message)))
                   :tag 2
                   :encode #'ddo::copy-to-static-vector
                   :cleanup #'static-vectors:free-static-vector
                   :decode #'copy-seq)))
             (loop for (from nil object) in comm-result do
                  (unless (equalp object (elt messages (mpi-rank)))
                    (format t "Proc ~D: Problem at level=~D&iteration=~D: received bad result from proc ~D~%"
                            (mpi-rank) level k from)
                    (error "Stop"))))))
    t))

(defun random-messages-3 (level nr-procs)
  (let ((n (expt 2 level)))
    (loop repeat nr-procs collect
         (random-double-vec n))))

(defun clmpi-test-3 (level repetitions)
  (do-sequentially
    (room)
    (gc :full t)
    (room)
    (dbg :ddo-test "Proc: ~D, Level: ~D~%" (mpi-rank) level)
    )
  (ignore-errors
    (loop for k below repetitions do
         (let ((messages (random-messages-3 level (mpi-size))))
           (let ((comm-result
                  (ddo::exchange
                   (loop for proc below (mpi-size)
                      for message = (elt messages proc)
                      when (not (= proc (mpi-rank)))
                      nconc (list (list :receive proc)
                                  (list :send proc message)))
                   :tag 2
                   :encode #'ddo::copy-to-static-vector
                   :cleanup #'static-vectors:free-static-vector
                   :decode #'copy-seq)))
             (loop for (from nil object) in comm-result do
                  (unless (equalp object (elt messages (mpi-rank)))
                    (format t "Proc ~D: Problem at level=~D&iteration=~D: received bad result from proc ~D~%"
                            (mpi-rank) level k from)
                    (error "Stop"))))))
    t))

(defun clmpi-test-4 (level repetitions)
  (do-sequentially
    (room)
    (gc :full t)
    (room)
    (dbg :ddo-test "Proc: ~D, Level: ~D~%" (mpi-rank) level)
    )
  (ignore-errors
    (loop for k below repetitions
       do
         (let* ((messages (random-messages-3 level (mpi-size)))
                (comm-result
                 (apply #'mpi-extensions::mpi-waitall-anything
                        (loop for proc below (mpi-size)
                           for message = (elt messages proc)
                           unless (= proc (mpi-rank))
                           collect
                             (mpi-extensions:mpi-isend-anything
                              message proc :tag 1
                              :encode #'ddo::copy-to-static-vector
                              :cleanup #'static-vectors:free-static-vector)
                           and collect
                             (mpi-extensions:mpi-irecv-anything
                              proc :tag 1 :decode #'copy-seq)))))
           (loop for (from nil object) in comm-result do
                (unless (equalp object (elt messages (mpi-rank)))
                  (format t "Proc ~D: Problem at level=~D&iteration=~D: received bad result from proc ~D~%"
                          (mpi-rank) level k from)
                  (error "Stop")))))
    t))

(defun run-all-clmpi-tests ()
  ;; ensure that we do not get too much debugging output
  (dbg-off)
  (setf ddo::*debug-show-data* nil)
  (dbg-on :ddo-test)
  ;; run all tests with different amount of communication
  (loop for func in (list #'clmpi-test #'clmpi-test-2 #'clmpi-test-3 #'clmpi-test-4)
        do
           (loop for level upto 5 do (funcall func level 1000))))

  

