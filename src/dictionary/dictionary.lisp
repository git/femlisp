;;; -*- mode: lisp; fill-column: 75; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; dictionary.lisp - Generic dictionaries
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003-2006 Nicolas Neuss, University of Heidelberg.
;;; Copyright (C) 2006-2011 Nicolas Neuss, KIT Karlsruhe.
;;; Copyright (C) 2011-
;;; Nicolas Neuss, Friedrich-Alexander-Universitaet Erlangen-Nuernberg
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
;;; IN NO EVENT SHALL THE AUTHOR, THE KARLSRUHE INSTITUTE OF TECHNOLOGY,
;;; OR OTHER CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.dictionary)

(file-documentation "Contains implementations of some types of
dictionaries.")

(defgeneric dic-ref (dictionary key)
  (:documentation "Returns the value of the entry for @arg{key} in
@arg{dictionary}.")
  (:method ((table list) key)
    (let ((entry (assoc key table)))
      (values (cdr entry) entry)))
  (:method ((table hash-table) key)
    (gethash key table))
  (:method ((vec vector) index)
    (aref vec index))
  (:method ((func function) key)
    (funcall func key)))

(defgeneric (setf dic-ref) (value dictionary key)
  (:documentation "Sets the entry @arg{key} in @arg{dictionary} to
@arg{value}.")
  (:method (value (table hash-table) key)
    (setf (gethash key table) value))
  (:method (value (vec vector) index)
    (setf (aref vec index) value)))

(defgeneric dic-for-each (function dictionary &key &allow-other-keys)
  (:documentation "Applies @arg{function} to each key-value pair in
  @arg{dictionary}.")
  (:method :around (function dictionary &key parallel &allow-other-keys)
    ;; at the moment, we do not allow nested parallelization...
    (if (and parallel (workers-available-p))
        (with-workers (function)
          (call-next-method #'work-on dictionary :parallel nil))
        (call-next-method)))
  (:method (func (dic list) &key &allow-other-keys)
    (loop for (key . value) in dic
              do (funcall func key value)))
  (:method (func (dic hash-table) &key &allow-other-keys)
    (maphash func dic))
  (:method (func (vec vector) &key &allow-other-keys)
    (loop for i from 0 and x across vec do
      (funcall func i x))))
  
(defgeneric dic-for-each-key (function dictionary &key &allow-other-keys)
  (:documentation "Applies @arg{function} to each key in @arg{dictionary}.")
  (:method (func dic &rest args &key &allow-other-keys)
    (apply #'dic-for-each
           (lambda (key value)
             (declare (ignore value))
             (funcall func key))
           dic args)))

(defgeneric dic-for-each-value (function dictionary &key &allow-other-keys)
  (:documentation "Applies @arg{function} to each value in @arg{dictionary}.")
  (:method (func dic &rest args)
    (apply #'dic-for-each
           (lambda (key value)
             (declare (ignore key))
             (funcall func value))
           dic args)))

(defgeneric dic-remove (key dictionary)
  (:documentation "Removes @arg{key} from @arg{dictionary}.")
  (:method (key (dic hash-table))
    (remhash key dic)))

(defgeneric dic-empty-p (dictionary)
  (:documentation "Tests if @arg{dictionary} is empty.")
  (:method (dic)
    (not (mapper-some (constantly t) dic)))
  (:method ((list list))
    (null list))
  (:method ((dic hash-table))
    (zerop (hash-table-count dic))))

;;; derived
(defgeneric keys (dic)
  (:documentation "Returns a list of all keys of @arg{dic}.")
  (:method (dic)
    (mapper-collect #'dic-for-each-key dic)))

(defgeneric dic-count (dic)
  (:documentation "Count the entries of @arg{dic}.")
  (:method (dic)
    (mapper-count #'dic-for-each-key dic)))

(defmacro dodic ((looping-var dic &key parallel) &body body)
  "Loops through @arg{dic}.  If @arg{looping-var} is an atom
@emph{key}, loop through the keys; if it is a list of the form
@emph{(value)} loop through the values; if it is a list of the form
@emph{(key value)} loop through key and value."
  (let ((key (if (single? looping-var)
                 (gensym)
                 (if (atom looping-var) looping-var (car looping-var))))
        (value (if (atom looping-var)
                   (gensym)
                   (car (last looping-var)))))
    `(block nil
       (dic-for-each
        (lambda (,key ,value)
          ,@(when (atom looping-var) `((declare (ignore ,value))))
          ,@(when (single? looping-var) `((declare (ignore ,key))))
          ,@body)
        ,dic :parallel ,parallel))))

(defun get-key-from-dic (dic)
  "Returns a key from dic if nonempty."
  (unless (dic-empty-p dic)
    (dodic (key dic)
      (return-from get-key-from-dic
        (values key t)))))
      
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Dictionaries and memoization
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass dictionary ()
  ()
  (:documentation "A superclass for user-defined dictionaries."))

(defclass sorted-dictionary (dictionary)
  ()
  (:documentation "A superclass for sorted dictionaries."))

(defgeneric dic-pop (sorted-dic)
  (:documentation "A pop routine for sorted dictionaries or heaps."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; small-cache-dictionary
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass small-cache-dictionary (dictionary)
  ((size :reader size :initform 1 :initarg :size)
   (test :initform 'eql :initarg :test)
   (store :reader store :initform ()))
  (:documentation "Implementation with lists."))

(defmethod initialize-instance :after ((dic small-cache-dictionary) &key &allow-other-keys)
  (assert (plusp (size dic))))

(defmethod dic-ref ((dic small-cache-dictionary) key)
  (quickly
    (with-slots (store test) dic
      (loop for tail on store
	 and prev-tail = nil then tail do
	 (when (slowly ; gets rid of an SBCL optimization note
		 (funcall test (caar tail) key))
	   (when prev-tail
	     (setf (cdr prev-tail) (cdr tail)
		   (cdr tail) store
		   store tail))
	   (return (values (cdar store) t)))))))

(defmethod (setf dic-ref) (value (dic small-cache-dictionary) key)
  (with-slots (store size test) dic
    (loop for tail on store
	  and prev-tail = nil then tail
	  and i from 1 do
	  (when (funcall test (caar tail) key)
	    ;; eliminate entry from store
	    (cond (prev-tail
		   (setf (cdr prev-tail) (cdr tail))
		   (return))
		  (t (return-from dic-ref
		       (setf (cdr tail) value)))))
	  (when (null (cdr tail))
	    (when (= i size)
	      ;; full: drop last entry
	      (if prev-tail
		  (setf (cdr prev-tail) nil)
		  (setf store nil)))
	    (return)))
    (pushnew (cons key value) store)
    value))

(defmethod dic-for-each (func (dic small-cache-dictionary) &rest args)
  (apply #'dic-for-each func (store dic) args))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; sorted-hash-table
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass sorted-hash-table (sorted-dictionary)
  ((test :initform #'eql :initarg :test)
   (store :initform (make-dll))
   (insertion-order :initform :FIFO :initarg :insertion-order)
   (item-store))
  (:documentation "This is a hash-table where new entries are interned
  FIFO-like (insertion-order=:fifo) or
  heap-like (insertion-order=:heap)."))

(defmethod initialize-instance :after
    ((dic sorted-hash-table) &key &allow-other-keys)
  (with-slots (test item-store size) dic
    (setf item-store (make-hash-table :test test))))

(defmethod dic-ref ((dic sorted-hash-table) key)
  (with-slots (store item-store) dic
    (whereas ((item (gethash key item-store)))
      (values (cdr (dli-object item)) t))))

(defmethod (setf dic-ref) (value (dic sorted-hash-table) key)
  (with-slots (store item-store size test insertion-order) dic
    (let ((item (gethash key item-store)))
      (if item
          (setf (cdr (dli-object item)) value)
          (setf (gethash key item-store)
                (funcall (ecase insertion-order
                           (:fifo #'dll-rear-insert)
                           (:heap #'dll-front-insert))
                         (cons key value) store)))))
  value)

(defmethod dic-for-each (func (dic sorted-hash-table) &key (direction :forward) &allow-other-keys)
  (with-slots (store) dic
    (dll-for-each (_ (funcall func (car _) (cdr _))) store direction)))

(defmethod dic-remove (key (dic sorted-hash-table))
  (with-slots (store item-store) dic
    (awhen (gethash key item-store)
      (dll-remove-item it store)
      (remhash key item-store))))

(defmethod dic-empty-p ((dic sorted-hash-table))
  (with-slots (store) dic
    (dll-empty-p store)))

(defmethod dic-pop ((dic sorted-hash-table))
  (with-slots (store item-store) dic
    (destructuring-bind (key . val)
        (dll-pop-first store)
      (remhash key item-store)
      (values key val))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; cache-dictionary
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cache-dictionary ()
  ((size :reader size :initform 1 :initarg :size)
   (test :initform #'eql :initarg :test)
   (store :initform (make-dll))
   (item-store))
  (:documentation "This is a dictionary which can only hold a certain
  number of items.  If more are inserted, only the most recently accessed
  items are kept."))

(defmethod initialize-instance :after ((dic cache-dictionary) &key &allow-other-keys)
  (with-slots (test item-store size) dic
    (assert (or (null size) (plusp size)))
    (setf item-store (make-hash-table :test test :size size))
    ))

(defmethod dic-ref ((dic cache-dictionary) key)
  (with-slots (store item-store) dic
    (whereas ((item (gethash key item-store)))
      ;; when in interior of list, put it at front
      (when (dli-pred item)
	(dll-remove-item item store)
	(dll-front-insert item store))
      (values (cdr (dli-object item)) t))))

(defmethod (setf dic-ref) (value (dic cache-dictionary) key)
  (with-slots (store item-store size test) dic
    (let ((item (gethash key item-store)))
      (cond
        (item
         ;; set value of item
         (setf (cdr (dli-object item)) value)
         ;; ensure that the item is the first
         (when (dli-pred item)
           (dll-remove-item store item)
           (dll-front-insert item store)))
        (t
         (setf (gethash key item-store)
               (dll-front-insert (cons key value) store))
         ;; ensure the size constraint
         (when (and size (> (hash-table-count item-store) size))
           (remhash (car (dll-pop-last store)) item-store))))))
  value)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; computed-value-dictionary
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass computed-value-dictionary ()
  ((keys :reader keys :type sequence :initarg :keys)
   (compute :reader compute :type function :initarg :compute
            :documentation "A (usually memoized) function computing the values")))

(defmethod dic-for-each-key (func (dic computed-value-dictionary)
                             &key &allow-other-keys)
  (for-each func (keys dic)))

(defmethod dic-for-each (func (dic computed-value-dictionary)
                         &key &allow-other-keys)
  (for-each (lambda (key)
              (funcall func key (funcall (compute dic) key)))
            (keys dic)))

(defmethod dic-ref ((dic computed-value-dictionary) key)
  (funcall (compute dic) key))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; tree dictionary for memoization
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass tree-dictionary ()
  ((tree :initform ()))
  (:documentation "For use in global memoization tables for short list keys."))

(defun find-key-in-tree (tree key-list)
  "Find key-list in tree."
  (if (null key-list)
      tree
      (aand (cdr (assoc (first key-list) tree))
            (find-key-in-tree it (rest key-list)))))

(defun insert-key-value-in-tree (tree key-list value)
  (if (null key-list)
      value
      (let ((key1 (first key-list))
            (foundp nil))
        (let ((mapped-tree
                (loop for branch in tree
                      for (key2 . subtree) = branch
                      collect 
                      (cond
                        ((eql key1 key2)
                         (setf foundp t)
                         (cons key2 (insert-key-value-in-tree subtree (rest key-list) value)))
                        (t branch)))))
          (if foundp
              mapped-tree
              (cons (cons key1
                          (insert-key-value-in-tree () (rest key-list) value))
                    mapped-tree))))))

(defmethod dic-ref ((tree-dic tree-dictionary) key)
  (find-key-in-tree (slot-value tree-dic 'tree) key))

(defmethod (setf dic-ref) (value (tree-dic tree-dictionary) key)
  (with-slots (tree) tree-dic
    (setf tree (insert-key-value-in-tree tree key value))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; thread-cached-dictionary
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass thread-cached-dictionary (mutex-mixin)
  ((generator :initarg :generator)
   (global-dic :initform nil)
   (thread-local-dics
    :initform (make-array 0 :adjustable t :initial-element nil)))
  (:documentation "When calling dic-ref from inside a thread, first an
  unlocked access to the thread-local dictionary is done, and only if this
  does not succed, the global dictionary is accessed with a lock."))

(defmethod initialize-instance :after ((dic thread-cached-dictionary) &key &allow-other-keys)
  (with-slots (generator global-dic) dic
    (setf global-dic (funcall generator))))

(defun ensure-thread-local-dic (dic index)
  (with-slots (generator thread-local-dics) dic
    ;; first ensure that the array length is long enough
    (when (>= index (length thread-local-dics))
      (with-mutual-exclusion (dic)
        ;; check again in a safe mode
        (when (>= index (length thread-local-dics))
          (setf thread-local-dics
                (adjust-array thread-local-dics (1+ index) :initial-element nil)))))
    ;; then ensure a thread-local dic
    (unless (aref thread-local-dics index)
      (setf (aref thread-local-dics index)
            (funcall generator)))))

(defmethod dic-ref ((dic thread-cached-dictionary) key)
  (with-slots (global-dic thread-local-dics) dic
    (let ((index (worker-index)))
      (cond
        (index                          ; we are in a worker thread
         (ensure-thread-local-dic dic index)
         (let ((local-dic (aref thread-local-dics index)))
           (or (dic-ref local-dic key)
               (with-mutual-exclusion (dic)
                 (whereas ((value (dic-ref global-dic key)))
                   ;; copy it in the thread-local dic
                 (setf (dic-ref local-dic key) value))))))
        (t ; we are not in a parallel section and may access the global dic without a lock
         (dic-ref global-dic key))))))

(defmethod (setf dic-ref) (value (dic thread-cached-dictionary) key)
  (with-slots (global-dic thread-local-dics) dic
    (let ((index (worker-index)))
      (cond
        (index                          ; we are in a worker thread
         (with-mutual-exclusion (dic)
           (let ((value2 (dic-ref global-dic key)))
             (unless (eql value value2)
               (when value2
                 (error "A global value does already exist, and destructive
             overwriting would be necessary which could make the local
             dictionaries inconsistent with the global one.  Please check
             your program logic, what you really want."))
               (setf (dic-ref global-dic key) value)))
           (ensure-thread-local-dic dic index))
         (let ((local-dic (aref thread-local-dics index)))
           (setf (dic-ref local-dic key) value)))
        (t ; we are not in a parallel section and may access the global dic without a lock
         (setf (dic-ref global-dic key)
               value))))))

(defmethod dic-for-each (func (dic thread-cached-dictionary) &key &allow-other-keys)
  (let ((global-dic (slot-value dic 'global-dic)))
    (with-mutual-exclusion (global-dic)
      (dic-for-each func global-dic))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Memoization
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun memoization-table (size test)
  (make-instance 'thread-cached-dictionary
                 :generator
                 (_ (if size
                        (make-instance (if (< size 100) 'small-cache-dictionary 'cache-dictionary)
                                       :size size :test test)
                        (make-hash-table :test test)))))

(defmacro memoizing-let-transformer (let-type key-vals &body body)
  (let* ((key-vals (mapcar (lambda (entry)
                             (if (symbolp entry)
                                 (list entry entry)
                                 entry))
                           key-vals))
         (keys (mapcar #'first key-vals)))
    `(,let-type ,key-vals
                (memoizing-evaluate (lambda ,keys
                                      (declare (ignorable ,@keys))
                                      ,@body)
                          (list ,@(mapcar #'first key-vals))))))

(defmacro memoizing-let (key-vals &body body)
  `(memoizing-let-transformer let ,key-vals ,@body))
  
(defmacro memoizing-let* (key-vals &body body)
  `(memoizing-let-transformer let* ,key-vals ,@body))

(defmacro with-memoization ((&key dictionary type size id (test ''equal)) &body body)
  "Sets up a memoization environment consisting of a table, and a captured
symbol @symbol{memoizing-let} memoizing its body depending on the
arguments.  Example of usage:
@lisp
  (with-memoization (:size 4 :id 'test)
    (defun test (n)
      (memoizing-let ((k (* 2 n)))
	(sleep 1)
	(* k k))))
@end lisp"
  (declare (ignore type))
  (with-gensyms (dic memo-id)
    `(let ((,memo-id ,id)
           (,dic ,(or dictionary `(memoization-table ,size ,test))))
       (labels ((memoizing-evaluate (evaluator args)
                  (or (dic-ref ,dic args)
                      (progn
                        (dbg :memoize "Memoizing: id=~A, args=~A" ,memo-id args)
                        (with-mutual-exclusion (,dic)
                          (or (dic-ref ,dic args)
                              (setf (dic-ref ,dic args)
                                    (apply evaluator args)))))))
                (memoizing (evaluator)
                  (lambda (&rest args)
                    (memoizing-evaluate evaluator args))))
         (declare (ignorable (function memoizing)))
         ,@body))))

;;;; Testing

(defun test-dictionaries ()
  
  (let ((dic (make-instance 'tree-dictionary)))
    (loop for (a b c) in '((1 2 3) (1 3 4) (2 3 5))
          do (setf (dic-ref dic (list a b)) c))
    (slot-value dic 'tree)
    (dic-ref dic '(1 1)))

  (with-memoization (:size 4 :id 'test)
    (flet ((test (n)
             (memoizing-let ((k (* 2 n)))
               (sleep 1)
               (* k k))))
      (test 4)))

  (let ((table (make-instance 'sorted-hash-table :insertion-order :heap)))
    (setf (dic-ref table 'a) 1)
    (setf (dic-ref table 'b) 2)
    (setf (dic-ref table 'c) 3)
    (dic-ref table 'b)
    (dic-for-each (_ (format t "~A->~A~%" _1 _2)) table
                  :direction :forward))
  
  (keys (make-hash-table))
  (let ((dic (make-instance 'small-cache-dictionary :size 2)))
    (describe dic)
    (setf (dic-ref dic 1) 1)
    (describe dic)
    (print (dic-ref dic 1))
    (describe dic)
    (setf (dic-ref dic 2) 4)
    (describe dic)
    (print (dic-ref dic 2))
    (describe dic)
    (print (dic-ref dic 1))
    (describe dic)
    (setf (dic-ref dic 3) 9)
    (describe dic)
    (print (dic-ref dic 1))
    (describe dic)
    (dic-for-each (lambda (key value)
                    (print (cons key value)))
                  dic)
    (keys dic))

  (print
   (macroexpand '(with-memoization ()
		  (flet ((test (n)
                           (memoizing-let ((k (* 2 n)))
                             (sleep 1)
                             (* k k))))))))
  (with-memoization ()
    (flet ((test (n)
	     (memoizing-let (n (l (* 3 n)))
	       (sleep 1)
	       (* n l))))
      (time (test 5))
      (time (test 5))))

  (with-dbg (:memoize)
    (with-memoization (:type :local :size 2 :id 'test-memoization)
      (flet ((test (n)
               (memoizing-let ((k (* 2 n))
                               (l (* 3 n)))
                 (sleep 1)
                 (* k l))))
        (time (test 5))
        (time (test 5)))))

  (dic-for-each-value #'print #(1 2 3))

  (with-memoization (:type :local :size 2 :id 'test-memoization)
    (flet ((test (n)
	     (memoizing-let ((k (* 2 n))
			     (l (* 3 n)))
	       (sleep 1)
	       (* k l))))
      (time (test 5))
      (time (test 5))))
  
  (with-memoization (:size 2)
    (flet ((test (n)
	     (memoizing-let* ((k (* 2 n))
                              (l (* k n)))
	       (sleep 1)
	       (* k l))))
      (time (test 5))
      (time (test 5))))

  (let* ((count 0)
         (f (with-memoization (:type :global :id 'test)
              (memoizing (lambda (k)
                           (incf count)
                           (* k k))))))
    (funcall f 10)
    (loop repeat 10 do
      (bordeaux-threads:make-thread
       (lambda ()
         (sleep (random 1.0))
         (funcall f (random 10)))))
    (sleep 2.0)
    count)

  (let ((dic (make-instance 'thread-cached-dictionary :generator (_ (make-hash-table :test 'equal))))
        (n 100))
    (with-workers
        ((lambda ()
           (loop for k below n do
             (when (or (null (worker-index))
                       (evenp (+ k (worker-index))))
               (dbg :memoize "Worker ~A inserting ~D" (worker-index) k) 
               (setf (dic-ref dic k) (* k k))))))
      (loop repeat (or (worker-count) 1) do (work-on)))
    (time
     (with-workers
         ((lambda ()
            (loop for k below 100 do (dic-ref dic k))))
       (loop repeat 10000 do (work-on)))))
  )

;;; (test-dictionaries)
(fl.tests:adjoin-test 'test-dictionaries)
