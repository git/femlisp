;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; compressed.lisp - Compressed sparse storage scheme
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2006 Nicolas Neuss, University of Karlsruhe.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.matlisp)

(def-sealable-class compressed-pattern ()
  ((sizes :initarg :sizes :reader sizes
	  :documentation "Vector of matrix sizes, at the moment only length
2 is allowed here.  The first is the dimension which is not compressed, the
second is the dimension which gets compressed.")
   (orientation :reader orientation :initform :row :type (member :row :column) :initarg :orientation
		:documentation "Denotes if rows or columns are compressed.")
   (starts :reader starts :initarg :starts
	   :documentation "Vector with start indices of compressed columns/rows.")
   (indices :reader indices :initarg :indices
	    :documentation "Vector with compressed row/column indices.")
   (offsets :reader offsets :initarg :offsets :initform nil
	    :documentation "Vector of offsets.  This is only non-nil, if
the pattern supports identification."))
  (:documentation "A compressed sparse pattern.  Note: we use int32 vectors
for @slot{starts} and @slot{indices}, such that they do not have to be
copied for a call to the alien sparse solvers."))

(defmethod initialize-instance :after
    ((pattern compressed-pattern) &key ((:pattern pattern-list)) &allow-other-keys)
  "This is a more elaborate compressed-pattern constructor.  A sparse
matrix of the form @math{ | * 0 0 0 a | | 0 a 0 0 0 | } can be described by
its sizes as a vector \(dimension of non-compressed, dimension/compressed)
together with the pattern '( ((* . 0) (a . 4)) ((a . 1)) ). Here, * means a
non-identified value.  Other symbols can be used to identify entries."
  ;; handle pattern parameter
  (when pattern-list
    (when (some (lambda (name)
		  (and (slot-boundp pattern name)
		       (slot-value pattern name)))
		'(starts indices offsets))
      (error "Either pattern or starts/indices/offsets should be given."))
    ;; initialize slots from pattern parameter
    (with-slots (sizes starts indices offsets) pattern
      (unless (= (length pattern-list) (aref sizes 0))
	(error "First entry of sizes and the length of the pattern do not match."))
      (multiple-value-setq (starts indices offsets)
	(loop with table = (make-hash-table)
	      and offset = 0
	      for row in pattern-list
	      do (assert (reduce #'< row :initial-value -1 :key #'cdr)) 
	      appending (mapcar #'cdr row) into indices
	      summing (length row) into nr-entries
	      appending
	      (mapcar (lambda (entry)
			(cond ((eq (car entry) '*)
			       (prog1 offset (incf offset)))
			      ((gethash (car entry) table))
			      (t (prog1
				     (setf (gethash (car entry) table) offset)
				   (incf offset)))))
		      row)
	      into offsets
	      collect nr-entries into starts
	      finally (return (values (cons 0 starts) indices offsets))))))
  ;; ensure that all slots are of the correct type
  (with-slots (sizes starts indices offsets orientation) pattern
    (assert (= (length starts) (1+ (aref sizes 0))))
    (assert (= (length indices) (number-of-nonzero-entries pattern)))
    (setq starts (coerce starts 'int-vec))
    (setq indices (coerce indices 'int-vec))
    (setq sizes (coerce sizes 'fixnum-vec))
    (when offsets 
      (assert (= (length indices) (length offsets)))
      (setq offsets (coerce offsets 'int-vec)))))

(defgeneric transposed-pattern (pattern)
  (:documentation "Transpose a sparse matrix pattern.")
  (:method ((pattern compressed-pattern))
      (with-slots (sizes starts indices orientation) pattern
        (assert (= 2 (length sizes)))
        (make-instance 'compressed-pattern
                       :sizes sizes
                       :starts starts :indices indices
                       :orientation (ecase orientation
                                      (:row :column)
                                      (:column :row))))))

(defgeneric number-of-nonzero-entries (pattern)
  (:documentation "Number of nonzero entries of a sparse matrix pattern.")
  (:method ((pattern compressed-pattern))
    (with-slots (starts) pattern
      (elt starts (1- (length starts))))))

(defgeneric number-of-stored-entries (pattern)
  (:documentation "Number of stored entries of a sparse matrix pattern.")
  (:method ((pattern compressed-pattern))
    (aif (offsets pattern)
         (1+ (reduce #'max it))
         (number-of-nonzero-entries pattern))))

(with-memoization (:id 'full-compressed-pattern)
  (defun full-compressed-pattern (nrows ncols &optional (orientation :column))
    "Returns a full compressed pattern."
    (when (eq orientation :column)
      (rotatef nrows ncols))
    (memoizing-let ((nrows nrows) (ncols ncols) (orientation orientation))
      (make-instance
       'compressed-pattern
       :sizes (vector nrows ncols)
       :starts (coerce (loop for i from 0 upto nrows
                          collect (* i ncols))
                       'int-vec)
       :indices (coerce (loop for i from 0 below (* nrows ncols)
                           collect (mod i ncols))
                        'int-vec)
       :orientation orientation))))

;;; for backward compatibility
(defun full-ccs-pattern (nrows ncols)
  (full-compressed-pattern nrows ncols :column))
(defun full-crs-pattern (nrows ncols)
  (full-compressed-pattern nrows ncols :row))

(with-memoization (:id 'compressed-eye-pattern)
  (defun compressed-eye-pattern (nrows ncols &optional (orientation :row))
    (memoizing-let ((nrows nrows) (ncols ncols) (orientation orientation))
      (when (eq orientation :column)
        (rotatef nrows ncols))
      (let ((n (min nrows ncols)))
        (make-instance
         'compressed-pattern
         :sizes (vector nrows ncols)
         :starts
         (coerce (loop for row upto nrows
                       for i = 0 then (+ i (if (<= row ncols) 1 0))
                       collect i)
                 'int-vec)
         :indices (coerce (loop for k below n collect k)
                          'int-vec)
         :offsets (make-int-vec n)
         :orientation orientation)))))

(defmethod in-pattern-p ((pattern compressed-pattern) &rest rest)
  (with-slots (starts indices orientation) pattern
    (destructuring-bind (i j)
        (ecase orientation
          (:row rest)
          (:column (reverse rest)))
      (loop for k from (aref starts i) below (aref starts (1+ i))
           thereis (= j (aref indices k))))))

(def-sealable-class compressed-matrix (<matrix>)
  ((pattern :reader pattern :initarg :pattern :type compressed-pattern
	  :documentation "A compressed pattern."))
  (:documentation "A compressed sparse matrix.  This is an abstract class
which is made concrete by mixing it with a store-vector containing the
entries."))

(defmethod initialize-instance :after ((cm compressed-matrix) &key &allow-other-keys)
  (assert (typep cm 'store-vector))
  (with-slots (store pattern) cm
    (if (slot-boundp cm 'store)
	(assert (= (length (store cm))
		   (number-of-stored-entries (pattern cm))))
	(setf store (zero-vector (number-of-stored-entries pattern) (element-type cm))))))

(inlining
 (defun find-compressed-offset (cm i j)
  "This method may be inefficient with large bandwidths because it ignores
the ordering of indices."
   (with-slots (pattern) cm
     (with-slots (starts indices orientation) pattern
       (when (eq orientation :row) (rotatef i j))
       (position i indices :start (aref starts j) :end (aref starts (1+ j)))))))

(defmethod mref ((cm compressed-matrix) i j)
  (let ((offset (find-compressed-offset cm i j)))
    (if offset
	(aref (store cm) offset)
	(coerce 0 (element-type cm)))))

(defmethod (setf mref) (value (cm compressed-matrix) i j)
  (whereas ((offset (find-compressed-offset cm i j)))
    (setf (aref (store cm) offset) value)))

(defmethod nrows ((cm compressed-matrix))
  (with-slots (sizes orientation) (pattern cm)
    (aref sizes (ecase orientation (:row 0) (:column 1)))))

(defmethod ncols ((cm compressed-matrix))
  (with-slots (sizes orientation) (pattern cm)
    (aref sizes (ecase orientation (:row 1) (:column 0)))))

(defmethod in-pattern-p ((cm compressed-matrix) &rest indices)
  (apply #'in-pattern-p (pattern cm) indices))

(defmethod make-domain-vector-for ((cm compressed-matrix) &optional (multiplicity 1))
  (make-instance (standard-matrix (element-type cm))
		 :nrows (ncols cm) :ncols multiplicity))

(defmethod make-image-vector-for ((cm compressed-matrix) &optional (multiplicity 1))
  (make-instance (standard-matrix (element-type cm))
		 :nrows (nrows cm) :ncols multiplicity))

(with-memoization (:type :global :size 4 :id 'compressed-matrix)
  (defun compressed-matrix (type)
    "Construct a compressed sparse matrix with entries of @arg{type}."
    (memoizing-let ((type type))
      (fl.amop:find-programmatic-class
       (list 'compressed-matrix (store-vector type))))))

(defun make-full-compressed-matrix (nrows ncols &optional (orientation :column))
  (make-instance (compressed-matrix 'double-float)
		 :pattern (full-compressed-pattern nrows ncols orientation)))

(defun make-full-crs-matrix (nrows ncols)
  (make-full-compressed-matrix nrows ncols :row))

(defun compressed-eye (m &optional (n m) (type 'double-float) (orientation :row))
  (lret ((result (make-instance
                  (compressed-matrix type)
                  :pattern (compressed-eye-pattern m n orientation))))
    (setf (vref result 0) 1.0)))

(defmethod transpose ((cm compressed-matrix))
  "A compressed matrix can be transposed easily by transposing its
pattern."
  (with-slots (pattern store) cm
    (make-instance (class-of cm) :pattern (transposed-pattern pattern)
		   :store store)))

(defmethod for-each-entry-and-key ((fn function) (x compressed-matrix))
  (let* ((store (store x))
	 (pattern (pattern x))
	 (sizes (sizes pattern))
	 (starts (starts pattern))
	 (indices (indices pattern))
	 (offsets (offsets pattern))
	 (orientation (orientation pattern)))
    (declare (type (simple-array * (*)) store))
    (declare (type (member :row :column) orientation))
    (declare (type int-vec starts indices))
    (declare (type (or null int-vec) offsets))
    (dotimes (i (aref sizes 0))
      (loop for k of-type int from (aref starts i) below (aref starts (1+ i)) do
	   (let* ((j (aref indices k))
		  (l (if offsets (aref offsets k) k)))
	     (declare (type int j l))
	     (if (eq orientation :row)
		 (funcall fn (aref store l) i j)
		 (funcall fn (aref store l) j i)))))))

(defmethod for-each-key-and-entry-in-row ((fn function) (mat compressed-matrix) i)
  (with-slots (pattern store) mat
    (let ((store store))
      (with-slots (orientation starts offsets indices) pattern
        (assert (eq orientation :row))
        (let ((offsets offsets)
              (indices indices))
          (loop for k of-type int from (aref starts i) below (aref starts (1+ i)) do
            (let* ((j (aref indices k))
                   (l (if offsets (aref offsets k) k)))
              (declare (type int j l))
              (funcall fn j (aref store l)))))))))

(defgeneric compressed->matlisp (cm)
  (:documentation "Converts a compressed matrix into matlisp format.")
  (:method ((cm compressed-matrix))
      (lret ((result (zeros (nrows cm) (ncols cm) (element-type cm))))
        (for-each-entry-and-key
         (lambda (value i j)
           (setf (mref result i j) value))
         cm))))

(defmethod ensure-matlisp ((cm compressed-matrix) &optional type)
  (declare (ignore type))
  (compressed->matlisp cm))

(defun read-compressed-matrix
    (nrows ncols triplet-list
     &key (orientation :row) (element-type 'double-float)
       (indexing :0-based) identify-p
     &allow-other-keys)
  "Read a compressed matrix of type @arg{orientation} from the given list
of triplets of the form (i j Aij).  If identify-p is T, try to identify
entries which are equal."
  (let ((bags (make-array (ecase orientation
                            (:row nrows)
                            (:column ncols))
                          :initial-element ())))
    (multiple-value-bind (select-bag select-pos)
        (ecase orientation
          (:row (values #'first #'second))
          (:column (values #'second #'first)))
      (loop for group
              in (safe-sort (group-by select-bag triplet-list) #'<
                            :key (compose select-bag #'first))
            do
              (let ((k (funcall select-bag (first group))))
                (setf (aref bags (ecase indexing (:0-based k) (:1-based (1- k))))
                      (safe-sort group #'< :key select-pos))))
      (let ((numbers (loop for bag across bags
                           appending (mapcar #'third bag)))
            offsets)
        (when identify-p
          ;; reduce the pattern and the store by identifying numbers
          (let ((table (make-hash-table :test 'eql)))
            (setq offsets
                  (loop with offset = -1
                        for x in numbers
                        collect (ensure (gethash x table) (incf offset))))
            (setq numbers
                  (loop for x in numbers
                        for old-offset = nil then offset
                        for offset = (gethash x table)
                        when (or (null old-offset)
                                 (> offset old-offset))
                          collect x))))
        (let ((pattern
                (make-instance
                 'compressed-pattern
                 :sizes (vector nrows ncols)
                 :starts (coerce (cons 0 (partial-sums (map 'list #'length bags))) 'int-vec)
                 :indices (coerce
                           (loop for bag across bags
                                 nconcing
                                 (mapcar (lambda (entry )
                                           (let ((k (funcall select-pos entry)))
                                             (ecase indexing (:0-based k) (:1-based (1- k)))))
                                         bag))
                           'int-vec)
                 :offsets (and offsets (coerce offsets 'int-vec))
                 :orientation orientation)))
        (make-instance (compressed-matrix element-type)
                       :pattern pattern
                       :store (coerce numbers `(simple-array ,element-type (*)))))))))

(defmethod read-matrix ((class (eql 'compressed-matrix)) entries
                        &rest keyword-args &key nrows ncols)
  (apply #'read-compressed-matrix
         nrows ncols entries keyword-args))

(defmethod show ((A compressed-matrix) &key (stream t))
  (format stream "~A has the entries:~%" A)
  (for-each-entry-and-key
   (lambda (value row col)
       (format stream "~D ~D ~A~%" row col value))
   A)
  (when (offsets (pattern A))
    (format stream "Offsets/identifications are used in this matrix.~%"))
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; GEMM!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod copy ((A compressed-matrix))
  (lret ((result (make-instance
                  (compressed-matrix (element-type A))
                  :pattern (pattern A))))
    (copy! A result)))

(defmethod m*-product-instance ((A compressed-matrix) (x vector))
  (zero-vector (nrows A) (array-element-type x)))

(defun compressed-gemm! (alpha x y beta z x-transposed y-transposed)
  "If calculations with compressed matrices become more common, this
routine must be optimized."
  (unless (= beta 1) (scal! beta z))
  (let ((ny (nrows y))
        (nz (nrows z))
        (multiplicity (ncols z))
        (storey (store y))
        (storez (store z)))
    (declare (type positive-fixnum ny nz multiplicity))
    (flet ((i+ (i k)
             (declare (type positive-fixnum i k))
             (the positive-fixnum (+ i k)))
           (i* (i k)
             (declare (type positive-fixnum i k))
             (the positive-fixnum (* i k))))
      (declare (inline i+ i*))
      (labels ((zref (j l)
                 (aref storez (i+ j (i* l nz))))
               ((setf zref) (value j l)
                 (setf (aref storez (i+ j (i* l nz))) value))
               (yref (j l)
                 (aref storey (if y-transposed
                                  (i+ l (i* j ny))
                                  (i+ j (i* l ny)))))
               (base-op (entry i j)
                 (declare (type fixnum i j))
                 (dotimes (l multiplicity)
                   (incf (zref i l)
                         (* alpha entry (yref j l))))))
        (declare (inline zref (setf zref) yref))
        (for-each-entry-and-key
         (lambda (xc i j)
           (if x-transposed
               (base-op xc j i)
               (base-op xc i j)))
         x))))
  z)

(defmethod gemm-nn! (alpha (x compressed-matrix) (y standard-matrix) beta (z standard-matrix))
  (compressed-gemm! alpha x y beta z nil nil))
(defmethod gemm-nt! (alpha (x compressed-matrix) (y standard-matrix) beta (z standard-matrix))
  (compressed-gemm! alpha x y beta z nil t))
(defmethod gemm-tn! (alpha (x compressed-matrix) (y standard-matrix) beta (z standard-matrix))
  (compressed-gemm! alpha x y beta z t nil))
(defmethod gemm-tt! (alpha (x compressed-matrix) (y standard-matrix) beta (z standard-matrix))
  (compressed-gemm! alpha x y beta z t t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; GESV!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun umfpack-solver ()
  (when fl.start::*umfpack-library* 'fl.alien::umfpack))

(defun superlu-solver ()
  (when fl.start::*superlu-library* 'fl.alien::superlu))

(defvar *default-cm-solver*
  (or (umfpack-solver)
      (superlu-solver))
  "Default solver for the CM format.  At the moment this can be UMFPACK or
SuperLU.")

(defmethod gesv! ((mat compressed-matrix) (vec standard-matrix))
  "Solve the system by calling an external sparse solver."
  ;;(break)
  (if *default-cm-solver*
      (with-slots (sizes starts indices orientation)
	  (pattern mat)
	(let ((nrows (aref sizes 1))
	      (ncols (aref sizes 0)))
	  (assert (= nrows (nrows vec)))
	  (let ((error-code
                  (funcall *default-cm-solver*
                           nrows ncols (number-of-nonzero-entries (pattern mat))
                           starts indices (store mat)
                           (ncols vec) (store vec) (store vec)
                           (ecase orientation (:column 0) (:row 1)))))
            (unless (zerop error-code)
              (error "External direct solver ~A terminated with error code ~D.~%~@[~A~]"
                     *default-cm-solver* error-code
                     (when (and (eq *default-cm-solver* (umfpack-solver))
                                (= error-code 1))
                       "For UMPACK this means a singular matrix.")))
	  vec)))
      (gesv! (compressed->matlisp mat) vec)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Test direct solvers on the CM scheme
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun fd-laplace-matrix-triplets (dim n &optional (sample 1.0))
  "Short version of generating an arbitrary-dimensional FD discretization
of the Laplace operator.  n can be a number or a vector of dim numbers."
  (let ((n (etypecase n
             (number (make-fixnum-vec dim n))
             (vector
              (assert (= (length n) dim))
              n))))
    (flet ((ivec->i (ivec)
             (loop for i across ivec
                   for nc across n 
                   for offset = 1 then (* offset nc)
                   for s = i then (+ s (* i offset))
                   finally (return s))))
      (let ((m (reduce #'* n))
            (diag (scal (* 2.0 dim) sample))
            (triplets ()))
        (multi-for (vec (make-fixnum-vec dim 0)
                        (map 'fixnum-vec #'1- n))
          (let ((index (ivec->i vec)))
            (push (list index index diag) triplets)
            (loop for i across vec
                  for nc across n
                  and offset = 1 then (* offset nc)
                  do
                     (when (plusp i)
                       (push (list index (- index offset) (scal -1.0 sample))
                             triplets))
                     (when (< i (1- nc))
                       (push (list index (+ index offset) (scal -1.0 sample))
                             triplets)))))
        (values (nreverse triplets) m)
        ))))

(defun fd-laplace-matrix (dim n &rest key-args &key (sample 1.0) &allow-other-keys)
  "Short version of generating an arbitrary-dimensional FD discretization
of the Laplace operator."
  (multiple-value-bind (triplets m)
      (fd-laplace-matrix-triplets dim n sample)
    (apply #'read-matrix 'compressed-matrix triplets
           :nrows m :ncols m
           :element-type (upgraded-array-element-type (type-of sample))
           (sans key-args :element-type))))

(defun five-point-stencil-matrix (nx ny &key (orientation :row) with-constraint-p)
  "Generate a CM matrix for the five-point stencil on a grid with the
given number of active grid points in each dimension.

If @arg{with-constraint-p} is true, a matrix corresponding to the
five-point discretization of a Neumann problem with an average zero
constraint is generated.

This routine is a specialized version of @function{fd-laplace-matrix}.  It
is several times faster, but can only handle the 2D stencil.  However, it
has also the possibility to add the constraint equation which was observed
to be a problem for direct sparse solvers."
  (declare (type (integer 0 10000000) nx ny))
  (let* ((inner-dofs (* nx ny))
         (nrows (+ inner-dofs (if with-constraint-p 1 0)))
	 (ncols nrows)
	 (nnz (+ (- (* 5 inner-dofs) (* 2 (+ nx ny)))  ; inner part
                 (if with-constraint-p
                     (* 2 inner-dofs)
                     0)))
         (constraint-index (and with-constraint-p inner-dofs))
         ;; the following are filled during generation of the matrix
	 (starts (zero-vector (1+ nrows) '(signed-byte 32)))
	 (indices (zero-vector nnz '(signed-byte 32)))
	 (store (zero-vector nnz 'double-float))
	 (row 0) (pos 0))
    (declare (type (integer 0 10000000) row)
             (type (integer 0 100000000) pos)
             (type (simple-array (signed-byte 32) (*)) starts))
    (flet ((connect (l value)
             (setf (aref store pos) value)
             (setf (aref indices pos) l)
             (incf pos)))
      (let ((shifts (list (- nx) -1 0 1 nx)))
        (dotimes (j ny)
          (declare (type (integer 0 10000) j))
          (dotimes (i nx)
            (declare (optimize speed))
            (assert (= row (+ i (* j nx))))
            (setf (aref starts row) pos)
            (let ((flags (list (plusp j) (plusp i) :diagonal (< i (1- nx)) (< j (1- ny)))))
              (loop with diag-value = (- 4.0 (if with-constraint-p
                                                 (float (count nil flags) 1.0)
                                                 0.0))
                    for flag in flags
                    and shift of-type (integer -100000 100000) in shifts
                    when flag do
                      (if (zerop shift)
                          (connect row diag-value)
                          (connect (+ row shift) -1.0))))
            (when with-constraint-p
              (connect constraint-index 1.0))
            (incf row))))
      (when with-constraint-p
        (assert (= row constraint-index))
        (setf (aref starts row) pos)
        (loop for k below inner-dofs do
          (connect k 1.0))))
      (assert (= pos nnz))
      (setf (aref starts nrows) pos)
    ;; return matrix
    (make-instance
     (compressed-matrix 'double-float)
     :pattern (make-instance 'compressed-pattern
                             :sizes (vector nrows ncols)
			     :starts starts
                             :indices indices
			     :orientation orientation)
     :store store)))

(defparameter *crs-test-matrix-entries*
  '((0 0 0.6666666666666666)
    (0 1 0.16666666666666666)
    (0 2 -0.41666666666666663)
    (0 3 -0.10416666666666666)
    (0 10 0.1666666666666666)
    (0 12 -0.10416666666666666)
    (0 17 -0.25)
    (0 18 -0.06249999999999999)
    (0 24 -0.06249999999999998)
    (1 0 0.16666666666666666)
    (1 1 0.6666666666666666)
    (1 2 -0.10416666666666664)
    (1 3 -0.41666666666666663)
    (1 17 -0.0625)
    (1 18 -0.25)
    (2 0 -0.24999999999999994)
    (2 1 -0.062499999999999986)
    (2 2 0.6666666666666666)
    (2 3 0.16666666666666663)
    (2 10 -0.06249999999999997)
    (2 12 0.1666666666666666)
    (3 0 -0.0625)
    (3 1 -0.25)
    (3 2 0.16666666666666666)
    (3 3 0.6666666666666666)
    (4 4 1.0)
    (5 5 1.0)
    (6 6 1.0)
    (7 7 1.0)
    (8 8 1.0)
    (9 9 1.0)
    (10 0 0.16666666666666663)
    (10 2 -0.1041666666666666)
    (10 10 0.6666666666666665)
    (10 12 -0.41666666666666663)
    (10 17 -0.06249999999999999)
    (10 24 -0.25000000000000006)
    (11 11 1.0)
    (12 0 -0.06249999999999999)
    (12 2 0.16666666666666663)
    (12 10 -0.25)
    (12 12 0.6666666666666666)
    (13 13 1.0)
    (14 14 1.0)
    (15 15 1.0)
    (16 16 1.0)
    (17 0 -0.41666666666666663)
    (17 1 -0.10416666666666666)
    (17 10 -0.10416666666666666)
    (17 17 0.6666666666666666)
    (17 18 0.16666666666666663)
    (17 24 0.16666666666666663)
    (18 0 -0.10416666666666664)
    (18 1 -0.41666666666666663)
    (18 17 0.16666666666666666)
    (18 18 0.6666666666666666)
    (19 19 1.0)
    (20 20 1.0)
    (21 21 1.0)
    (22 22 1.0)
    (23 23 1.0)
    (24 0 -0.10416666666666663)
    (24 10 -0.41666666666666663)
    (24 17 0.16666666666666663)
    (24 24 0.6666666666666666)))

(defparameter *crs-test-matrix*
  (read-compressed-matrix
   25 25 *crs-test-matrix-entries*)
  "CRS Matrix for testing purposes.  Origin: discretization of a
  streamline-diffusion stabilized convection problem.")

(defun direct-solver-performance-test (solver n &key with-constraint-p)
  (when (evenp n) (setf n (1- n)))
  (let* ((cm (five-point-stencil-matrix n n :with-constraint-p with-constraint-p))
	 (rhs (ones (nrows cm) 1)))
    (time
     (let ((*default-cm-solver* solver))
       (progn (gesv! cm rhs)
	      (/ (vref rhs (floor (nrows cm) 2))
		 (expt (1+ n) 2)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Testing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun test-compressed ()
  (compressed-eye-pattern 2 3 :column)
  (compressed-eye 2 2)
  (gemm 2.0 (compressed-eye 3 2) #m((1.0) (2.0)) 1.0 #m((1.0) (2.0) (3.0)))
  (loop for (m n orientation) in '((2 3 :row) (2 3 :column)
                                   (3 2 :row) (3 2 :column))
        collect
        (compressed->matlisp (compressed-eye m n 'double-float orientation)))
  (compressed->matlisp (compressed-eye 3 2 'double-float))
  (scal 2.0 (compressed-eye 2))
  (when fl.start::*superlu-library*
    (direct-solver-performance-test 'fl.alien::superlu 1000))
  (when fl.start::*umfpack-library*
    (direct-solver-performance-test 'fl.alien::umfpack 1000))
  ;; SuperLU and UMFPACK are both strangely slow for matrices incorporating
  ;; a nonlocal constraint row/column, although a sparse direct decomposition should
  ;; be straightforward.
  (let ((solver 'fl.alien::umfpack)
        (n 400))
    (when (fboundp solver)
      (direct-solver-performance-test solver n)
      (direct-solver-performance-test solver n :with-constraint-p t)))
  (let ((*print-matrix* t)
        (crs (five-point-stencil-matrix 5 5 :with-constraint-p nil)))
    (show crs))

  (let* ((n 300)
	 (mat (five-point-stencil-matrix n n)))
    (time
     (for-each-entry-and-key (lambda (x i j)
			       (declare (ignore x i j))
			       nil)
			     mat)))
  (make-instance (store-vector 'single-float)
		 :store (zero-vector 1 'single-float))
  (let* ((pattern (make-instance
		   'compressed-pattern :sizes #(1 1)
		   :starts (int-vec 0 1)
		   :indices (int-vec 0)))
	 (cm (make-instance
	       (compressed-matrix 'double-float) :pattern pattern :store #d(2.0)))
	 (rhs #m(1.0)))
    (mref cm 0 0)
    (gesv! cm rhs)
    (transpose cm))

  (assert (= (nr-of-entries (fd-laplace-matrix 2 3))
             (nr-of-entries (five-point-stencil-matrix 3 3))))
  
  (transpose (five-point-stencil-matrix 2 2))
  (make-full-crs-matrix 3 2)
  (assert (eq (full-crs-pattern 2 2)
              (full-crs-pattern 2 2)))
  (make-instance 'compressed-pattern
		 :sizes #(2 2) :orientation :row
		 :pattern '( ((a . 0))  ((a . 1)) ))
  
  (show (five-point-stencil-matrix 2 2))
  
  (show (fd-laplace-matrix 2 2 :sample (eye 1)))
  (show (fd-laplace-matrix 2 2 :sample 1.0))
  
  (let ((cm (read-compressed-matrix
             4 4
             '((2 1 1.0) (2 2 1.0)
               (1 2 2.0)
               (3 0 1.0))
             :identify-p t)))
    (show cm)
    (let ((mat (mrandom 3 4)))
      (assert (mequalp (m*-nt cm mat)
                       (m*-nt (compressed->matlisp cm) mat)))))
  )

;;; (test-compressed)
(fl.tests:adjoin-test 'test-compressed)


