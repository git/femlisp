;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; matrix.lisp - Matrix class and interface
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.matlisp)

(def-sealable-class <matrix> (<vector>)
  ()
  (:documentation "General matrix class."))

(def-sealable-class standard-matrix (<matrix>)
  ((nrows :initarg :nrows :documentation "Number of rows in the matrix")
   (ncols :initarg :ncols :documentation "Number of columns in the matrix"))
  (:documentation "Mixin for dense matrices."))

(def-fgf nrows (mat)
  (:documentation "Number of matrix rows."))
(def-fgf ncols (mat)
  (:documentation "Number of matrix columns."))

(def-fgf mref (A i j)
  (:documentation "Returns the matrix element @code{A[i,j]}."))

(def-fgf (setf mref) (value A i j)
  (:documentation "Writes the matrix element @code{A[i,j]}."))

;;; For these routines, inlining may help a lot.

(def-fgf for-each-row-key (func mat &key &allow-other-keys)
  (:documentation "Loop through row keys."))

(def-fgf for-each-col-key (func mat &key &allow-other-keys)
  (:documentation "Loop through column keys."))

(def-fgf for-each-key-in-row (func mat row-key)
  (:documentation "Loop through col-keys in row."))

(def-fgf for-each-key-in-col (func mat col-key)
  (:documentation "Loop through row-keys in column col."))

(def-fgf for-each-entry-in-row (func mat row-key)
  (:documentation "Loop through col-keys in row."))

(def-fgf for-each-entry-in-col (func mat col-key)
  (:documentation "Loop through entries in column col."))

(def-fgf for-each-key-and-entry-in-row (func mat row-key)
  (:documentation "Loop through col-keys and entries in row."))

(def-fgf for-each-key-and-entry-in-col (func mat col-key)
  (:documentation "Loop through row-keys and entries in col."))

(defmacro dorows ((key mat) &body body)
  "Syntax: @slisp{(dorows (key mat) ...)}"
  `(for-each-row-key (lambda (,key) ,@body) ,mat))

(defmacro docols ((key mat) &body body)
  "Syntax: @slisp{(docols (key mat) ...)}"
  `(for-each-col-key (lambda (,key) ,@body) ,mat))

(defmacro dorow ((loop-vars mat row) &body body)
  (multiple-value-bind (key entry func)
      (cond ((atom loop-vars) (values loop-vars nil 'for-each-key-in-row))
            ((single? loop-vars) (values nil (first loop-vars) 'for-each-entry-in-row))
            (t (values (first loop-vars) (second loop-vars) 'for-each-key-and-entry-in-row)))
    `(,func (lambda (,@(when key (list key))
                     ,@(when entry (list entry)))
              ,@body) ,mat ,row)))

(defmacro docol ((loop-vars mat col) &body body)
  (multiple-value-bind (key entry func)
      (cond ((atom loop-vars) (values loop-vars nil 'for-each-key-in-col))
            ((single? loop-vars) (values nil (first loop-vars) 'for-each-entry-in-col))
            (t (values (first loop-vars) (second loop-vars) 'for-each-key-and-entry-in-col)))
    `(,func (lambda (,@(when key (list key))
                     ,@(when entry (list entry)))
              ,@body) ,mat ,col)))

;;;; derived functionality

(defgeneric row-keys (mat)
  (:documentation "All row keys for a matrix.")
  (:method ((mat <matrix>))
    (mapper-collect #'for-each-row-key mat)))

(defgeneric col-keys (mat)
  (:documentation "All column keys for a matrix.")
  (:method ((mat <matrix>))
    (mapper-collect #'for-each-col-key mat)))

(defgeneric keys-of-row (mat key)
  (:documentation "All column keys in the given row for a matrix.")
  (:method ((mat <matrix>) key)
    (mapper-collect #'for-each-key-in-row mat key)))

(defgeneric keys-of-column (mat key)
  (:documentation "All row keys in the given column for a matrix.")
  (:method ((mat <matrix>) key)
    (mapper-collect #'for-each-key-in-col mat key)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; matrix tests
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def-fgf msquare-p (mat)
  (:documentation "Returns T, iff @arg{mat} is square."))

(def-fgf msymmetric-p (mat &key threshold output)
  (:documentation "Returns T, if @arg{mat} is symmetric up to a accuracy in THRESHOLD.
If output is T, the differences to symmetry are reported."))

(defgeneric midentity-p (number &optional threshold)
  (:documentation "Returns T, if @arg{mat} is the identity, i.e. if the
  elementwise difference to the identity is not larger than
  @arg{threshold}."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; matrix reading
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric read-matrix (class entries &key &allow-other-keys)
  (:documentation "Generic interface for reading a sparse matrix.
@arg{class} is a symbol denoting the sparse matrix class with the sole
purpose of allowing dispatch, @arg{entries} is a list of triplets of the
form (row-index column-index element).  Other key arguments can provide
more specifications like the format."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; matrix printing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric show (matrix &key &allow-other-keys)
  (:documentation "Shows the contents of @arg{matrix} in a readable form.")
  (:method :around (matrix &key &allow-other-keys)
    (fl.parallel:with-atomic-output
      (call-next-method)
      matrix))
  (:method (matrix &key (stream t) &allow-other-keys)
    "The default method describes its argument."
    (describe matrix stream)))

(defgeneric display (matrix &key &allow-other-keys)
  (:documentation "Formats the contents of @arg{matrix} in rectangular
  form.")
  (:method (matrix &key (stream t) &allow-other-keys)
    "The default method describes its argument."
    (describe matrix stream)))
  
(defgeneric print-matlab-format (mat)
  (:documentation "Print a matrix in Matlab/Octave format."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; special matrix methods
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; BLAS Level 3
(def-fgf gemm-nn! (a x y b z)
  (:documentation "General matrix-matrix multiplication:
@math{Z <- alpha * X * Y + beta * Z}"))
(def-fgf gemm-nt! (a x y b z)
  (:documentation "General matrix-matrix multiplication:
@math{Z <- alpha * X * Y' + beta * Z}"))
(def-fgf gemm-tn! (a x y b z)
  (:documentation "General matrix-matrix multiplication:
@math{Z <- alpha * X' * Y + beta * Z}"))
(def-fgf gemm-tt! (a x y b z)
  (:documentation "General matrix-matrix multiplication:
@math{Z <- alpha * X' * Y' + beta * Z}"))

;;; LAPACK
(def-fgf getrf! (A &optional ipiv)
  (:documentation "Computes the PA=LU decomposition of @arg{A} which is
stored again in @arg{A}.  @arg{ipiv} can be a pre-allocated vector which
the routine fills with the indices for column pivoting, or NIL which
implies that the routine allocates such a vector itself.  If @arg{ipiv} is
@symbol{:none}, no pivoting is done.  Returns @arg{A} as the first value,
the pivot vector as a second value, and a boolean as the third value
indicating that the decomposition succeeded."))

(def-fgf getrs! (LU b &optional ipiv)
  (:documentation "Solves the PA=LU decomposition specified by @arg{LU} and
@arg{ipiv} for the rhs @arg{b}.  The result is stored in @arg{b}."))

;;; Special
(def-fgf transpose! (x y)
  (:documentation "Sets Y to the transpose of X."))
(defgeneric matrix-transpose-instance (x)
  (:documentation "Returns a zero matrix for storing the transpose of X."))

;;; Matrix manipulation

(def-fgf minject! (x y row-offset col-offset)
  (:documentation "Inject matrix X in matrix Y at the position given by
ROW-OFFSET and COL-OFFSET."))

(def-fgf extended-minject! (x y
                               y-row-off y-col-off
                               x-row-off x-col-off x-row-end x-col-end)
  (:documentation "Inject a part of the matrix X specified by the last four parameters
in matrix Y at the position given by Y-ROW-OFFSET and Y-COL-OFFSET."))

(def-fgf extended-mclear! (x x-row-off x-col-off x-row-end x-col-end)
  (:documentation "Clear the specified part of X."))

(def-fgf mextract! (x y row-offset col-offset)
  (:documentation "Extract matrix X out of matrix Y from the position given
by ROW-OFFSET and COL-OFFSET."))

;;; derived functionality
(def-fgf vector-slice (x offset size)
  (:documentation "Extract a subvector of size @arg{size} out of @arg{x}
starting from position @arg{offset}."))

(def-fgf matrix-slice (x &key from-row from-col nrows ncols)
  (:documentation "Extract a submatrix of size @arg{nrows} @math{times}
@arg{ncols} out of @arg{x} starting from position
@arg{from-row}/@arg{from-col}."))

(def-fgf submatrix (mat &key row-indices col-indices)
  (:documentation "General extraction of submatrices specified
by non-adjacent lists of row- and column indices."))

(def-fgf diagonal (A)
  (:documentation "Extracts the diagonal from matrix @arg{A} as a vector."))

(defgeneric join-horizontal! (result &rest matrices)
  (:documentation "Joins @arg{matrices} horizontally into result.")
  (:method (result &rest matrices)
    (loop with n = (nrows result) and k = 0
       for mat in matrices do
	 (assert (= n (nrows mat)))
	 (minject! mat result 0 k)
	 (incf k (ncols mat))
       finally (assert (= k (ncols result))))
    result))

(defgeneric join-vertical! (result &rest matrices)
  (:documentation "Joins @arg{matrices} vertically into result.")
  (:method (result &rest matrices)
    (loop with n = (ncols result) and k = 0
       for mat in matrices do
	 (assert (= n (ncols mat)))
	 (minject! mat result k 0)
	 (incf k (nrows mat))
       finally (assert (= k (nrows result))))
    result))

(defgeneric join-instance (orientation matrix &rest matrices)
  (:documentation "Compute an instance for storing the join of
  @arg{orientation} applied to matrix and matrices."))

(defun join (orientation &rest matrices)
  "Joins @arg{matrices} either horizontally or vertically depending on
@arg{orientation}.  Due to the call to @function{zeros} this is not yet a
generic function."
  (unless matrices (error "No arguments to join"))
  (apply (ecase orientation
	   (:horizontal #'join-horizontal!)
	   (:vertical #'join-vertical!))
	 (apply #'join-instance orientation matrices)
	 matrices))

;;; Matrix-vector routines

(defgeneric make-domain-vector-for (mat &optional multiplicity))
(defgeneric make-image-vector-for (mat &optional multiplicity))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; General methods
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod matrix-transpose-instance (x)
  (make-instance (class-of x) :nrows (ncols x) :ncols (nrows x)))

(defgeneric transpose (x)
  (:documentation "Transpose the matrix @arg{x}.")
  (:method (x)
    "The default method casts performs a combination of
@function{transpose!} and @function{matrix-transpose-instance}."
    (transpose! x (matrix-transpose-instance x))))

(defun gemm! (alpha x y beta z &optional (job :nn))
  "Dispatches on the optional job argument (member :nn :tn :nt :tt) and
calls the corresponding generic function, e.g. GEMM-NN!."
  (ecase job
    (:nn (gemm-nn! alpha x y beta z))
    (:nt (gemm-nt! alpha x y beta z))
    (:tn (gemm-tn! alpha x y beta z))
    (:tt (gemm-tt! alpha x y beta z))))

(defun gemm (alpha x y beta z  &optional (job :nn))
  "Rewriting of GEMM in terms of GEMM!."
  (gemm! alpha x y beta (copy z) job))

(defgeneric m*-product-instance (x y)
  (:documentation "Allocates an instance for the product of X and Y.")
  (:method (x y)
    (make-instance (class-of y) :nrows (nrows x) :ncols (ncols y))))

(def-fgf m* (x y)
  (:documentation "Multiply X by Y."))
  
(defgeneric m*-tn-product-instance (x y)
  (:documentation "Allocates an instance for the product of X^t and Y.")
  (:method (x y)
    (make-instance (class-of y) :nrows (ncols x) :ncols (ncols y))))

(def-fgf m*-tn (x y)
  (:documentation "Multiply X^t by Y."))

(defgeneric m*-nt-product-instance (x y)
  (:documentation "Allocates an instance for the product of X and Y^t.")
  (:method (x y)
    (make-instance (class-of y) :nrows (nrows x) :ncols (nrows y))))

(def-fgf m*-nt (x y)
  (:documentation "Multiply X by Y^t."))

(defun getrf (x &optional ipiv)
  "Rewriting for GETRF in terms of GETRF!."
  (getrf! (copy x) ipiv))

(defun getrs (lu b &optional ipiv)
  "Rewriting for GETRS in terms of GETRS!."
  (getrs! lu (copy b) ipiv))

(defgeneric gesv! (A b)
  (:documentation "Solves a linear system A X = B for X.")
  (:method (A b)
    "Default method solves LSE via GETRF and GETRS!."
    (multiple-value-bind (LR ipiv info)
        (getrf A)
      (if (numberp info)
          (error "argument A given to GESV! is singular to working machine precision")
          (getrs! LR b ipiv)))))

(defun gesv (A b)
  "Rewriting for GESV in terms of GESV!."
  (gesv! A (copy b)))

(defmethod mat-diff (mat1 mat2)
  (dovec ((entry i j) mat1)
    (whereas ((entry2 (mref mat2 i j)))
      (unless (mzerop (m- entry entry2))
	(format t "(~A,~A) : ~A <--> ~A~%" i j entry entry2)))))

(defgeneric det (mat)
  (:documentation "Returns the determinant of the square matrix @arg{mat}."))

(defun area-of-span (mat)
  "Computes the volume spanned by the columns of @arg{mat}."
  (sqrt (abs (det (m*-tn mat mat)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Submatrices
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def-sealable-class <submatrix> (<matrix>)
  ((matrix :initarg :matrix :documentation "The \"supermatrix\".")
   (row-keys :reader row-keys :initarg :row-keys :type vector
	     :documentation "The row indices of the submatrix.")
   (col-keys :reader col-keys :initarg :col-keys :type vector
	     :documentation "The column indices of the submatrix."))
  (:documentation "Describes an ordered submatrix of a matrix.  Only a
restricted set of operations is allowed for these matrices and element
access is slow.  They are indexed with ordinary integers."))

