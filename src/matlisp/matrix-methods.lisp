;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; vector-methods.lisp - Some matrix methods, mainly the default ones
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2020 Nicolas Neuss, FAU Erlangen-Nuernberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE FAU ERLANGEN-NUERNBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.matlisp)

(defmethod nrows ((mat <matrix>))
  "Default and very inefficient method for computing the number of rows of a
matrix."
  (lret ((n 0))
    (for-each-row-key (_ (incf n)) mat)))

(defmethod ncols ((mat <matrix>))
  "Default and very inefficient method for computing the number of columns of a
matrix."
  (lret ((n 0))
    (for-each-col-key (_ (incf n)) mat)))

(defmethod multiplicity ((vec <matrix>))
  "If @arg{vec} should be a matrix, the multiplicity is the number of
columns by default."
  (ncols vec))
  
(defmethod mref ((x number) (i (eql 0)) (j (eql 0)))
  x)
(defmethod mref ((x vector) i (j (eql 0)))
  (aref x i))
(defmethod mref ((x vector) (i (eql 0)) j)
  (aref x j))
(defmethod mref ((x <vector>) i (j (eql 0)))
  (vref x i))
(defmethod mref ((x <vector>) (i (eql 0)) j)
  (vref x j))

(defmethod (setf mref) (value (x vector) i (j (eql 0)))
    (setf (vref x i) value))
(defmethod (setf mref) (value (x vector) (i (eql 0)) j)
  (setf (vref x j) value))
(defmethod (setf mref) (value (x <vector>) i (j (eql 0)))
  (setf (vref x i) value))
(defmethod (setf mref) (value (x <vector>) (i (eql 0)) j)
  (setf (vref x j) value))

(defmethod for-each-row-key (fn mat &key &allow-other-keys)
  "Default: Assume that the row keys are integers from 0 upto (nrows mat)."
  (dotimes (i (nrows mat))
    (funcall fn i)))

(defmethod for-each-col-key (fn mat &key &allow-other-keys)
  "Default: Assume that the column keys are integers from 0 upto (ncols mat)."
  (dotimes (i (ncols mat))
    (funcall fn i)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; specializations of vector methods
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod vref ((mat <matrix>) (indices list))
  "Vector referencing on matrices is done by default by matrix referencing
a list of indices."
  (apply #'mref mat indices))

(defmethod (setf vref) (value (mat <matrix>) (indices list))
  "Vector referencing on matrices is done by default by matrix referencing
a list of indices."
  (setf (apply #'mref mat indices) value))

(defmethod for-each-entry-and-key (fn (mat <matrix>))
  (for-each-row-key
   (lambda (rk)
     (for-each-key-and-entry-in-row
      (lambda (ck entry)
        (funcall fn entry rk ck))
      mat rk))
   mat))

(defmethod for-each-entry-and-vector-index (func (mat <matrix>))
  (for-each-entry-and-key
   (lambda (entry i j) (funcall func entry (list i j)))
   mat))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; matrix tests
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod msquare-p (mat)
  (= (nrows mat) (ncols mat)))

(defmethod msymmetric-p (mat &key (threshold 0.0) output)
  (declare (ignore output))
  (mzerop (m- mat (transpose mat)) threshold))

;;; BLAS Level 3

(defmacro define-default-gemm! (name At yt)
  (multiple-value-bind (i j)
      (if At (values 'j 'i) (values 'i 'j))
    `(defmethod ,name (alpha A y beta z)
      (unless (= beta 1) (scal! beta z))
      (dovec ((entry i j) A z)
	(dotimes (l (multiplicity z))
	  (setf (mref z ,i l)
		(,name alpha entry (mref y ,@(if yt `(l ,j) `(,j l)))
		       1.0 (mref z ,i l))))))))

(define-default-gemm! gemm-nn! nil nil)
(define-default-gemm! gemm-nt! nil t)
(define-default-gemm! gemm-tn! t nil)
(define-default-gemm! gemm-tt! t t)

(defmethod m* (x y)
  "By default M* is rewritten in terms of GEMM!."
  (gemm-nn! (coerce 1 (scalar-type x)) x y
            (coerce 0 (scalar-type x)) (m*-product-instance x y)))

(defmethod m*-tn (x y)
  "By default, M*-TN is rewritten in terms of GEMM!."
  (gemm-tn! (coerce 1 (scalar-type x)) x y
            (coerce 0 (scalar-type x)) (m*-tn-product-instance x y)))

(defmethod m*-nt (x y)
  "By default, M*-NT is rewritten in terms of GEMM!."
  (gemm-nt! (coerce 1 (scalar-type x)) x y
            (coerce 0 (scalar-type x)) (m*-nt-product-instance x y)))

;;;; Methods for <submatrix>

(defmethod mref ((submat <submatrix>) i j)
  (mref (slot-value submat 'matrix)
	(aref (row-keys submat) i)
	(aref (col-keys submat) j)))

(defmethod (setf mref) (value (submat <submatrix>) i j)
  (setf (mref (slot-value submat 'matrix)
	      (aref (row-keys submat) i)
	      (aref (col-keys submat) j))
	value))

(defmethod vref ((submat <submatrix>) k)
  "A @class{<submatrix>} can be accessed as one-dimensional vector."
  (multiple-value-bind (i j) (floor k (nrows submat))
    (mref submat i j)))

(defmethod (setf vref) (value (submat <submatrix>) k)
  (multiple-value-bind (i j) (floor k (nrows submat))
    (setf (mref submat i j) value)))

(defmethod for-each-entry-and-key (func (mat <submatrix>))
  (dotimes (i (nrows mat))
    (dotimes (j (ncols mat))
      (funcall func (mref mat i j) i j))))

(defmethod nrows ((mat <submatrix>)) (length (row-keys mat)))

(defmethod ncols ((mat <submatrix>)) (length (col-keys mat)))
