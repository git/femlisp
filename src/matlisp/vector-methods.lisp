;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; vector-methods.lisp - Some methods, mainly the default ones
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.matlisp)

(defmethod multiplicity (vec)
  "The default is a multiplicity of 1."
  1)

(defmethod rank ((vec <vector>))
  "Vectors have rank 1."
  1)

(defmethod element-type (vec)
  "Default method returns T."
  t)

(defmethod scalar-type (vec)
  "Default method returns NUMBER."
  'number)

(defmethod entries (vec)
  "Default method"
  (mapper-collect #'for-each vec))
(defmethod entries ((arr array))
  (mapper-collect #'array-for-each arr))
(defmethod entries ((vec <vector>))
  (mapper-collect #'for-each-entry vec))

(defmethod copy (x)
    "This default method for @function{copy} does a destructive copy to an
empty analog of @arg{x}."
    (copy! x (make-analog x)))

(defmethod dot ((x list) (y list))
  (loop for xc in x and yc in y summing (dot xc yc)))
(defmethod dot ((x vector) (y vector))
    (loop for xc across x
          and yc across y summing (dot xc yc)))

(defmethod for-each-key (func vec)
  "The default method calls @function{for-each-entry-and-key}."
  (for-each-entry-and-key
   (lambda (entry &rest keys)
     (declare (ignore entry))
     (apply func keys))
   vec))

(defmethod for-each-entry (func vec)
  "The default method calls @function{for-each-entry-and-key}."
  (for-each-entry-and-key
   (lambda (entry &rest keys)
     (declare (ignore keys))
     (funcall func entry))
   vec))

(defmethod for-each-entry-and-vector-index (func vec)
  "The default method calls @arg{for-each-entry-and-key} which works for
single-indexed objects, i.e. rather general vectors."
  (for-each-entry-and-key
   #'(lambda (entry key) (funcall func entry key))
   vec))

(defmethod axpy (alpha x y)
  (axpy! alpha x (copy y)))
(defmethod axpy ((alpha number) (x number) (y number))
  (+ (* alpha x) y))

(defmethod m+ (x y)
  "Default method uses M+! and COPY."
  (m+! x (copy y)))
(defmethod m+ ((x list) (y list))
    (mapcar #'m+ x y))

(defmethod norm (x &optional (p 2))
  (case p
    (2 (l2-norm x))
    (:inf (linf-norm x))
    (t (lp-norm x p))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; some core functionality is recursively defined for block vectors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod fill! (x s)
  "Recursive definition for FILL! usable for sparse block vectors."
  (dovec ((xc i) x x)
    (setf (vref x i) (fill! xc s))))

(defmethod fill-random! (x s)
  "Recursive definition for FILL-RANDOM! usable for sparse block vectors."
  (dovec ((xc i) x x)
    (setf (vref x i) (fill-random! xc s))))

(defmethod keys ((vec <vector>))
  (mapper-collect #'for-each-key vec))

(defmethod nr-of-entries (obj)
  (lret ((entries 0))
    (for-each-entry (_ (incf entries)) obj)))

(defmethod total-entries (obj &aux (entries 0))
  (dovec (entry obj entries)
    (incf entries (total-entries entry))))

(defmethod mzerop (mat &optional (threshold *mzerop-threshold*))
  "The default method should work for every matrix."
  (dovec (entry mat t)
    (unless (mzerop entry threshold)
      (return nil))))

(defmethod copy! (x y)
  "Recursive definition for COPY! usable for sparse block vectors."
  (dovec ((xc i) x y)
    (setf (vref y i)
	  (copy! xc (vref y i)))))

(defmethod m+! (x y)
  "Recursive definition for M+! usable for sparse block vectors."
  (dovec ((xc i) x y)
    (setf (vref y i)
	  (m+! xc (vref y i)))))

(defmethod scal! (s x)
  (dovec ((xc i) x x)
    (setf (vref x i)
	  (scal! (coerce s (scalar-type xc)) xc))))

(defmethod axpy! (alpha x y)
  "Recursive definition for AXPY! usable for sparse block vectors which return nil if no entry is set."
  (dovec ((xc i) x y)
    (setf (vref y i)
          (let ((yi (vref y i)))
            (if yi
                (axpy! (coerce alpha (scalar-type xc)) xc yi)
                (scal! alpha (copy xc)))))))

(defmethod l2-norm (vec &aux (sum 0))
  "Recursive definition for the l2-norm."
  (dovec (x vec (sqrt sum))
    (incf sum (expt (l2-norm x) 2))))

(defmethod lp-norm (vec (p number) &aux (sum 0))
  "Recursive definition for the lp-norm."
  (dovec (x vec (expt sum (/ 1 p)))
    (incf sum (expt (lp-norm x p) p))))

(defmethod linf-norm (vec &aux (max 0))
  "Recursive definition for the linf-norm."
  (dovec (x vec max)
    (let ((norm-x (linf-norm x)))
      (when (> norm-x max) (setq max norm-x)))))

(defmethod dot (x y)
  (lret ((sum 0))
    (dovec ((xc i) x)
      (incf sum (dot xc (vref y i))))))

(defmethod dot-abs (x y)
  (lret ((sum 0))
    (dovec ((xc i) x)
      (incf sum (dot-abs xc (vref y i))))))

(defmethod mequalp (x y)
  (when (= (total-entries x) (total-entries y))
    (dovec ((xc i) x t)
      (unless (mequalp xc (vref y i))
	(return nil)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Testing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun test-vector-methods ()
  (assert (mequalp (m+! (vector (vector 3.0)) (vector (vector 4.0))) #(#(7.0))))
  (dovec ((entry key) #(A B C))
    (format t "~A ~A~%" entry key))
  )

;;; (test-vector-methods)
(fl.tests:adjoin-test 'test-vector-methods)
