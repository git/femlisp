;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ellsys.lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2006 Nicolas Neuss, University of Karlsruhe.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF KARLSRUHE OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cl-user)

(defpackage "FL.ELLSYS"
  (:use "COMMON-LISP" "FL.MACROS" "FL.UTILITIES" "FL.DEBUG"
        "FL.DICTIONARY"
	"FL.MATLISP" "FL.FUNCTION"
	"FL.MESH" "FL.PROBLEM")
  (:import-from "FL.PROBLEM" "CONSTRAINT")
  (:export "<ELLSYS-PROBLEM>" "<MULTIPHYSICS-PROBLEM>"
           "NR-OF-COMPONENTS"
           "LINEARIZATION"
           "TIME-DEPENDENT-P" "INITIAL-VALUE-PROVIDED-P"
	   "ISOTROPIC-DIFFUSION" "ISOTROPIC-DIFFUSION-COEFFICIENT"
           "ARTIFICIAL-DIFFUSION"
	   "DIAGONAL-REACTION-COEFFICIENT"
	   "ELLSYS-ONE-FORCE-COEFFICIENT"
	   "UNIT-VECTOR-FORCE-COEFFICIENT"
	   "ELLSYS-MODEL-PROBLEM")
  (:documentation "This package contains the problem definition of systems
of second order PDEs.  The system is defined in the following quasilinear
form:

@math{div(-a(x,u_old,\nabla u_old) \nabla u)
 + div(b(x,u_old,\nabla u_old) u) +
 + c(x,u_old,\nabla u_old) \nabla u +
 + r(x,u_old,\nabla u_old) u
= f(x,u_old, \nabla u_old) 
- div(g(x,u_old, \nabla u_old))
- div(a(x,u_old,\nabla u_old) h(x,u_old, \nabla u_old)) }

where @math{u:G \to \R^N}.  Note that the last two terms are introduced in
the variational formulation and imply a natural Neumann boundary condition
@math{\derivative{u}{n} = (g+a h) \cdot n} at boundaries where no Dirichlet
constraints are posed."))

(in-package :fl.ellsys)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; <ellsys-problem>
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <ellsys-problem> (<pde-problem>)
  ()
  (:documentation "Systems of convection-diffusion-reaction equations.  The
coefficients should be vector-valued functions in this case."))

(defclass <multiphysics-problem> (<ellsys-problem> <multiphysics-mixin>)
  ()
  (:documentation "At least for the moment, multiphysics problems are a
  special kind of ellsys problems."))

(defmethod self-adjoint-p ((problem <ellsys-problem>))
  "The problem is assumed to be self-adjoint if there are no first order
coefficients."
  (doskel (patch (domain problem))
    (let ((coeffs (coefficients-of-patch patch problem)))
      (when (or (get-coefficients coeffs 'FL.ELLSYS::B)
                (get-coefficients coeffs 'FL.ELLSYS::C))
        (return-from self-adjoint-p (values NIL T)))))
  (values T T))

(defun time-dependent-p (problem)
  "Tests if problem is time-dependent, either as suggested by its class, or
alternatively by its coefficients."
  (or (typep problem '<time-dependent-problem>)
      (find-coefficient 'FL.ELLSYS::SIGMA problem)))

(defun initial-value-provided-p (problem)
  "Tests if problem is provided with an initial value.  This should be
normally the case for time-dependent problems, but may also be useful for
stationary problems, e.g. for providing a starting guess in an iterative
method."
  (or (typep problem '<time-dependent-problem>)
      (find-coefficient 'FL.ELLSYS::INITIAL problem)))

(defmethod initialize-instance :after ((problem <ellsys-problem>)
                                       &key start-time end-time &allow-other-keys)
  (when (time-dependent-p problem)
    (unless (typep problem '<time-dependent-problem>)
      (ensure start-time (get-property problem :start-time))
      (ensure end-time (get-property problem :end-time))
      (change-class problem (fl.amop:make-programmatic-instance
                             (list '<time-dependent-problem>
                                   (class-of problem)))
                    :start-time start-time :end-time end-time))))

(defmethod fl.problem::classify-problem :after
    ((problem <ellsys-problem>)
     &key start-time end-time &allow-other-keys)
  (let ((properties (properties problem)))
    (when (time-dependent-p problem)
      (unless (typep problem '<time-dependent-problem>)
        (change-class problem (time-dependent-problem-class problem)
                      :start-time (or start-time (getf properties :start-time))
                      :end-time (or end-time (getf properties :end-time)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; New problem definition support macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro define-ellsys-coefficient-macro (name rank)
  (assert (and (symbolp name) (member rank '(0 1 2))))
  (with-gensyms (args body)
    `(defmacro ,name (,args &body ,body)
       `(fl.problem::make-coefficients-for
         ,fl.problem::internal-problem ',',name
         ,fl.problem::internal-patch ',,args
         (lambda ,,args ,@,body)
         ;;,@(make-list ,rank :initial-element fl.problem::internal-position)
         ,@(subseq (list fl.problem::internal-position fl.problem::internal-position-2)
                   0 ,rank)
         ))))

(define-ellsys-coefficient-macro FL.ELLSYS::A 2)
(define-ellsys-coefficient-macro FL.ELLSYS::AI 2)
(define-ellsys-coefficient-macro FL.ELLSYS::B 2)
(define-ellsys-coefficient-macro FL.ELLSYS::C 2)
(define-ellsys-coefficient-macro FL.ELLSYS::R 2)
(define-ellsys-coefficient-macro FL.ELLSYS::F 1)
(define-ellsys-coefficient-macro FL.ELLSYS::G 1)
(define-ellsys-coefficient-macro FL.ELLSYS::H 1)
(define-ellsys-coefficient-macro FL.ELLSYS::INITIAL 1)
(define-ellsys-coefficient-macro FL.ELLSYS::SIGMA 2)
(define-ellsys-coefficient-macro FL.ELLSYS::CONSTRAINT 1)

(register-coefficient-macros
 '<ellsys-problem>
 'a 'ai 'b 'c 'r 'f 'g 'h 'initial 'sigma 'constraint)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Generation of standard ellsys problems
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun isotropic-diffusion (dim values)
  "Returns a sparse diagonal diffusion tensor with isotropic diffusion in
each component.  @arg{values} should be a vector or a number and contains
the amount of diffusion in each component."
  (let ((values (if (numberp values) (vector values) values)))
    (diagonal-sparse-tensor
     (map 'vector #'(lambda (val) (scal val (eye dim))) values))))

(defun isotropic-diffusion-coefficient (dim values)
  (constant-coefficient 'FL.ELLSYS::A (isotropic-diffusion dim values)))

(defun artificial-diffusion (factor power)
  "An isotropic diffusion of size factor*h^power"
  (make-instance
   '<coefficient>
   :name 'FL.ELLSYS::A
   :demands '(:Dphi)
   :eval (lambda (&key Dphi &allow-other-keys)
           (let ((h (norm Dphi :inf))
                 (dim (nrows Dphi)))
             (diagonal-sparse-tensor
              (scal (* factor (expt h power)) (eye dim))
              dim)))))

(defun diagonal-reaction-coefficient (values)
  (constant-coefficient
   'FL.ELLSYS::R
    (diagonal-sparse-tensor
     (map 'vector #'(lambda (val) (if (numberp val) (scal val (ones 1)) val))
	  values))))

(defun ellsys-one-force-coefficient (nr-comps multiplicity)
  (constant-coefficient
   'FL.ELLSYS::F
   (make-array nr-comps :initial-element (ones 1 multiplicity))))

(defun unit-vector-force-coefficient (direction)
  (constant-coefficient
   'FL.ELLSYS::F
   (lret ((result (make-instance '<sparse-tensor> :rank 1)))
     (setf (tensor-ref result direction) (eye 1)))))

(defun ellsys-model-problem
    (domain components
     &key a b c d r f g h (dirichlet nil dirichlet-p)
     initial sigma evp properties derived-class)
  "Generates a rather general elliptic problem on the given domain."
  (when (numberp components)
    (setq components (list (list 'u components))))
  (setq domain (ensure-domain domain))
  (unless dirichlet-p
    (setq dirichlet
	  (let ((nr-comps (loop for comp in components summing
				(if (listp comp) (second comp) 1))))
	    (constraint-coefficient nr-comps 1))))
  (apply #'fl.amop:make-programmatic-instance
	 (remove nil (list (or derived-class '<ellsys-problem>)
			   (and initial sigma '<time-dependent-problem>)))
	 :components components
	 :properties properties
	 :domain domain :patch->coefficients
	 `((:external-boundary
	    ,(when dirichlet (list dirichlet)))
	   (:d-dimensional
	    ,(macrolet ((coefflist (&rest coeffs)
				   `(append
				     ,@(loop for sym in coeffs collect
					     `(when ,sym (list (ensure-coefficient ',sym ,sym)))))))
		       (coefflist a b c d r f g h initial sigma))))
	 (append
	  (when evp (destructuring-bind (&key lambda mu) evp
		      (list :lambda lambda :mu mu))))
	 ))

;;; Testing

(defun test-ellsys ()
  (let ((problem
	 (ellsys-model-problem 
	  2 '(u v)
	  :a (isotropic-diffusion 2 #(1.0 2.0))
	  :b (vector #m((1.0) (0.0)) #m((0.0) (1.0)))
	  :f (vector #m(1.0) #m(1.0))
	  :dirichlet (constraint-coefficient 2 1))))
    (assert (components problem))
    problem)
  )

;;; (test-ellsys)
(fl.tests:adjoin-test 'test-ellsys)
