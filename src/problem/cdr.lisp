;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  cdr.lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cl-user)
(defpackage "FL.CDR"
  (:use "COMMON-LISP" "FL.MACROS" "FL.UTILITIES"
	"FL.MATLISP" "FL.FUNCTION"
	"FL.MESH" "FL.PROBLEM" "FL.ELLSYS")
  (:export "<CDR-PROBLEM>"
	   "SCALAR-DIFFUSION" "IDENTITY-DIFFUSION-TENSOR"
	   "SCALAR-SOURCE" "SCALAR-REACTION"
	   "ENSURE-TENSOR-COEFFICIENT" "ENSURE-VECTOR-COEFFICIENT"
	   "ENSURE-DIRICHLET-COEFFICIENT"
	   "CDR-MODEL-PROBLEM" "CDR-NONLINEAR-RHS-PROBLEM"
	   "BRATU-PROBLEM")
  (:documentation "Defines convection-diffusion-reaction problems"))

(in-package "FL.CDR")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; <cdr-problem>
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <cdr-problem> (<ellsys-problem>)
  ((components :initform '(u) :initarg :components))
  (:documentation "Convection-diffusion-reaction problem."))

(defmethod nr-of-components-of-problem ((problem (eql '<cdr-problem>)) (domain <domain>))
  "A convection-diffusion-reaction equation has only 1 component - at least at the moment."
  1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; New problem definition support
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-coefficient-macro <cdr-problem> diffusion (args &body body)
  `(FL.ELLSYS::A ,args
     (let ((value (progn ,@body)))
       (sparse-tensor
        (list (list (if (numberp value)
                        (scal value (eye ,fl.problem::internal-patch-dimension))
                        value)
                    0 0))))))

(define-coefficient-macro <cdr-problem> FL.CDR::RESISTANCE (args &body body)
  `(FL.ELLSYS::AI ,args
     (let ((value (progn ,@body)))
       (sparse-tensor
        (list (list (if (numberp value)
                        (scal value (eye (embedded-dimension ,fl.problem::internal-patch)))
                        value)
                    0 0))))))

(defvar *streamline-diffusion* 1.0
  "Streamline diffusion parameter for convection problems.  Note that we do
not modify the right-hand side at the moment, so this is more comparable to
an FE upwinding and it is of O(h) only.")

(define-coefficient-macro <cdr-problem> FL.CDR::STREAMLINE-DIFFUSION (args &body body)
  `(FL.CDR::DIFFUSION ,(cons 'Dphi args)
     (let ((value (progn ,@body)))
       (lret* ((v (ensure-matlisp value :column))
               (v1 (gesv Dphi v))
               (nv1 (norm v1))
               (streamline-D (m* v (transpose v))))
         (unless (zerop nv1)
           (scal! (/ (* 2.0 *streamline-diffusion*) nv1)
                  streamline-D))))))

(define-coefficient-macro <cdr-problem> FL.CDR::CONVECTION (args &body body)
  `(list (FL.ELLSYS::C ,args
           (sparse-tensor
            `((,(ensure-matlisp (progn ,@body) :column) 0 0))))
         (FL.CDR::STREAMLINE-DIFFUSION ,args ,@body)))

(define-coefficient-macro <cdr-problem> FL.CDR::CONSERVATIVE-CONVECTION (args &body body)
  `(list (FL.ELLSYS::B ,args
           (sparse-tensor
            `((,(ensure-matlisp (progn ,@body) :column) 0 0))))
         (FL.CDR::STREAMLINE-DIFFUSION ,args ,@body)))

(define-coefficient-macro <cdr-problem> FL.CDR::REACTION (args &body body)
  `(FL.ELLSYS::R ,args
     (sparse-tensor `((,(progn ,@body) 0 0)))))

(define-coefficient-macro <cdr-problem> FL.CDR::SOURCE (args &body body)
  `(FL.ELLSYS::F ,args
     (vector (ensure-matlisp (progn ,@body) :row))))

(define-coefficient-macro <cdr-problem> FL.CDR::CONSTRAINT (args &body body)
  `(FL.ELLSYS::CONSTRAINT ,args
     (let ((value (progn ,@body)))
       (when value
         (values (vector #m(1.0)) (vector (ensure-matlisp value)))))))

(define-coefficient-macro <cdr-problem> FL.CDR::ZERO-CONSTRAINT ()
  `(FL.CDR::CONSTRAINT ()
     (values (vector #m(1.0))
             (vector #m(0.0)))))

;; not yet tested
(define-coefficient-macro <cdr-problem> FL.CDR::DERIVATIVE-SOURCE (args &body body)
  `(FL.ELLSYS::G ,args
     (vector (ensure-matlisp (progn ,@body) :column))))

#+(or)
(defmethod make-coefficients-for ((problem fl.cdr::<cdr-problem>)
				 (coeff (eql 'FL.CDR::SCALAR-NONLINEAR-SOURCE))
				  patch args eval)
  (make-coefficients-for problem 'FL.ELLSYS::NONLINEAR-F patch args
			 (lambda (&rest args)
			   (sparse-tensor 
			    `((,(apply eval args) 0))))))

(define-coefficient-macro <cdr-problem> FL.CDR::SIGMA (args &body body)
  `(FL.ELLSYS::SIGMA ,args
     (sparse-tensor `((,(progn ,@body) 0 0)))))

(define-coefficient-macro <cdr-problem> FL.CDR::INITIAL (args &body body)
  `(FL.ELLSYS::INITIAL ,args
     (vector (ensure-matlisp (progn ,@body) :row))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod dual-problem ((problem <cdr-problem>) cell->rhs)
  "Dual problem of a cdr problem.  At the moment it works only for
selfadjoint problems with the functional of interest being the load
functional."
  (let ((mult (multiplicity problem)))
    (make-instance
     '<cdr-problem> :components '(u) :multiplicity mult
     :domain (domain problem)
     :coefficients
     #'(lambda (cell)
	 (lret ((coeffs (coefficients-of-patch cell problem)))
	   ;; check that problem is self adjoint
	   (when (or (find 'FL.ELLSYS::B coeffs :key #'coefficient-name)
                     (find 'FL.ELLSYS::C coeffs :key #'coefficient-name))
	     (error "NYI")) ; better: change to negative
	   (unless (eq cell->rhs :load-functional)
	     (setf coeffs
		   (append (and cell->rhs (funcall cell->rhs cell))
			   (remove-if (lambda (coeff)
					(member (coefficient-name coeff)
						'(FL.ELLSYS::F 'FL.ELLSYS::G 'FL.ELLSYS::FE-RHS)))
				      coeffs)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Old problem definition support
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun scalar-diffusion (dim value &key compressed-p)
  "Scalar isotropic diffusion as an ellsys coefficient.  value is the
diffusion strength, compressed-p designates the storage form."
  (constant-coefficient
   'FL.ELLSYS::A
   (sparse-tensor
    `((,(scal value (funcall (if compressed-p
                                 #'compressed-eye
                                 #'eye)
                             dim))
                0 0)))))

(defun scalar-source (value)
  (constant-coefficient
   'FL.ELLSYS::F
   (vector (if (standard-matrix-p value)
               value
               (scal value (ones 1))))))

(defun scalar-reaction (value)
  (constant-coefficient
   'FL.ELLSYS::R
   (diagonal-sparse-tensor (vector (scal value (ones 1))))))

(defun ensure-1-component-tensor (value)
  (when (numberp value)
    (setq value (ensure-matlisp value)))
  (when (standard-matrix-p value)
    (setq value (diagonal-sparse-tensor value 1)))
  value)

(defun ensure-tensor-coefficient (name obj)
  (cond ((typep obj '<coefficient>) obj)
	((or (functionp obj) (typep obj '<function>))
	 (make-instance '<coefficient> :name name :demands '(:global)
			:eval #'(lambda (&key global &allow-other-keys)
				  (ensure-1-component-tensor (evaluate obj global)))))
	(t (constant-coefficient name (ensure-1-component-tensor obj)))))

(defun ensure-1-component-vector (value)
  (when (numberp value)
    (setq value (ensure-matlisp value)))
  (when (standard-matrix-p value)
    (setq value (vector value)))
  value)

(defun ensure-vector-coefficient (name obj)
  (cond ((typep obj '<coefficient>) obj)
	((or (functionp obj) (typep obj '<function>))
	 (make-instance '<coefficient> :name name :demands '(:global)
			:eval #'(lambda (&key global &allow-other-keys)
				  (ensure-1-component-vector (evaluate obj global)))))
	(t (constant-coefficient name (ensure-1-component-vector obj)))))

(defun ensure-dirichlet-coefficient (obj)
  (cond ((typep obj '<coefficient>) obj)
	((or (functionp obj) (typep obj '<function>))
	 (make-instance '<coefficient> :name 'FL.PROBLEM::CONSTRAINT :demands '(:global)
			:eval #'(lambda (&key global &allow-other-keys)
				  (let ((result (evaluate obj global)))
				    (values (make-array 1 :initial-element t)
					    (ensure-1-component-vector result))))))
	(t (constant-coefficient
	    'FL.PROBLEM::CONSTRAINT (make-array 1 :initial-element t)
	    (ensure-1-component-vector obj)))))

(defun cdr-model-problem (domain &key (diffusion nil diffusion-p)
			  (source nil source-p) (dirichlet nil dirichlet-p)
			  gamma convection reaction sigma initial
			  evp (multiplicity 1)
			  properties)
  "Generates a convection-diffusion-reaction model problem.  Defaults are
identity diffusion, right-hand-side equal 1, and Dirichlet zero boundary
conditions.  Ordinary function are converted into coefficient functions
depending on a global coordinate.  The first argument can be either a
domain or an integer n which is interpreted as the n-dimensional unit
cube."
  (setq domain (ensure-domain domain))
  (let ((dim (dimension domain)))
    ;; set default values
    (unless diffusion-p (setq diffusion (eye dim)))
    (unless source-p (setq source (if evp
                                      (zeros 1 multiplicity)
                                      (ones 1 multiplicity))))
    (unless dirichlet-p (setq dirichlet (zeros 1 multiplicity)))
    (setq diffusion (ensure-1-component-tensor diffusion))
    (setq convection (ensure-1-component-tensor convection))
    (setq source (ensure-1-component-vector source))
    (setq gamma (ensure-1-component-vector gamma))
    ;;(setq dirichlet (ensure-dirichlet-coefficient dirichlet))
    (when initial (ensure sigma 1.0))
    (fl.amop:make-programmatic-instance
     (cond ((and initial sigma) '(<time-dependent-problem> <cdr-problem>))
	   (evp '(<cdr-problem> <evp-mixin>))
	   (t '<cdr-problem>))
     :properties properties
     :domain domain :components '(u) :patch->coefficients
     `((:external-boundary
	,(when dirichlet `(,(ensure-dirichlet-coefficient dirichlet))))
       (:d-dimensional
	,(append
	  (when diffusion
	    `(,(ensure-tensor-coefficient 'FL.ELLSYS::A diffusion)))
	  (when source
	    `(,(ensure-vector-coefficient 'FL.ELLSYS::F source)))
	  (when convection
	    `(,(ensure-tensor-coefficient 'FL.ELLSYS::B convection)))
	  (when reaction
	    `(,(ensure-tensor-coefficient 'FL.ELLSYS::R reaction)))
	  (when gamma
	    `(,(ensure-vector-coefficient 'FL.ELLSYS::G gamma)))
	  (when initial
	    `(,(ensure-vector-coefficient 'FL.ELLSYS::INITIAL initial)))
	  (when sigma
	    `(,(ensure-tensor-coefficient 'FL.ELLSYS::SIGMA sigma)))
	  )))
     :multiplicity multiplicity)
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Bratu problem
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun bratu-problem (dim &key (C 1.0) (a 0.1))
  "Formulates a Newton linearization for the Bratu problem @math{-Delta u = C e^u}."
  (create-problem (:type '<cdr-problem>
                   :domain (n-cube-domain dim)
                   :components '(u))
    (select-on-patch ()
      (:d-dimensional
       (diffusion () 1.0)
       (reaction (u) (* (- C) (exp u)))
       (source (u) (* C (- 1 u) (exp u)))
       (initial (x)
         (* a (reduce #'* x :key (_ (expt (sin (* pi _)) 2))))))
      (:external-boundary
       (constraint () 0.0)
       (initial ()
         0.0)))))

;;; Testing

(defun test-cdr ()

  (macroexpand '(fl.cdr::reaction (u)
               (* 2 u)))
  
  (let ((problem
          (create-problem  (:type '<cdr-problem>
                            :domain (n-cube-domain 2)
                            :components '(u))
            (select-on-patch ()
              (:d-dimensional
               (list
                (diffusion () 1.0)
                (source (x) (aref x 0))))
              (:external-boundary
               (constraint () 0.0))))))
    (assert problem)
    (describe problem)
    #+(or)  ; fl.iteration is not yet available
    (solve (blackboard :problem problem :solver (fl.iteration::lu-solver)
                       :success-if '(> :step 3) :output :all))
    )
  (assert (get-property (cdr-model-problem 1) 'linear-p))
  (assert (not (get-property (bratu-problem 1) 'linear-p)))
    
  (assert (not (get-property (bratu-problem 2) 'linear-p)))
  (check (domain (cdr-model-problem 2)))
  (let* ((domain (n-cell-domain 1))
	 (problem (cdr-model-problem domain)))
    (describe problem))
  (describe (cdr-model-problem 2 :initial (constantly 1.0)))
  (assert (find-coefficient 'fl.ellsys::initial
                            (fl.cdr::cdr-model-problem 2 :sigma 1.0 :initial 0.0)))

  )

;;; (test-cdr)
(fl.tests:adjoin-test 'test-cdr)

