(in-package :fl.problem)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; defines an embedded language for simplifying problems definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *coefficient-component-offsets* nil
  "This may contain a list of offsets, in which case the coefficient
  function is shifted as a sparse tensor.")

(defgeneric make-coefficients-for (problem name patch demands evaluator
                                   &optional offset1 offset2)
  (:documentation "Generates a coefficient while dispatching on problem and
coefficient name.  May return a single coefficient or a list of several
coefficients."))

(defun extract-from (solution from ncomps &optional scalar-p index)
  "Extracts numbers or subvectors from the solution vector."
  (let ((multiplicity (ncols (vref solution 0))))
    ;; no scalar return value is possible for multiple solutions
    (cond
      (scalar-p (assert (or index (= multiplicity 1)))
		(tensor-ref solution from (or index 0)))
      (t (lret ((result (zeros ncomps (if index 1 multiplicity))))
	   (dotimes (i ncomps)
	     (if index
		 (setf (mref result i 0)
		       (tensor-ref solution (+ from i) index))
		 (dotimes (j multiplicity)
		   (setf (mref result i j)
			 (tensor-ref solution (+ from i) j))))))))))

(defun extract-from-gradient (gradient from ncomps)
  "Extract a numbers or subvectors from the gradient vector."
  (let ((x (vref gradient from)))
    (assert (= 1 (ncols x)) ()
            "Multiplicity >1 is not allowed here - one would have to use
            general tensors here and at several other places.")
    (lret* ((dim (nrows x))
            (result (zeros ncomps dim)))
      (loop repeat ncomps
            for i from 0
            for y = (vref gradient (+ from i)) do
              (dotimes (j dim)
                (setf (mref result i j) (mref y j 0)))))))

(defun derivative-symbol-p (sym)
  (let* ((name (symbol-name sym))
         (grad-p (> (mismatch name "D_" :test #'string-equal) 1)))
    (values grad-p
            (if grad-p
                (find-symbol (subseq name 2) (symbol-package sym))
                sym))))

(defun prepare-coefficient-arguments (components args)
  "Prepares arguments for the given coefficient function."
  (let ((source
	 `(lambda (&key global solution time cell Dphi problem &allow-other-keys)
	   (declare (ignorable global solution time cell Dphi problem))
	   (values
	    ,@(loop
	       for sym in args collecting
	       (let ((name (symbol-name sym)))
		 (cond
		   ((string-equal name "X") 'global)
		   ((string-equal name "TIME") 'time)
		   ((string-equal name "CELL") 'cell)
		   ((string-equal name "DPHI") 'Dphi)
		   ((string-equal name "PROBLEM") 'problem)
		   (t (multiple-value-bind (grad-p sym)
                          (derivative-symbol-p sym)
                        (multiple-value-bind (from ncomps flag)
                            (extraction-information components sym)
                          (if grad-p
                              `(extract-from-gradient (second solution) ,from ,ncomps)
                              `(extract-from (first solution) ,from ,ncomps ,flag))))))))))))
        (fl.debug:dbg :compile "Compiling:~%~S" source)
        (compile nil source)))

(defmethod make-coefficients-for ((problem <pde-problem>) coeff-name patch args eval
                                  &optional offset1 offset2)
  (let* ((components (components-of-patch patch problem))
         (demands
	 (remove-duplicates
	  (mapcar (lambda (sym)
		    (let ((name (symbol-name sym)))
		      (cond
			((string-equal name "X") :global)
			((string-equal name "TIME") :time)
			((string-equal name "CELL") :cell)
			((string-equal name "DPHI") :Dphi)
			((string-equal name "PROBLEM") :problem)
                        (t (multiple-value-bind (grad-p sym)
                               (derivative-symbol-p sym)
                             (unless (find-component components sym)
                               (error "No such solution component: ~A" sym))
                             (if grad-p
                                 '(:fe-parameters (:solution . 1))
                                 '(:fe-parameters (:solution . 0))))))))
		  args)
	  :test 'equalp))
	(prepare-args (prepare-coefficient-arguments
		       (components-of-patch patch problem) args)))
    (list
     (make-instance
      '<coefficient>
      :name coeff-name :dimension (dimension patch)
      :demands demands
      :eval
      (if (null demands)
          (let ((memoized (multiple-value-list (funcall eval))))
            (_ (values-list memoized)))
          (lambda (&rest rest)
            (multiple-value-call
                eval (apply prepare-args rest))))
      :row-offset offset1
      :col-offset offset2))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; problem definition macrology
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; coefficient macros
(defmacro define-coefficient-macro (problem-name coeff-name args &body body)
  `(progn
     (pushnew ',coeff-name (get ',problem-name 'coefficient-macros))
     (defmacro ,coeff-name ,args ,@body)))

(defun register-coefficient-macros (problem-symbol &rest coeff-syms)
  "Register coefficient macros for the given problem"
  (_f union (get problem-symbol 'coefficient-macros) coeff-syms))

(defun coefficient-macros (symbol)
  (get symbol 'coefficient-macros))

(defmacro with-coefficient-macros ((problem-symbol) &body body)
  (let* ((problem-symbol
           (if (and (listp problem-symbol)
                    (eq (first problem-symbol) 'quote))
               (second problem-symbol)
               problem-symbol))
         (coeff-macro-symbols (coefficient-macros problem-symbol)))
    `(macrolet (,@(unless (eql (symbol-package problem-symbol) *package*)
                    (loop for sym in coeff-macro-symbols
                          for local-sym = (intern (symbol-name sym) *package*)
                        unless (eq sym local-sym) do
                          (when (fboundp local-sym)
                            (warn "Fbound coefficient symbol ~A is locally shadowed" sym))
                        and collect `(,local-sym (&rest args) (list* ',sym args)))))
       ,@body)))

#+(or)(macrodynamics:def-dynenv-var **internal-problem-class-name**)

;;; These are symbols for use in the compile-time (or runtime) expansion of
;;; coefficient macros.  They contain symbols which should be available as
;;; local variables.

(defparameter internal-components (gensym "INTERNAL-COMPONENTS"))
(defparameter internal-component (gensym "INTERNAL-COMPONENT"))
(defparameter internal-component-2 (gensym "INTERNAL-COMPONENT-2"))
(defparameter internal-problem (gensym "INTERNAL-PROBLEM"))
(defparameter internal-subproblems (gensym "INTERNAL-SUBPROBLEMS"))
(defparameter internal-subproblem-name (gensym "INTERNAL-SUBPROBLEM-NAME"))
(defparameter internal-subproblem-package (gensym "INTERNAL-SUBPROBLEM-PACKAGE"))
(defparameter internal-domain (gensym "INTERNAL-DOMAIN"))
(defparameter internal-multiplicity (gensym "INTERNAL-MULTIPLICITY"))
(defparameter internal-properties (gensym "INTERNAL-PROPERTIES"))
(defparameter internal-patch (gensym "INTERNAL-PATCH"))
(defparameter internal-patch-dimension (gensym "INTERNAL-PATCH-DIMENSION"))
(defparameter internal-patch-if (gensym "INTERNAL-PATCH-IF"))
(defparameter internal-value (gensym "INTERNAL-VALUE"))
(defparameter internal-position (gensym "INTERNAL-POSITION"))
(defparameter internal-position-2 (gensym "INTERNAL-POSITION-2"))
(defparameter internal-length (gensym "INTERNAL-LENGTH"))
(defparameter internal-length-2 (gensym "INTERNAL-LENGTH-2"))
(defparameter internal-dimension (gensym "INTERNAL-DIMENSION"))

(defmacro create-problem ((&key type domain components (multiplicity 1)
                             subproblems properties &allow-other-keys)
                          &body body)
  "Creates a PDE problem.  @arg{type} is the type of the problem which can
be the name of a problem class or a list of class names.  @arg{domain} is
the domain for this problem, @arg{multiplicity} is the multiplicity of the
solution, e.g. the number of eigenvectors we search for.  In @arg{body},
patch-dependent coefficients should be defined with
@mac{setup-coefficients}.  It is also possible to define patch-dependent
components with @mac{setup-components}, and in the case of multiphysics
problems, subproblems can be switched using @mac{subproblem}."
  `(lret* ((,internal-domain ,domain)
           (,internal-dimension (dimension ,internal-domain))
           (,internal-multiplicity ,multiplicity)
           (,internal-subproblems ,subproblems)
           (,internal-properties ,properties)
           (,internal-problem
            (fl.amop:make-programmatic-instance
             ,type
             :domain ,internal-domain
             :multiplicity ,internal-multiplicity
             :components ,(unless (and (listp components)
                                       (member (car components) '(select-on-patch select-components-on-patch)))
                            components)
             :subproblems ,internal-subproblems
             :properties ,internal-properties)))
     (declare (ignorable ,internal-domain ,internal-dimension
                         ,internal-multiplicity ,internal-subproblems
                         ,internal-problem))
     (with-coefficient-macros (,type)
       (flet ((,internal-patch-if (patch condition)
                (test-condition
                 condition (patch-classification
                            patch ,internal-domain))))
         (declare (ignorable (function ,internal-patch-if)))
         ,(when (and (listp components)
                    (member (car components) '(select-on-patch select-components-on-patch)))
            `(setf (slot-value ,internal-problem 'components)
                   (memoize-1 (lambda (,internal-patch)
                                (macrolet ((select-on-patch (&rest args)
                                             `(select-components-on-patch ,@args)))
                                  ,components)))))
         (setf (slot-value ,internal-problem 'coefficients)
               (memoize-1
                (lambda (,internal-patch)
                  (let* ((,internal-patch-dimension (dimension ,internal-patch))
                         (,internal-components ,components)
                         (,internal-position 0)
                         (,internal-position-2 ,internal-position)
                         (,internal-length (component-length ,internal-components))
                         (,internal-length-2 ,internal-length))
                    (declare (ignorable ,internal-patch-dimension ,internal-components
                                        ,internal-position ,internal-position-2
                                        ,internal-length ,internal-length-2))
                    (remove nil
                            (flatten
                             (append
                              ,@body)))))))))
     ;; classification can work only now after coefficients have been defined
     (classify-problem ,internal-problem)))

(defmacro subproblem ((problem-name component) &body body)
  `(let* ((,internal-subproblem-name ',problem-name)
          (,internal-component ',component))
     (multiple-value-bind (,internal-position ,internal-length)
         (extraction-information ,internal-components ,internal-component)
       (when ,internal-position  ; otherwise ignore unknown component
         (awhen (nr-of-components-of-problem ,internal-subproblem-name ,internal-domain)
           (assert (= ,internal-length it) ()
                   "Length of component ~A does not fit to problem ~A"
                   ,internal-component ,internal-subproblem-name))
         (unless (member (list ,internal-subproblem-name ,internal-component)
                         ,internal-subproblems :test 'equal)
           (error "(~A ~A) not found in subproblems" ,internal-subproblem-name ,internal-component))
         (let ((,internal-component-2 ,internal-component)
               (,internal-position-2 ,internal-position)
               (,internal-length-2 ,internal-length))
           (declare (ignorable ,internal-component-2 ,internal-position-2 ,internal-length-2))
           (with-coefficient-macros (,problem-name)
             ,@body))))))

(defmacro coupling-with ((sym) &rest body)
  `(let ((,internal-component-2 ',sym))
     (multiple-value-bind (,internal-position-2 ,internal-length-2)
         (extraction-information ,internal-components ,internal-component-2)
       (declare (ignore ,internal-length-2))
       (unless ,internal-position-2
         (error "Coupling component ~A not found in components ~A"
                ,internal-component-2 ,internal-components))
       ,@body)))

(defmacro select-components-on-patch ((&optional (patch internal-patch)) &body clauses)
  `(let ((,patch ,internal-patch))
     (declare (ignorable ,patch))
     (cond
       ,@(loop for (pattern components) in clauses collect
               `((,internal-patch-if ,patch ',pattern)
                 ,components)))))

(defmacro select-on-patch ((&optional (patch internal-patch)) &body clauses)
  `(let ((,patch ,internal-patch))
     (declare (ignorable ,patch))
     (cond
       ,@(loop for (pattern . coeffs) in clauses collect
               `((,internal-patch-if ,patch ',pattern)
                 (list ,@coeffs))))))

#|
(in-package :fl.application)

(defclass <multiphysics-problem> (<ellsys-problem>)
  ((subproblems
    :reader subproblems :initarg :subproblems
    :documentation "A vector of subproblems.  Each entry is of the form (problem-class component-symbol).")))

(create-problem (:type '<multiphysics-problem>
                 :domain (n-ball-domain 2)
                 :components '((upc (up (u 2) p) c))
                 :subproblems '((<navier-stokes-problem> up) (<cdr-problem> c))
                 :multiplicity 1)
  ;; Navier-Stokes
  (subproblem (<navier-stokes> up)
    (select-on-patch (patch)
      (:d-dimensional
       (viscosity (Dphi x u)
         (f u x))
       (inertia () 1.0)
       (when (equalp (origin patch) #(0.0 0.0))
         (divpi () 1.0))
       (fl.navier-stokes::continuity)
       (fl.navier-stokes::force (x)
         (vector (- (aref x 1)) (aref x 0))))
      (:external-boundary
       (fl.navier-stokes::no-slip))))
  ;; convection-diffusion
  (subproblem (<cdr-problem> c)
    (select-on-patch ()
      (:d-dimensional
       (fl.cdr::diffusion (x)
         (scal #I"1.0+2.0*x[0]^^2*x[1]^^2"
               (eye dim)))
       (fl.cdr::source () 1.0))
      (:external-boundary
       (fl.cdr::zero-constraint)))))

|#
