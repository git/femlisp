;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; time.lisp - Time-dependent problems
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003 Nicolas Neuss, University of Heidelberg.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.problem)

(defclass <time-dependent-problem> ()
  #+(or)  ; this slot is stored as a property 'time
  ((model-time :accessor model-time :initarg :model-time :documentation
	       "Current model time of the problem to be used in a Rothe method."))
  ((start-time :reader start-time :initform nil :initarg :start-time
               :documentation "The start time of the problem (NIL=unspecified).")
   (end-time :reader end-time :initform nil :initarg :end-time
             :documentation "The end time of the problem (NIL=unspecified)."))
  (:documentation "A mixin which should be used together with a
<PDE-PROBLEM> in a call to MAKE-PROGRAMMATIC-INSTANCE."))

#|  OLD version:
(defclass <time-dependent-problem> (<pde-problem>)
  ((stationary-problem :accessor stationary-problem
    :initarg :stationary-problem :documentation
    "Stationary problem F(u)=A(u)-f=0 for which this problem is the
time-dependent version."))
  (:documentation "The problem d/dt(alpha u) + F(u) = 0, where F(u) is
given by stationary-problem."))
|#

(defmethod self-adjoint-p ((problem <time-dependent-problem>))
  nil)

(defun stationary-problem-class (tdp)
  "Finds the stationary pde problem for the time-dependent problem TDP."
  (let ((pde-problem-classes
          (filter-if #'(lambda (class) (subtypep class '<pde-problem>))
                     (fl.amop:class-direct-superclasses (class-of tdp)))))
    (assert (single? pde-problem-classes) ()
            "More than one stationary PDE problem as superclass.  This has
            never been encountered before and its implications should be
            checked before proceeding.")
    (first pde-problem-classes)))

(defgeneric time-dependent-problem-class (problem)
  (:method ((problem <time-dependent-problem>))
    (class-of problem))
  (:method (problem)
    (fl.amop:find-programmatic-class
     (list (find-class '<time-dependent-problem>) (class-of problem)))))

(defun test-time ()
  (stationary-problem-class (make-instance '<time-dependent-problem>))
  )
