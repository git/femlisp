;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; navier-stokes.lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2007- Nicolas Neuss, University of Karlsruhe.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :cl-user)
(defpackage "FL.NAVIER-STOKES"
  (:use "COMMON-LISP" "FL.MACROS" "FL.UTILITIES" "FL.MATLISP"
	"FL.FUNCTION" "FL.MESH"
	"FL.PROBLEM" "FL.ELLSYS")
  (:export
   "<NAVIER-STOKES-PROBLEM>" "NO-SLIP-BOUNDARY"
   "STANDARD-NAVIER-STOKES-PROBLEM"
   "NAVIER-STOKES-VISCOSITY-COEFFICIENT"
   "NAVIER-STOKES-PRESSURE-AND-CONTINUITY-COEFFICIENT"
   "NAVIER-STOKES-INERTIA-COEFFICIENTS"
   "DRIVEN-CAVITY" "PERIODIC-CAVITY")
  (:documentation "Defines incompressible Navier-Stokes problems as a
special case of general elliptic systems."))

(in-package "FL.NAVIER-STOKES")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; <navier-stokes-problem>
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <navier-stokes-problem> (<ellsys-problem>)
  ()
  (:documentation "Navier-Stokes problem."))

(defmethod nr-of-components-of-problem ((problem (eql '<navier-stokes-problem>)) (domain <domain>))
  (1+ (dimension domain)))

(defmethod shared-initialize
    ((problem <navier-stokes-problem>) slot-names &key &allow-other-keys)
  "Insert default component names u and p.  This is a questionable feature,
because it may interfere with other names.  Therefore it might be removed
later on.  It is also suspect, that we cannot define it as an after method,
because shared-initialize/after for <domain-problem> depends on the
components being bound."
  (call-next-method)
  (let ((dim (dimension (domain problem))))
    (unless (slot-boundp problem 'components)
      (setf (slot-value problem 'components) nil))
    (with-slots (components) problem
      ;; ensure components: by default we use u and p as component names
      (ensure components `((up (u ,dim) p)))
      ;; consistency check
      #+(or)
      (destructuring-bind (udef pdef . rest-comps) components
        (declare (ignore rest-comps))
        (assert (and (listp udef) (= (second udef) dim)
                     (or (symbolp pdef) (= (second pdef) 1))))
        ;; at the moment we even need the following strong equality, because
        ;; the problem definition for the nonlinear velocity terms depends on
        ;; it.
        (assert (equalp (list udef pdef)
                        `((u ,dim) p)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Generation of a standard Navier-Stokes problem
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; We generate the Quasi-Newton linearization of the equation
;;;   Lu + N(u) = f
;;; as
;;;   Lu + N(u0)+ N'(u0)(u-u0) = f
;;;   <=> Lu + N'(u0) u = f-N(u0)+N'(u0)u0
;;; Here  N(u) = Re u . grad u is the nonlinearity and
;;; N' is an approximation to its derivative.
;;;
;;; Choosing N'(u0) u = alpha Re u0 . grad u + beta Re u . grad u0 
;;; we obtain
;;; - mu Delta u + alpha Re u0 . grad u + beta Re u . grad u0 + grad p =
;;;       = f + (alpha + beta - 1) Re u0 . grad u0
;;; div u = 0
;;; which can then be transformed into ellsys coefficients a,b,c,r,f.

;;;; New problem definition via macros

(define-coefficient-macro <navier-stokes-problem> VISCOSITY (args &body body)
  `(FL.ELLSYS::A ,args
     (let ((viscosity (progn ,@body)))
       (ecase *viscosity-model*
         (:gugv
          (diagonal-sparse-tensor
           (scal viscosity (eye ,fl.problem::internal-dimension))
           ,fl.problem::internal-dimension))
         (:dudv (dudv-tensor ,fl.problem::internal-dimension viscosity))))))

(define-coefficient-macro <navier-stokes-problem> INERTIA (args &body body)
  "This macro expands into a quasi-linear formulation of a quasi-Newton
treatment of the momentum part of the Navier-Stokes equations.  Which parts
of the Newton linearization are included is determined by the special
variables @var{*alpha*} and @var{*beta*}."
  `(list
    (FL.ELLSYS::C (u ,@args)
                  (diagonal-sparse-tensor
                   (scal (* *alpha* (progn ,@body)) u)
                   ,fl.problem::internal-dimension))
    (FL.ELLSYS::R (d_u ,@args)
     (scal (* *beta* (progn ,@body)) d_u))
    (FL.ELLSYS::F (u d_u)
     (store (join :vertical 
             (scal (* (+ *alpha* *beta* -1) (progn ,@body)) (m* d_u u))
             (zeros 1))))))

(define-coefficient-macro <navier-stokes-problem> GRADP (&optional (args nil args-p) &body body)
  "We discretize grad(p) or div(p*I) by differentiating the test function.
The coefficient value eta is 1.0 by default."
  `(FL.ELLSYS::B ,args
     (let ((eta ,(if args-p
                     `(progn ,@body)
                     1.0)))
       (sparse-tensor
        (loop for i below ,fl.problem::internal-dimension
              collect (list (scal eta (ensure-matlisp (unit-vector ,fl.problem::internal-dimension i)))
                            i ,fl.problem::internal-dimension))))))

(define-coefficient-macro <navier-stokes-problem> CONTINUITY (&optional (args nil args-p) &body body)
  "Continuity equation coefficient with derivative on u.  The coefficient
value xi is 1.0 by default."
  `(FL.ELLSYS::C ,args
     (let ((xi ,(if args-p
                    `(progn ,@body)
                    1.0)))
       (sparse-tensor
        (loop for i below ,fl.problem::internal-dimension
              collect (list (scal xi (ensure-matlisp (unit-vector ,fl.problem::internal-dimension i)))
                            ,fl.problem::internal-dimension i))))))

(define-coefficient-macro <navier-stokes-problem> FORCE (args &body body)
  `(FL.ELLSYS::F ,args
     (map 'vector #'ensure-matlisp (progn ,@body))))

(define-coefficient-macro <navier-stokes-problem> PRESCRIBED-VELOCITY (args &body body)
  `(FL.ELLSYS::CONSTRAINT ,args
     (let ((velocity (progn ,@body)))
       (values
        (sparse-tensor (loop for k below ,fl.problem::internal-dimension
                             collect (list #m(1.0) k)))
        (sparse-tensor (loop for k below ,fl.problem::internal-dimension
                             collect (list (matrix-slice velocity :from-row k :nrows 1)
                                           k)))))))

(define-coefficient-macro <navier-stokes-problem> NO-SLIP (args &body body)
  (declare (ignore body))
  (assert (null args))
  `(PRESCRIBED-VELOCITY ()
     (zeros ,fl.problem::internal-dimension ,fl.problem::internal-multiplicity)))

(define-coefficient-macro <navier-stokes-problem> PRESSURE-BC (args &body body)
  `(FL.ELLSYS::CONSTRAINT ,args
     (values
      (sparse-tensor (list (list #m(1.0) ,fl.problem::internal-dimension)))
      (sparse-tensor (list (list (ensure-matlisp (progn ,@body))
                                 ,fl.problem::internal-dimension))))))

(define-coefficient-macro <navier-stokes-problem> SIGMA (args &body body)
  "Pass arguments to FL.ELLSYS::SIGMA.  The sparse tensor returned should fit with what is required here."
  `(FL.ELLSYS::SIGMA ,args ,@body))

(define-coefficient-macro <navier-stokes-problem> INITIAL (args &body body)
  "Pass arguments to FL.ELLSYS::INITIAL.  The values returned should fit
with what is required here.  Note that it can make sense to set also the
pressure to some reasonable initial value, if an iterative solver is used."
  `(FL.ELLSYS::INITIAL ,args ,@body))

(register-coefficient-macros
 '<navier-stokes-problem> 'viscosity 'inertia 'gradp 'continuity
 'force 'prescribed-velocity 'no-slip 'pressure-bc
 'sigma 'initial)
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; old problem definition
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun no-slip-boundary (dim)
  (let ((flags (make-array (1+ dim) :initial-element t))
	(values (make-array (1+ dim) :initial-element (zeros 1))))
    (setf (aref flags dim) nil)
  (constant-coefficient 'FL.PROBLEM::CONSTRAINT flags values)))

(defun pressure-boundary-conditions (dim value)
  (let ((flags (make-array (1+ dim) :initial-element nil))
	(values (make-array (1+ dim) :initial-element (zeros 1))))
    (setf (aref flags dim) t
          (aref values dim) value)
  (constant-coefficient 'FL.PROBLEM::CONSTRAINT flags values)))

(defvar *artificial-viscosity* nil
  "If *artificial-viscosity* is equal to a non-nil number eps, then an
  additional vicosity of size max(viscosity,eps*h) is used.")

(defun artificial-viscosity (dim viscosity factor)
  (make-instance
   '<coefficient>
   :name 'FL.ELLSYS::A
   :demands '(:Dphi)
   :eval (lambda (&key Dphi &allow-other-keys)
           (let ((h (norm Dphi :inf)))
             (diagonal-sparse-tensor
              (scal (max viscosity (* h factor))
                    (eye dim))
              dim)))))

(defun dudv-tensor (dim viscosity)
  "This is the coefficient for discretizing viscosity*eps(u):eps(v),
where eps(u) is the symmetrized gradient of u.  We call it DUDV like
in the FEM-code Navier."
  (lret ((result (make-instance '<sparse-tensor> :rank 2)))
    (dotimes (k dim)
      (dotimes (l dim)
        (setf (tensor-ref result k l)
              (elementary dim l k))))
    (dotimes (k dim)
      (m+! (eye dim) (tensor-ref result k k)))
    (scal! viscosity result)))

(defun dudv-coefficient (dim viscosity)
  (constant-coefficient 'FL.ELLSYS::A (dudv-tensor dim viscosity)))

(defvar *viscosity-model* :gugv
  "Type of viscosity model.  Can be either :GUGV or :DUDV.
The first corresponds to (grad(u), grad(v)), the second to (eps(u),
eps(v)), where eps denotes the symmetrized gradient.")

(defun navier-stokes-viscosity-coefficient (dim viscosity)
  (if *artificial-viscosity*
      (artificial-viscosity dim viscosity *artificial-viscosity*)
      (ecase *viscosity-model*
        (:gugv (isotropic-diffusion-coefficient
                dim (loop for i below dim collect viscosity)))
        (:dudv (dudv-coefficient dim viscosity)))))

;;; a start for streamline diffusion - not working at the moment!

(defvar *streamline-diffusion* 0.0
  "Streamline diffusion adds a certain velocity-dependent amount of
numerical viscosity which depends on the size of the element and the size
of convection.")

(defun navier-stokes-streamline-diffusion-coefficient (dim)
  (make-instance
   '<coefficient>
   :name 'FL.ELLSYS::A
   :demands '(:Dphi :fe-parameters (:solution . 0))
   :eval (lambda (&key Dphi solution &allow-other-keys)
           (let* ((velocities
                    (apply #'join :vertical
                           (take dim (coerce (first solution) 'list))))
                  (sd (* *streamline-diffusion*
                         (norm (m* Dphi velocities)))))
             (diagonal-sparse-tensor
              (scal sd (eye dim)) dim)))))

(defun p-div-coefficient (dim &optional (eta 1.0))
  "Pressure coefficient with derivative on test function"
  (constant-coefficient
   'FL.ELLSYS::B
   (lret ((pc (make-instance '<sparse-tensor> :rank 2)))
     (dotimes (i dim)
       (let ((direction (ensure-matlisp (unit-vector dim i))))
         (setf (tensor-ref pc i dim) (scal eta direction)))))))

(defun div-u-coefficient (dim &optional (xi 1.0))
  "Continuity equation coefficient with derivative on u"
  (constant-coefficient
   'FL.ELLSYS::C
   (lret ((cc (make-instance '<sparse-tensor> :rank 2)))
     (dotimes (i dim)
       (let ((direction (ensure-matlisp (unit-vector dim i))))
         (setf (tensor-ref cc dim i) (scal xi direction)))))))

(defun navier-stokes-pressure-and-continuity-coefficient
    (dim &key (eta 1.0) (xi 1.0))
  "The pressure and continuity part of the Navier-Stokes equation.  These
parts can be multiplied by factors @arg{eta} and @arg{xi} which can be
useful for debugging, for example."
  (?2
   ;; Old and wrong variant discretizing as grad(p)*phi!  Left here at the
   ;; moment now because I might like to examine that more in detail.
   (constant-coefficient
   'FL.ELLSYS::C
   (lret ((pc (make-instance '<sparse-tensor> :rank 2)))
     (dotimes (i dim)
       (let ((direction (ensure-matlisp (unit-vector dim i))))
	 (setf (tensor-ref pc i dim) (scal eta direction))
	 (setf (tensor-ref pc dim i) (scal xi direction))))))
   (list
    (p-div-coefficient dim eta)
    (div-u-coefficient dim xi))))

(defvar *alpha* 1.0
  "Weight for the convective part in the Quasi-Newton linearization of the
Navier-Stokes equation.")

(defvar *beta* 1.0
    "Weight for the reactive part in the Quasi-Newton linearization of the
Navier-Stokes equation.")

(defun navier-stokes-inertia-coefficients (dim reynolds)
  "Yields a quasi-Newton linearization of the term @math{u . grad u} which
has the form
@math{a Re u0 . grad u + b Re u . grad u0 = (a + b - 1) Re u0 . grad u0}
a and b are given by the values of the special variables @var{*alpha*} and
@var{*beta*}."
  (declare (ignore dim))
  (unless (zerop reynolds)
    (error "Not supported anymore for the new Navier-Stokes discretization.
Please define the problem using @macro{create-problem}.")))

(defun standard-navier-stokes-problem
    (domain &key (viscosity 1.0) (reynolds 0.0) force)
  (let ((dim (dimension domain)))
    (make-instance
     '<navier-stokes-problem>
     :domain domain :components `((u ,dim) p)
     :coefficients
     #'(lambda (patch)
	 (cond ((member-of-skeleton? patch (domain-boundary domain))
		(list (no-slip-boundary dim)))
	       ((= dim (dimension patch))
                (list (navier-stokes-viscosity-coefficient dim viscosity)
                      (navier-stokes-pressure-and-continuity-coefficient dim)
                      (ensure-coefficient 'FL.ELLSYS::F force)
                      (navier-stokes-inertia-coefficients dim reynolds)))
	       (t ()))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Driven cavity
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun driven-cavity-upper-boundary (dim &optional smooth-p)
  (let ((flags (make-array (1+ dim) :initial-element (not smooth-p)))
	(values (make-array (1+ dim) :initial-element #m(0.0))))
    (setf (aref flags dim) nil) ; no constraint for pressure
    (when smooth-p  ; zero constraint for u_n
      (setf (aref flags (1- dim)) t))
    (unless smooth-p
      (setf (aref values 0) #m(1.0)))
    (constant-coefficient 'FL.PROBLEM::CONSTRAINT flags values)))

(defun driven-cavity
    (dim &key (viscosity 1.0) (reynolds 0.0) (eta 1.0) (xi 1.0) (smooth-p nil))
  (let ((domain (n-cube-domain dim)))
    (?1 (create-problem
            (:type '<navier-stokes-problem>
             :domain domain
             :components `((u ,dim) p)
             :multiplicity 1)
          (select-on-patch (patch)
            (:d-dimensional (viscosity () viscosity)
                            (unless (zerop reynolds)
                              (inertia () reynolds))
                            (gradp () eta)
                            (continuity () xi))
            ((and :d-1-dimensional :top)
             (ecase smooth-p
               (:values (prescribed-velocity (x)
                          (let ((s (aref x 0)))
                            (scal (* s (- 1.0 s))
                                  (ensure-matlisp (unit-vector dim 0))))))
               (:force (force ()
                         (unit-vector (1+ dim) 0)))
               ((nil) (prescribed-velocity ()
                        (ensure-matlisp (unit-vector dim 0))))))
            (:origin (constraint-coefficient (1+ dim) 1))
            (:external-boundary (no-slip ())))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Periodic cavity
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun oscillating-force (dim)
  (function->coefficient
   'FL.ELLSYS::F
   #'(lambda (x)
    (let ((result (make-array (1+ dim) :initial-element nil)))
      (dotimes (i (1+ dim) result)
	(setf (aref result i)
	      (if (< i dim)
		  (make-real-matrix `((,#I"sin(2*pi*x[0])*cos(2*pi*x[1])")))
		  (zeros 1))))))))

(defun periodic-cavity (dim &key (viscosity 1.0) (reynolds 0.0))
  (let ((domain (n-cell-domain dim)))
    (make-instance
     '<navier-stokes-problem>
     :domain domain
     :coefficients
     #'(lambda (patch)
	 (when (= (dimension patch) dim)
	   (list* (navier-stokes-viscosity-coefficient dim viscosity)
		  (navier-stokes-pressure-and-continuity-coefficient dim)
		  (oscillating-force dim)
		  (navier-stokes-inertia-coefficients dim reynolds)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Channel flow
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun simple-pressure-boundary-conditions (dim dir value)
  "This function is a preliminary version which works only for boundaries
lying in a coordinate hyperplane."
  (assert (<= 0 dir (1- dim)))
  (let ((flags (make-array (1+ dim) :initial-element t))
	(values (make-array (1+ dim) :initial-element #m(0.0))))
    (setf (aref flags dir) nil) ; no constraint for dir-component
    (setf (aref values dim) (ensure-matlisp value))
    (constant-coefficient 'FL.PROBLEM::CONSTRAINT flags values)))

(defun cubic-channel (dim &key (viscosity 1.0) (reynolds 0.0) (direction 0))
  "The channel is a simple test problem which has for every Reynolds number
a Hagen-Poiseuille solution with linear pressure.  The corresponding
velocity profile is a solution to @math{-\Delta u = constant}."
  (let ((domain (n-cube-domain dim)))
    (make-instance
     '<navier-stokes-problem>
     :domain domain
     :coefficients
     #'(lambda (patch)
	 (let ((dir-coordinate (aref (midpoint patch) direction)))
	   (cond
	     ((= dim (dimension patch)) ; interior coefficients
	      (list (navier-stokes-viscosity-coefficient dim viscosity)
                    (navier-stokes-pressure-and-continuity-coefficient dim)
                    (navier-stokes-inertia-coefficients dim reynolds)))
	     ((and (= (1- dim) (dimension patch))
		   (not (/= dir-coordinate 0.0 1.0)))
	      (list (simple-pressure-boundary-conditions
		     dim direction (if (zerop dir-coordinate) 1.0 0.0))))
	     ;; other boundaries have standard no-slip bc
	     (t (list (no-slip-boundary dim)))))))))

;;; Testing

(defun test-navier-stokes ()
  (describe
   (standard-navier-stokes-problem
    (n-cube-domain 2)
    :force (unit-vector-force-coefficient 0)))
  (describe (driven-cavity 2 :smooth-p :force :reynolds 1.0))
  (linear-p (driven-cavity 2 :smooth-p :force :reynolds 1.0))

  (describe
   (create-problem (:type '<navier-stokes-problem>
                    :domain (n-cube-domain 2)
                    :components '((u 2) p))
     (select-on-patch (patch)
       (:d-dimensional
        (viscosity () 1.0)
        (inertia () 1.0))
       ((and :d-1-dimensional :upper)
        (prescribed-velocity ()
          #m((1.0) (0.0))))
       (:external-boundary
        (no-slip ())))))
  )

;;; (test-navier-stokes)
(fl.tests:adjoin-test 'test-navier-stokes)
