;;; -*- mode: lisp; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; pde-problem.lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright (C) 2003-2006 Nicolas Neuss, University of Heidelberg.
;;; Copyright (C) 2007- Nicolas Neuss, University of Karlsruhe.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; 1. Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; 2. Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
;;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
;;; NO EVENT SHALL THE AUTHOR, THE UNIVERSITY OF HEIDELBERG OR OTHER
;;; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :fl.problem)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; coefficient
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *assembly-type* '(:matrix :residual)
  "This can be dynamically bound for making a coefficient only influence
  matrix assembly.  The idea is to make parts of the coefficients only
  valid for the Jacobian calculation.  This is not yet used at the moment,
  because it would require the discretization to calculate the residual
  directly and not matrix and right-hand side.")

;;; Q: would it be reasonable to include the problem?
(defclass <coefficient> (property-mixin)
  ((name :reader coefficient-name :initform nil :initarg :name)
   (dimension :reader dimension :initform nil :initarg :dimension :documentation
	      "The dimension of the cell on which this coefficient is
active.  The value T means that it is active on all cells lying on the
patch.  The default value NIL means that it is active on cells with the
same dimension as the patch.")
   (demands :reader demands :initform () :initarg :demands
    :documentation "A list indicating which information the evaluation
function needs.  Possible choices depend on problem and discretization,
e.g. @code{:local}, @code{:global}, @code{:fe}, @code{:cell} are possible
choices.  One element can also be a list starting with the keyword
@code{:fe-parameters} and followed by symbols indicating names of finite
element functions on the discretization blackboard.")
   (eval :reader coeff-eval :initarg :eval :type function
    :documentation "The evaluation funtion.  It accepts a list of
keyword parameters which should correspond to the list in DEMANDS.")
   (row-offset :initarg :row-offset :initform nil
            :documentation "For multiphysics problem a row offset for the component index.")
   (col-offset :initarg :col-offset :initform nil
            :documentation "For multiphysics problem a column offset for the component index.")
   (assembly-type :reader assembly-type :initform *assembly-type* :initarg :assembly-type
    :documentation "A list of two symbols.  If :matrix is a member, it
    means evaluation that the coefficient contributes to the Jacobian,
    if :residual is a member, it contributes to assembling the residual.
    As noted in the documentation of @var{*assembly-type*}, this
    functionality is not yet used."))
  (:documentation "The coefficient class."))

#+(or)
(defmethod initialize-instance :after ((coeff <coefficient>) &key &allow-other-keys)
  "For compatibility with older coefficient definitions."
  (mapf (slot-value coeff 'demands)
	(lambda (x) (if (atom x) (cons x 1) x))))

(defmethod print-object :after ((coeff <coefficient>) stream)
  "Prints the coefficient name."
  (format stream "{~A}" (coefficient-name coeff)))

(defmethod evaluate ((coeff <coefficient>) (input list))
  "The pairing between coefficient and input."
  (with-slots (eval row-offset col-offset) coeff
    (let ((values (multiple-value-list (apply eval input))))
      (apply #'values
             (if row-offset
                 (if col-offset
                     (mapcar (rcurry #'shift-tensor (list row-offset col-offset)) values)
                     (mapcar (rcurry #'shift-tensor row-offset) values))
                 values)))))
#+(or)
(defmethod evaluate ((coeff <coefficient>) (input list))
  "The pairing between coefficient and input."
  (with-slots (eval row-offset col-offset) coeff
    (if col-offset
        (if (= 0 row-offset col-offset)
            (apply eval input)
            (shift-tensor (apply eval input) (list row-offset col-offset)))
        (if (= 0 row-offset)
            (apply eval input)
            (shift-tensor (apply eval input) row-offset)))))

#+(or)
(if (or (null offset1)
        (and (zerop offset1)
             (or (null offset2) (zerop offset2))))
    (if (null demands)
        (let ((memoized (multiple-value-call #'list (funcall eval))))
          (_ (values-list memoized)))
        (lambda (&rest rest)
          (multiple-value-call
              eval (apply prepare-args rest))))
    (lambda (&rest rest)
      (let ((values (multiple-value-call
                        eval (apply prepare-args rest))))
        ;; eval could return multiple values -- not yet handled!
        (break)
        (apply #'values
               (if offset2
                   (mapcar (_ #'shift-tensor _ offset1 offset2) values)
                   (mapcar (_ #'shift-tensor _ offset1) values))))))
  
(defun get-coefficients (coeffs name)
  "Get coefficients with name @arg{name} from the list @arg{coeffs}."
  (filter name coeffs :key #'coefficient-name))

(defun get-coefficient (coeffs name)
  "Get coefficient @arg{name} from the list @arg{coeffs}."
  (whereas ((these-coeffs (get-coefficients coeffs name)))
    (assert (single? these-coeffs))
    (first these-coeffs)))

(defmethod demands ((coeffs list))
  "Returns unified demands for all coefficients in the list."
  (let ((demands nil))
    (dolist (coeff coeffs)
      (dolist (demand (demands coeff))
	(pushnew demand demands :test #'equalp)))
    demands))

(defun copy-coefficient (coeff &key demands name eval)
  (with-slots (dimension assembly-type) coeff
    (make-instance
     '<coefficient>
     :name (or name (coefficient-name coeff))
     :dimension dimension
     :demands (or demands (demands coeff))
     :eval (or eval (slot-value coeff 'eval))
     :assembly-type assembly-type)))
    
(defun constant-coefficient (name value &rest other-values)
  "Returns a coefficient which takes the given value.  Several values can
be passed which is needed, for example, for returning also the type of a
boundary condition."
  (make-instance
   '<coefficient> :name name :demands () :eval
   (if other-values
       #'(lambda (&key &allow-other-keys)
	   (apply #'values value other-values))
       (constantly value))))

(defun constraint-coefficient (nr-of-components multiplicity)
  "Returns a coefficient function which sets Dirichlet zero boundary
conditions for all components of a PDE system."
  (make-instance
   '<coefficient> :name 'CONSTRAINT :dimension t :demands ()
                  :eval (let* ((eyemat (eye 1))
                               (zeromat (zeros 1 multiplicity))
                               (P (sparse-tensor (loop for k below nr-of-components
                                                       collect (list eyemat k))))
                               (Q (sparse-tensor (loop for k below nr-of-components
                                                       collect (list zeromat k)))))
                          (lambda (&key &allow-other-keys)
                            (values P Q)))))

(defgeneric zero-constraints (problem)
  (:documentation "Returns a coefficient function which constrains all
system components to zero.")
  (:method (problem)
    (constraint-coefficient
     (nr-of-components problem) (multiplicity problem))))

(defun identification-coefficient (master mapping)
 "A special coefficient used for identifying parts of the domain.  The
  coefficient evaluation returns the master coordinates."
 (lret ((coeff (make-instance
                '<coefficient> :name 'IDENTIFICATION
                :dimension t :demands '(:local :global)
                :eval mapping :assembly-type nil)))
   (setf (get-property coeff 'MASTER) master)))

(defun identification-coefficient-p (coeff)
  (and (eq (coefficient-name coeff) 'IDENTIFICATION)
       (get-property coeff 'MASTER)))

(defun find-coefficient (coeff-name problem)
  (mapper-some (lambda (patch)
                 (find coeff-name (coefficients-of-patch patch problem)
                       :key #'coefficient-name))
               #'skel-for-each (domain problem)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun constraint-p (coeff)
  (member (coefficient-name coeff) '(CONSTRAINT IDENTIFICATION)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun sigma-p (coeff)
  (eq (coefficient-name coeff) 'SIGMA))

;;; fe-parameters

(concept-documentation
 "fe-parameters is an element in the demand list of coefficient functions
which is a pair consisting of :fe-parameters followed by a list of symbols
denoting the ansatz-space functions on the blackboard to be evaluated at
each integration point.  Each entry is of the form '(symbol . k)'. which
means that a k-jet has to be evaluated.  The form 'symbol' is an
abbreviation for '(symbol . 0)'.

Examples:
@code{(:fe-parameters :solution)}
@code{(:fe-parameters (:solution . 1))}
@code{(:fe-parameters (:solution . 1) (:flow-field . 0))}")
 
(defun fe-parameter-p (demand)
  (and (consp demand)
       (eq :fe-parameters (car demand))))

(defun parameter-name (para)
  (etypecase para
    (symbol para)
    (list (car para))))

(defun parameter-order (para)
  (etypecase para
    (symbol 0)
    (list (cdr para))))

(defun compress-fe-parameters (paras)
  (remove-duplicates
   (loop for para in paras collect
	 (cons (parameter-name para)
	       (loop for para2 in paras
		     when (eq (parameter-name para2) (parameter-name para))
		     maximize (parameter-order para2))))
   :test #'equalp))

(defun required-fe-functions (coeffs)
  "Returns a list of finite element functions required by the coefficients
in the property list @arg{coeffs}."
  (compress-fe-parameters
   (loop for coeff in coeffs appending
	 (loop for demand in (demands coeff)
	       when (fe-parameter-p demand)
	       appending (cdr demand)))))

(defun add-fe-parameters-demand (demands new-paras)
  "Adds a list of fe-functions to the demands."
  (let ((old-paras (cdr (find-if #'fe-parameter-p demands))))
    (cons (cons :fe-parameters
		(compress-fe-parameters (union old-paras new-paras)))
	  (remove-if #'fe-parameter-p demands))))

(defun solution-dependent (coeffs)
  (find :solution (required-fe-functions coeffs) :key #'car))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; some coefficient functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun fx->coefficient (name func &key (offset 0))
  "The function argument @arg{func} is transformed into a coefficient
depending on global coordinates."
  (make-instance '<coefficient>
                 :name name :demands '(:global)
                 :eval #'(lambda (&key global &allow-other-keys)
                           (let ((result (funcall func global)))
                             (if (zerop offset)
                                 result
                                 (shift-tensor result offset))))))

(defun fu->coefficient (name func)
  "The function argument @arg{func} is transformed into a coefficient
depending on the solution."
  (make-instance '<coefficient> :name name :demands '((:fe-parameters :solution))
		 :eval #'(lambda (&key solution &allow-other-keys)
			   (funcall func (first solution)))))

(defun fxu->coefficient (name func &key (k 0))
  "The function argument @arg{func} is transformed into a coefficient
depending on position and solution.  If k is different from 0 then the
k-jet of f is returned as arguments."
  (make-instance '<coefficient> :name name
		 :demands `(:global (:fe-parameters (:solution . ,k)))
		 :eval #'(lambda (&key global solution &allow-other-keys)
			   (apply func global solution))))

(defun function->coefficient (name func)
  (fx->coefficient name func))

(defun ensure-coefficient (name obj)
  "Returns @arg{obj} if it is a coefficient, converts @arg{obj} into a
coefficient depending on the space variable if @arg{obj} is a function;
otherwise, @arg{obj} is made into a constant coefficient."
  (cond ((typep obj '<coefficient>) obj)
	((functionp obj) (fx->coefficient name obj))
	((typep obj '<function>)
	 (make-instance '<coefficient> :name name :demands '(:global)
			:eval #'(lambda (&key global &allow-other-keys)
				  (evaluate obj global))))
	(t (constant-coefficient name obj))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; <domain-problem>
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass <domain-problem> (<problem>)
  ((domain :reader domain :initarg :domain :type <domain>)
   (components :reader components :initarg :components
	       :documentation "A list whose elements are symbols or lists
of the forms (symbol dim) or (symbol dim type) or (symbol subcomponents)
describing the components occuring in the pde.  Alternatively, this slot
can contain a function/dictionary mapping patches to such lists.")
   (multiplicity :reader multiplicity :initform 1 :initarg :multiplicity
		 :documentation "Multiplicity of the right-hand side.")
   (coefficients :reader coefficients :initarg :coefficients :documentation
		 "A -usually memoized- function mapping patches to coefficient lists."))
  ;;
  (:documentation "An instance of this class describes a problem posed on
@slot{domain} with coefficients given on each patch of the domain.  The
slot @slot{multiplicity} is a positive integer which denotes the number of
right-hand sides and solutions (e.g. when computing several eigenvectors at
once)."))

(defclass <interpolation-problem> (<domain-problem>)
  ()
  (:documentation "Interpolation problem on a domain.  The function which
is to be interpolated is given as a coefficient with key FUNCTION in the
coefficient list."))

(defclass <pde-problem> (<domain-problem>)
  ()
  (:documentation "Base-class for a pde-problem."))

(defclass <multiphysics-mixin> ()
  ((subproblems :reader subproblems :initform nil :initarg :subproblems)))

(defgeneric nr-of-components-of-problem (problem domain)
  (:documentation "Returns the number of components a problem should have
  which is known for many problems.  A return value of NIL means that the
  number is not clear.")
  (:method ((problem <pde-problem>) domain)
    (nr-of-components-of-problem (class-name (class-of problem))))
  (:method ((problem symbol) domain)
    nil))

(defun component-symbol (comp)
  (if (symbolp comp) comp (first comp)))

(defun component-name (comp)
  (symbol-name (component-symbol comp)))

(defun check-component (comp)
  "Checks if comps is of the correct form, see the documentation of the
slot @slot{components} in @class{domain-problem}."
  (or (symbolp comp)
      (and (listp comp)
           (symbolp (first comp))
           (or (and (<= 2 (length comp) 3) (numberp (second comp)))
               (check-components (rest comp))))))

(defun check-components (comps)
  "Checks if comps is of the correct form, see the documentation of the
slot @slot{components} in @class{domain-problem}."
  (every #'check-component comps))

;;; (check-components '((upc (up (u 2) p) c)))
;;; (check-components '((up (u 2) p) c))
;;; (check-components '((u 2) p))

(defun component-length (component)
  (check-component component)
  (if (symbolp component)
      1
      (if (numberp (second component))
          (second component)
          (reduce #'+ (rest component) :key #'component-length))))

;;; (component-length '(upc (up (u 2) p) c))
;;; (component-length '(up (u 2) p))
;;; (component-length '(u 2))
;;; (component-length '(c))

(defun find-component (components component-name)
  "Finds the component with the given name together with its offset."
  (labels ((walk-tree (comps offset)
             (and comps
                  (let ((comp (first comps)))
                    (if (eql (component-symbol comp) component-name)
                        (cons comp offset)
                        (or (and (listp comp)
                                 (not (numberp (second comp)))
                                 (walk-tree (rest comp) offset))
                            (walk-tree (rest comps) (+ offset (component-length comp)))))))))
    (aand (walk-tree components 0)
          (values (progn (assert (car it)) (car it))
                  (cdr it)))))

(defun component-position (components component-name)
  (nth-value 1 (find-component components component-name)))

;;; (find-component '((upc (up (u 2) p) c)) 'upc)
;;; (find-component '((upc (up (u 2) p) c)) 'up)
;;; (find-component '((upc (up (u 2) p) c)) 'u)
;;; (find-component '((upc (up (u 2) p) c)) 'p)
;;; (find-component '((upc (up (u 2) p) c)) 'c)

(defun extraction-information (components component)
  "If @arg{component} is found, its offset, length, a scalarp-flag, and the
full form of the component is returned."
  (multiple-value-bind (comp offset)
      (find-component components component)
    (and comp
         (let ((length (component-length comp)))
           (values offset length (= length 1) comp)))))

;;; (extraction-information '((upc (up (u 2) p) c)) 'upc)
;;; (extraction-information '((upc (up (u 2) p) c)) 'up)
;;; (extraction-information '((upc (up (u 2) p) c)) 'u)
;;; (extraction-information '((upc (up (u 2) p) c)) 'p)
;;; (extraction-information '((upc (up (u 2) p) c)) 'c)

(defmethod shared-initialize :after ((problem <domain-problem>) slot-names
                                     &key patch->coefficients &allow-other-keys)
  "Checks @slot{components}, does @slot{coefficients} setup when the
problem specification is given as a list in @arg{patch->coefficients}, and
finally memoizes @slot{coefficients}."
  (with-slots (domain coefficients) problem
    ;; component name check
    (doskel (patch domain)
      (let ((components (components-of-patch patch problem)))
	(assert (check-components components))
	(when (some (lambda (comp)
		      (let ((name (component-name comp)))
			(or (string-equal name "X")
			    (string-equal name "TIME")
			    (string-equal (subseq name 0 1) "D"))))
		    components)
	  (error "Illegal component names."))))
    ;; setup coefficients slot from old-style patch->coefficients
    (when patch->coefficients
      (assert (listp patch->coefficients))
      (setf coefficients
	    (lambda (patch)
	      (loop with classifications = (patch-classification patch domain)
		 for (id coeffs) in patch->coefficients
		 when (test-condition id classifications)
		 do (return coeffs)))))
    ;; memoize coefficients
    (setf coefficients
          (if (slot-boundp problem 'coefficients)
              (let ((coefficients coefficients))  ; !! necessary
                (memoize-1 (lambda (patch)
                             (flatten (funcall coefficients patch)))))
              (constantly ())))))

(defmethod domain-dimension ((problem <domain-problem>))
  (dimension (domain problem)))

(defmethod describe-object :after ((problem <domain-problem>) stream)
  (doskel ((patch properties) (domain problem))
    (format stream "~&Cell ~A  [Mapping ~A]~%Properties: ~S~%Coeffs: ~A~2%"
	    patch (mapping patch) properties
	    (coefficients-of-patch patch problem))))

(defgeneric nr-of-components (problem)
  (:documentation "Returns the number of components for @arg{problem}.")
  (:method ((problem <domain-problem>))
    "Counts the number of components."
    (with-slots (components) problem
      (typecase components
	(list (reduce #'+ components :key #'component-length))
	(t nil)  ; no clear definition of number of components
	))))

(defgeneric components-of-patch (patch problem)
  (:documentation "Reader for the components of @arg{problem} on @arg{patch}.")
  (:method (patch problem)
    (with-slots (components) problem
      (etypecase components
        (list components)
        (function (funcall components patch))
        (hash-table (gethash patch components))))))

(defgeneric (setf components-of-patch) (value patch problem)
  (:documentation "Writer for the components of @arg{problem} on @arg{patch}.")
  (:method (value patch problem)
    (with-slots (components) problem
      (ensure components (make-hash-table))
      (setf (gethash patch components) value))))

(defun some-components (problem)
  "Find a component list even in the case where there is a variable number
of components across the domain."
  (let ((components (components problem)))
    (typecase components
      (list components)
      (function
       (let* ((domain (domain problem))
              (patch (first (cells-of-highest-dim domain))))
         (components-of-patch patch problem))))))
  
(defun nr-of-components-of-patch (patch problem)
  (reduce #'+ (components-of-patch patch problem)
          :key #'component-length))

(defgeneric components-of-cell (cell mesh problem)
  (:documentation
   "Returns the components of @arg{problem} on @arg{cell}.")
  (:method (cell mesh problem)
    (components-of-patch (patch-of-cell cell mesh) problem)))

(defun coefficients-of-patch (patch problem)
  "Reader for the coefficients of @arg{patch} for @arg{problem}."
  (funcall (coefficients problem) patch))

(defgeneric coefficients-of-cell (cell mesh problem)
  (:documentation
   "Returns the coefficients of @arg{problem} on @arg{cell}.")
  (:method (cell mesh problem)
    "Default method, returns the coefficients of the associated patch."
    (coefficients-of-patch (patch-of-cell cell mesh) problem)))

(defun filter-applicable-coefficients (coeffs cell patch &key (constraints t))
  "Filters out the applicable coefficients for the respective cell with the
given patch."
  (remove-if-not
   (lambda (coeff)
     (and (or (eq (dimension coeff) t)
	      (= (dimension cell)
		 (or (dimension coeff) (dimension patch))))
	  (or constraints (not (constraint-p coeff)))))
   coeffs))

(defgeneric classify-problem (problem &rest keyword-args)
  (:documentation "Classifies a problem (e.g. linear/nonlinear,
self-adjoint or not, time-dependent or not, and puts possible results in
its property list, or even change the problem class to a more specialized
problem.")
  (:method ((problem <pde-problem>)
            &key (linear-p nil linear-p-p)
              (coercive nil coercive-p)
            &allow-other-keys)
    "Finds out nonlinearity of the problem by checking for nonlinear coefficients"
    (unless linear-p-p
      (setf linear-p t)
      (doskel (patch (domain problem))
        (when (solution-dependent (coefficients-of-patch patch problem))
          (setf linear-p nil)
          (return))))
    (setf (get-property problem 'linear-p) linear-p)
    (when coercive-p
      (setf (get-property problem 'coercive) coercive))
    ))

(defmethod shared-initialize :after
    ((problem <pde-problem>) slot-names &rest args
     &key (classify t) &allow-other-keys)
  (when classify
    (apply #'classify-problem problem args)))

(defgeneric dual-problem (problem functional)
  (:documentation "Returns the dual problem for @arg{problem} with the
right-hand side given by @arg{functional}.  The solution of this problem
measures the sensitivity of @arg{functional} applied to the solution of
problem with respect to errors in the solution."))

(defgeneric self-adjoint-p (problem)
  (:documentation "Returns two values.  The first says if @arg{problem} is
self-adjoint, the second says if that value has really been checked.")
  (:method (problem)
    "The default method says that @arg{problem} is not self-adjoint and
that no check has been performed."
    (declare (ignore problem))
    (values nil nil)))

;;; Testing

(defun test-pde-problem ()
  (function->coefficient
   'test
   #'(lambda (&key global &allow-other-keys) (exp (aref global 0))))
  (make-instance '<pde-problem> :domain (n-cube-domain 1) :components '(u))
  )

;;; (test-pde-problem)
(fl.tests:adjoin-test 'test-pde-problem)
